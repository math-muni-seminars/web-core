const path = require('path');

module.exports = {
    module: {
        rules: [{
            test: /\.m?js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env', '@babel/react']
                }
            }
        }]
    },
    entry: './src/index.js',
    output: {
        filename: 'main.js',
        library: 'cmp',
        path: path.resolve(__dirname, 'django-web/core/static/build/'),
    },
};
module.exports = {
  darkMode: 'class',
  purge: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        // primary: {
        //   lighter: "#EEC147",
        //   DEFAULT: "#EBB627",
        //   darker: "#D8A414",
        // },
      },
      width: {
        small: '640px',
      },
      minHeight: {
        '44': '11rem',
      },
      fontFamily: {
        math: ["CMU", "Old Standard TT"],
        brkos: "Quicksand",
      },
    },
  },
  variants: {},
  plugins: [require("daisyui")],
  daisyui: {
    styled: true,
    themes: [
      {
        brkos: {
          // 'primary': '#FFD519',
          // 'primary-focus': '#F5C800',
          'primary': '#FBBD23',
          'primary-focus': '#efac04',
          'primary-content': '#ffffff',
          'secondary': '#00B68F',
          'secondary-focus': '#008F70',
          'secondary-content': '#ffffff',
          'accent': '#FF0084',
          'accent-focus': '#E00074',
          'accent-content': '#ffffff',
          'neutral': '#2f4858',
          'neutral-focus': '#243742',
          'neutral-content': '#ffffff',
          'base-100': '#ffffff',
          'base-200': '#F9FBFC',
          'base-300': '#F2F6F8',
          'base-content': '#171B1D',
          'info': '#3ABFF8',
          'success': '#00B68F',
          'warning': '#FBBD23',
          'error': '#FF0084',

          "--rounded-box": "0.2rem", // border radius rounded-box utility class, used in card and other large boxes
          "--rounded-btn": "0.2rem", // border radius rounded-btn utility class, used in buttons and similar element
          "--rounded-badge": "1rem", // border radius rounded-badge utility class, used in badges and similar
          // "--animation-btn": "0.25s", // duration of animation when you click on button
          // "--animation-input": "0.2s", // duration of animation for inputs like checkbox, toggle, radio, etc
          "--btn-text-case": "uppercase", // set default text transform for buttons
          // "--btn-focus-scale": "0.95", // scale transform of button when you focus on it
           "--border-btn": "2px", // border width of buttons
          // "--tab-border": "1px", // border width of tabs
          // "--tab-radius": "0.5rem", // border radius of tabs
        },
        /*mathrace: {
          'primary': '#50a376',
          'primary-focus': '#48936a',
          'primary-content': '#ffffff',
          'secondary': '#62949d',
          'secondary-focus': '#4c858f',
          'secondary-content': '#ffffff',
          'accent': '#fde26a',
          'accent-focus': '#e4cb5f',
          'accent-content': '#ffffff',
          'neutral': '#2f4858',
          'neutral-focus': '#2a414f',
          'neutral-content': '#ffffff',
          'base-100': '#ffffff',
          'base-200': '#f9fafb',
          'base-300': '#d1d5db',
          'base-content': '#2f4858',
          'info': '#2094f3',
          'success': '#009485',
          'warning': '#ff9900',
          'error': '#ff5724',

          '--border-btn': '3px',
        },*/
        mathrace: {
          'primary': '#008800',
          'primary-focus': '#007500',
          'primary-content': '#ffffff',
          'secondary': '#006670',
          'secondary-focus': '#005860',
          'secondary-content': '#ffffff',
          'accent': '#fae316',
          'accent-focus': '#d9c512',
          'accent-content': '#ffffff',
          'neutral': '#004100',
          'neutral-focus': '#003700',
          'neutral-content': '#ffffff',
          'base-100': '#ffffff',
          'base-200': '#f9fafb',
          'base-300': '#d1d5db',
          'base-content': '#2f4858',
          'info': '#00c5ff',
          'success': '#32942e',
          'warning': '#ff9900',
          'error': '#d24300',

          "--rounded-box": "0.2rem", // border radius rounded-box utility class, used in card and other large boxes
          "--rounded-btn": "0.2rem", // border radius rounded-btn utility class, used in buttons and similar element
          "--rounded-badge": "1rem", // border radius rounded-badge utility class, used in badges and similar
          "--btn-text-case": "uppercase", // set default text transform for buttons
          "--border-btn": "2px",
        },
      },
    ],
    base: true,
    utils: true,
    logs: true,
    rtl: false,
  },
}
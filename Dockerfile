FROM node:latest as builder

RUN mkdir /build
COPY ./src /build/src
COPY ./django-web /build/django-web
COPY ./package-lock.json /build
COPY ./package.json /build
COPY ./webpack.config.js /build
COPY ./tailwind.config.js /build

RUN rm -rf /build/django-web/data
RUN rm -rf /build/django-web/static


WORKDIR /build
RUN npm install
RUN npm run build

FROM ubuntu:latest

RUN apt update
RUN apt install python3 python3-pip pandoc -y

WORKDIR /srv
COPY ./requirements.txt /srv
RUN pip3 install -r requirements.txt

COPY --from=builder /build/django-web /srv

RUN mkdir data

# RUN python3 ./manage.py migrate

EXPOSE 8080

CMD ["python3", "manage.py", "runserver", "0.0.0.0:8080"]
from django.contrib.auth.forms import PasswordChangeForm, SetPasswordForm
from django.core.validators import FileExtensionValidator, MinValueValidator, MaxValueValidator
from mathrace.models import Team
from django.utils import timezone

from django.template import loader
from django.contrib.auth import get_user_model
from django.conf import settings
from django.core.mail import EmailMessage
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from django.utils.translation import gettext, gettext_lazy as _
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes
from django.core.exceptions import ValidationError

from core import forms
from contest.widgets import SchoolWidget
from core.widgets import CheckboxGDPRWidget

User = get_user_model()


class RegisterTeamFormNoGDPR(forms.ModelForm):
    name = forms.CharField(required=True, max_length=30, label="Název", labeled=True)
    underground = forms.BooleanField(required=False, label="Underground")
    school = forms.CharField(widget=SchoolWidget, required=False, max_length=250, label="Škola")
    second_member = forms.CharField(required=True, max_length=60, label="Jméno a příjmení 2. člena", labeled=True)
    third_member = forms.CharField(required=False, max_length=60, label="Jméno a příjmení 3. člena", labeled=True)
    fourth_member = forms.CharField(required=False, max_length=60, label="Jméno a příjmení 4. člena", labeled=True)

    def clean(self):
        data = self.cleaned_data

        if not data.get('underground',False) and not data.get('school',None):
            self.add_error('school',forms.ValidationError('Zadejte školu'))
        if data.get('underground',False):
            data['school'] = None

        if not data.get('third_member',None) and data.get('fourth_member',None):
            data['third_member'] = data['fourth_member']
            data['fourth_member'] = None

        return data

    class Meta:
        model = Team
        fields = ['name', 'underground', 'school', 'second_member', 'third_member', 'fourth_member']


class RegisterTeamForm(RegisterTeamFormNoGDPR):
    name = forms.CharField(required=True, max_length=30, label="Název", labeled=False)
    second_member = forms.CharField(required=True, max_length=60, label="Jméno a příjmení 2. člena", labeled=False)
    third_member = forms.CharField(required=False, max_length=60, label="Jméno a příjmení 3. člena", labeled=False)
    fourth_member = forms.CharField(required=False, max_length=60, label="Jméno a příjmení 4. člena", labeled=False)
    gdpr = forms.BooleanField(widget=CheckboxGDPRWidget(attrs={'gdpr_url':'/static/mathrace/GDPR.pdf'}), required=True, label="Souhlasím s podmínkami zpracování osobních údajů")

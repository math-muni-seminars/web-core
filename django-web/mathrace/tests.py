"""
from django.test import TestCase
from django.test import Client
from django.core.exceptions import ObjectDoesNotExist
from .models import Contest, ContestType, TaskCollection, Task, TaskFileSubmission, Contestant, ResultsTable
from .views.results import PageResults
from datetime import datetime
from django.contrib.auth import get_user_model
User = get_user_model()
import pytz
utc=pytz.UTC


# Tests suggestions:
# submit pdf solution
# generate real world worth number of contestants and submissions and generate pdf results.


class TestRegistration(TestCase):
    def test_register_ok(self):
        c = Client()
        response = c.post('/register/', {'username': 'tom',
                                         'first_name': 'Tomas',
                                         'last_name': 'Novotny',
                                         'email': 'ahoj@neco.cz',
                                         'school': 'GML',
                                         'graduate_year': '2020',
                                         'password1': 'mySuperSecret486#pswd',
                                         'password2': 'mySuperSecret486#pswd'})
        self.assertEqual(response.status_code, 302)  # form submitted

    def test_register_nok_email(self):
        c = Client()
        response = c.post('/register/', {'username': 'tom',
                                         'first_name': 'Tomas',
                                         'last_name': 'Novotny',
                                         'email': 'ahoj',
                                         'school': 'GML',
                                         'graduate_year': '2020',
                                         'password1': 'mySuperSecret486#pswd',
                                         'password2': 'mySuperSecret486#pswd'})
        self.assertEqual(response.status_code, 200)

    def test_login(self):
        c = Client()
        response = c.post('/register/', {'username': 'tom',
                                         'first_name': 'Tomas',
                                         'last_name': 'Novotny',
                                         'email': 'ahoj@neco.cz',
                                         'school': 'GML',
                                         'graduate_year': '2020',
                                         'password1': 'mySuperSecret486#pswd',
                                         'password2': 'mySuperSecret486#pswd'})
        self.assertEqual(response.status_code, 302)
        try:
            u = Contestant.objects.get(user__username='tom')
        except ObjectDoesNotExist:
            self.fail("contestant not registered")

        response = c.post('/login/', {'username': 'tom', 'password1': 'mySuperSecret486#pswd'})
        self.assertTrue(u.user.is_authenticated)
        response = c.get('/account/')
        self.assertEqual(response.status_code, 200)


    def test_account_redirect(self):
        c = Client()
        response = c.get('/account/')
        self.assertEqual(response.status_code, 302)


class TestPublicPages(TestCase):
    def setUp(self):
        user = User.objects.create_user(username='petr', password='12345')

    def test_view_index(self):
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)

    def test_view_index_user(self):
        self.client.login(username='petr', password='12345')
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_view_tasks(self):
        c = Client()
        response = c.get('/tasks')
        self.assertEqual(response.status_code, 301)  # redirect

    def test_view_tasks_user(self):
        self.client.login(username='petr', password='12345')
        response = self.client.get('/tasks')
        self.assertEqual(response.status_code, 200)

    def test_view_results(self):
        c = Client()
        response = c.get('/results')
        self.assertEqual(response.status_code, 301)

    def test_view_tasks_user(self):
        self.client.login(username='petr', password='12345')
        response = self.client.get('/results')
        self.assertEqual(response.status_code, 301)


class TestResultsPage(TestCase):
    def setUp(self):
        user1 = User.objects.create_user(username='petr',
                                        password='12345',
                                        first_name='Pavel',
                                        last_name='Trnka')
        contestant1 = Contestant.objects.create(user=user1,
                                               school='GML',
                                               math_class=False,
                                               graduate_year=2023)
        user2 = User.objects.create_user(username='pavel',
                                        password='12345',
                                        first_name='Petr',
                                        last_name='Vémola')
        contestant2 = Contestant.objects.create(user=user2,
                                               school='Gyrec',
                                               math_class=True,
                                               graduate_year=2022)
        now = datetime.now(utc)
        end_year = 2021
        close_date = datetime(now.year + 1, now.month, now.day, tzinfo=utc)
        contest_type = ContestType.objects.create(abbreviation="brkos",
                                                  name='Brkos')
        contest = Contest.objects.create(created=now,
                                         updated=now, 
                                         published=True,
                                         public_from=now,
                                         order=10,
                                         contest_type=contest_type,
                                         end_year=end_year)
        tc1 = TaskCollection.objects.create(created=now,
                                           updated=now,
                                           published=True,
                                           public_from=now,
                                           order=1,
                                           name='1. série',
                                           close_date=close_date,
                                           contest=contest)
        tc2 = TaskCollection.objects.create(created=now,
                                           updated=now,
                                           published=True,
                                           public_from=now,
                                           order=2,
                                           name='2. série',
                                           close_date=close_date,
                                           contest=contest)
        tc3 = TaskCollection.objects.create(created=now,
                                           updated=now,
                                           published=True,
                                           public_from=now,
                                           order=3,
                                           name='3. série',
                                           close_date=close_date,
                                           contest=contest)
        tcs = [tc1, tc2, tc3]
        for serie in range(1, 4):
            for uloha in range(1, 9):
                Task.objects.create(created=now,
                                    updated=now,
                                    order=uloha,
                                    name='%d' % uloha,
                                    collection=tcs[serie - 1],
                                    text='text',
                                    max_score=5)

    def test_view_get_result_collection(self):
        contestant1 = Contestant.objects.get(user__username='petr')
        contestant2 = Contestant.objects.get(user__username='pavel')
        tc = TaskCollection.objects.get(order=1)
        tasks = Task.objects.filter(collection=tc)
        grades1 = [3, 1.12, 2.2, 4, 1, 0, 4, 5]
        grades2 = [3, 2.5, 4, 5, 3, 0, 4, 5]

        for i in range(8):
            TaskFileSubmission.objects.create(contestant=contestant1,
                                              task=tasks[i],
                                              score=grades1[i])
        for i in range(8):
            TaskFileSubmission.objects.create(contestant=contestant2,
                                              task=tasks[i],
                                              score=grades2[i])

        response = self.client.get('/results/')
        table = response.context['results']
        table = [[x["value"] for x in row] for row in table]
        self.assertEqual(table[0][-1], 24)
        self.assertEqual(table[1][-1], 20.49)
    
    def test_view_get_results_cumulative(self):
        contestant1 = Contestant.objects.get(user__username='petr')
        contestant2 = Contestant.objects.get(user__username='pavel')
        conts = [contestant1, contestant2]
        tc1 = TaskCollection.objects.get(order=1)
        tc2 = TaskCollection.objects.get(order=2)
        tc3 = TaskCollection.objects.get(order=3)
        tasks1 = Task.objects.filter(collection=tc1)
        tasks2 = Task.objects.filter(collection=tc2)
        tasks3 = Task.objects.filter(collection=tc3)
        tasks = [tasks1, tasks2, tasks3]
        grades = [[[3, 3, 0, 0, 0, 2.5, 3, 2], [3, 3, 2.8, 3, 3.9, 0, 0, 0]],
                  [[0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0]],
                  [[3, 3, 3, 3, 1, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0]]]
        # grades[i][j][k] is i-th task collection, j-th person, k-th task
        for cont in range(1, 3):
            for tc in range(1, 4):
                for t in range(1, 9):
                    TaskFileSubmission.objects.create(contestant=conts[cont - 1],
                                                      task=tasks[tc - 1][t - 1],
                                                      score=grades[tc-1][cont-1][t-1])

        table = self.client.get('/results/10/1/cumulative').context['results']
        table = [[x["value"] for x in row] for row in table]
        self.assertEqual(table[0][-2], 16.39)
        self.assertEqual(table[1][-2], 16.12)
        self.assertEqual(table[0][3], 'Gyrec')
        table = self.client.get('/results/10/2/cumulative').context['results']
        table = [[x["value"] for x in row] for row in table]
        self.assertEqual(table[0][-1], 16.39)
        self.assertEqual(table[1][-1], 16.12)
        self.assertEqual(table[0][-2], 0)
        self.assertEqual(table[1][-2], 0)
        self.assertEqual(table[0][3], 'Gyrec')
        table = self.client.get('/results/10/3/cumulative').context['results']
        table = [[x["value"] for x in row] for row in table]
        self.assertEqual(table[0][-1], 31.88)
        self.assertEqual(table[1][-1], 16.39)
        self.assertEqual(table[0][-2], 15.75)
        self.assertEqual(table[1][-2], 0)
        self.assertEqual(table[0][3], 'GML')
"""
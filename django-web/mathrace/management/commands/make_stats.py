from mathrace.models import Contest, TaskCollection, Task, Team, ContestStatus
import datetime

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Makes MathRace stats'

    # def add_arguments(self, parser):
    #     parser.add_argument('poll_ids', nargs='+', type=int)

    def handle(self, *args, **options):
        f = open("core/static/mathrace/stats.html", "w")

        contest = Contest.objects.order_by("order").last()
        tasks = Task.objects.filter(collection__contest=contest)

        f.write("<table class='table mx-auto'><thead><tr><th>Úloha</th><th>Vyřešili</th><th>Zkoušeli</th><th>Průměrný čas</th><th>Pokusů (úspěšní)</th><th>Pokusů (všichni)</th></tr></thead><tbody>")

        for task in tasks:
            statuses = ContestStatus.objects.filter(task=task)

            count_successfull = 0
            count_all = 0
            time = 0
            attempts_succesfull = 0
            attempts_all = 0

            for status in statuses:
                count_all += 1
                attempts_all += status.wrong_attempts

                if status.solved:
                    count_successfull += 1
                    attempts_succesfull += status.wrong_attempts + 1
                    attempts_all += 1
                    if status.time is not None: time += status.time

            try: avg_attempts_succesfull = attempts_succesfull / count_successfull
            except: avg_attempts_succesfull = "-"
            try: avg_attempts_all = attempts_all / count_all
            except: avg_attempts_all = "-"
            try: avg_delta = datetime.timedelta(seconds=time//count_successfull)
            except: avg_delta = "-"

            f.write("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td></tr>".format(task.name,count_successfull,count_all,avg_delta,avg_attempts_succesfull,avg_attempts_all))
        
        f.write("</tbody></table>")
        f.close()


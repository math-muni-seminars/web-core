from mathrace.models import Contest, TaskCollection, Task, Team, ContestStatus
import datetime

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Makes MathRace results'

    # def add_arguments(self, parser):
    #     parser.add_argument('poll_ids', nargs='+', type=int)

    def handle(self, *args, **options):
        f = open("core/static/mathrace/results.html", "w")

        contest = Contest.objects.order_by("order").last()
        collections = TaskCollection.objects.filter(contest=contest).order_by("order")
        tasks = [[task for task in Task.objects.filter(collection=collection).order_by("order")] for collection in collections]

        cols = max([len(colltasks) for colltasks in tasks])
        rows = len(collections)

        f.write("<table class='w-full mathrace-results'><tr><th>Pořadí</th><th>Název týmu</th><th>Úloh</th><th>Čas</th><th colspan='{0}'>Vyřešeno</th></tr>".format(cols))

        teams = Team.current_contest().order_by('-solved_count','-decisive_time')

        for order, team in enumerate(teams):
            try:
                delta = datetime.timedelta(seconds=team.decisive_time)
            except:
                delta = "-"

            f.write("<tr><td rowspan='{0}'>{1}.</td><td rowspan='{0}'>{2}</td><td rowspan='{0}'>{3}</td><td rowspan='{0}'>{4}</td>".format(rows,order+1,team.name,team.solved_count,delta))

            for task in tasks[0]:
                try:
                    status = ContestStatus.objects.get(team=team, task=task)
                    f.write("<td class='text-center{0}'>{1}</td>".format(" task-solved" if status.solved else "",status.wrong_attempts))
                except:
                    f.write("<td></td>")

            f.write("</tr>")

            for colltasks in tasks[1:]:
                f.write("<tr>")

                for task in colltasks:
                    try:
                        status = ContestStatus.objects.get(team=team, task=task)
                        f.write("<td class='text-center{0}'>{1}</td>".format(" task-solved" if status.solved else "",status.wrong_attempts))
                    except:
                        f.write("<td></td>")

                f.write("</tr>")
        
        f.write("</table>")
        f.close()


from mathrace.models import Contest, TaskCollection, Task, Team, ContestStatus, Attempt
import datetime

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Recalculates MathRace results'

    # def add_arguments(self, parser):
    #     parser.add_argument('poll_ids', nargs='+', type=int)

    def handle(self, *args, **options):
        contest = Contest.objects.order_by("order").last()

        ContestStatus.objects.filter(team__contest=contest).update(solved=False, wrong_attempts=0, time=0)
        Team.objects.filter(contest=contest).update(solved_count=0, decisive_time=0, last_wrong_attempt=None)

        for attempt in Attempt.objects.filter(team__contest=contest).order_by('time'):
            solution_values = attempt.task.solution_value.split('|')
            status = ContestStatus.objects.get(team=attempt.team, task=attempt.task)
            team = attempt.team

            if not status.solved and (team.last_wrong_attempt is None or (attempt.time - team.last_wrong_attempt) >= 40):
                if attempt.answer in solution_values:
                    status.solved = True
                    status.time = attempt.time
                else:
                    status.wrong_attempts += 1
                    team.last_wrong_attempt = attempt.time
                    team.save()
                
                status.save()

        for status in ContestStatus.objects.filter(team__contest=contest):
            team = status.team

            if status.solved:
                team.solved_count += 1
                team.decisive_time += status.time + 1200 * status.wrong_attempts
                team.save()


        


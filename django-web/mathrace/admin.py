from django.contrib import admin, messages
from django.template.loader import render_to_string

from .models import Contest, TaskCollection, Task, Team, Attempt, ContestStatus
from django.utils.html import format_html, urlencode
from django.urls import reverse, path
from django.forms import ModelForm
from django.core.validators import MaxValueValidator
from django.db import models
from core import widgets

from core.templatetags.extras import markdown_to_html
from django.utils.html import mark_safe
from django.template.response import TemplateResponse
from django.http import HttpResponseForbidden

import datetime

from core.admin import PublishableAdmin


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['name', 'email', 'school', 'underground', 'contest']
    search_fields = ['name', 'school','user__first_name','user__last_name','user__email','second_member','third_member','fourth_member']
    fields = ['user','contest','name','email','school','underground', 'first_member','second_member','third_member','fourth_member','photo']
    readonly_fields = ['first_member','email']
    list_filter = [('contest',admin.RelatedOnlyFieldListFilter)]
    autocomplete_fields = ['user']


@admin.register(Contest)
class ContestAdmin(PublishableAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['order', 'year', 'is_public', 'view_collections_link', 'file_management']
    fields = ['order','year','start','published','public_from','public_to','scoreboard_html','stats_html']

    ordering = ('-order',)

    def view_collections_link(self, obj):
        count = obj.task_collections.count()
        link_url = (
                reverse("admin:contest_taskcollection_changelist")
                + "?"
                + urlencode({"contest__id": f"{obj.id}"})
        )
        return format_html('<a href="{}">{} sérií</a>', link_url, count)

    view_collections_link.short_description = "série"

    def file_management(self, obj):
        upload_form = render_to_string('mathrace/parts/contest_file_upload.html', {'contest': obj})
        return format_html(upload_form)
    file_management.short_description = "soubory"


class TaskInline(admin.TabularInline):
    model = Task

    fields = [
        'order',
        'collection',
        'name',
        'solution_value',
        'text',
        'model_solution',
    ]


@admin.register(TaskCollection)
class TaskCollectionAdmin(PublishableAdmin):
    change_list_template = 'smuggler/change_list.html'
    fields = ['contest','order','name','close_date','published','public_from','public_to','ready','release_solution',
        'assignment_pdf','solution_pdf']
    readonly_fields = ['ready']
    list_display = ['__str__', 'is_public', 'close_date', 'ready', 'release_solution', 'view_tasks_link', 'file_management']
    list_filter = [('contest',admin.RelatedOnlyFieldListFilter)]

    ordering = ('-contest__order', '-order')

    inlines = [
        TaskInline,
    ]

    def file_management(self, obj):
        upload_form = render_to_string('mathrace/parts/collection_file_upload.html', {'collection': obj})
        return format_html(upload_form)

    def view_tasks_link(self, obj):
        count = obj.tasks.count()
        link_url = (
                reverse("admin:mathrace_task_changelist")
                + "?"
                + urlencode({"collection__id": f"{obj.id}"})
        )
        return format_html('<a href="{}">{} úloh</a>', link_url, count)

    file_management.short_description = "soubory"
    view_tasks_link.short_description = "úlohy"

    def save_model(self, request, obj, form, change):
        super(TaskCollectionAdmin,self).save_model(request, obj, form, change)
        if (obj.is_public() or obj.public_from) and not obj.ready():
            messages.add_message(request, messages.WARNING, "Pozor, série úloh {} není připravena pro zadání.".format(obj))


class Form(ModelForm):
    def __init__(self, *args, **kwargs):
        super(Form, self).__init__(*args, **kwargs)
        if 'instance' in kwargs:
            self.fields['score'].validators.append(MaxValueValidator(kwargs['instance'].task.max_score))


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    change_list_template = 'smuggler/change_list.html'
    fields = [
        'name',
        'collection',
        'contest',
        'order',
        'ready',
        'solution_value',
        'text',
        'model_solution',
        #'comment',
        #'comment_preview',
    ]
    readonly_fields = ['contest']
    list_display = ['__str__', 'ready']

    formfield_overrides = {
        models.TextField: {'widget': widgets.MarkdownTextField}
    }

    search_fields = ['name']
    list_filter = [('collection__contest',admin.RelatedOnlyFieldListFilter),('collection',admin.RelatedOnlyFieldListFilter)]
    ordering = ('-collection__contest__order','-collection__order', 'order')

    def contest(self,obj):
        try:
            return obj.collection.contest
        except:
            return "-"
    contest.short_description = "ročník"

"""
class Stats(models.Model):
    class Meta:
        verbose_name = "statistika"
        verbose_name_plural = "statistika"
        #proxy = True


@admin.register(Stats)
class StatsAdmin(admin.ModelAdmin):
    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('', self.stats),
        ]
        return my_urls + urls

    def stats(self, request):
        if not request.user.is_staff:
            return HttpResponseForbidden("")

        context = dict(
            self.admin_site.each_context(request)
        )

        g1=0
        g2=0
        g3=0
        g4=0
        for contestant in Contestant.current_contest():
            if contestant.grade()==1:
                g1+=1
            if contestant.grade()==2:
                g2+=1
            if contestant.grade()==3:
                g3+=1
            if contestant.grade()==4:
                g4+=1

        current_contest = Contest.objects.filter(contest_type=1).all().order_by('order').last();
        tasks_scores = []
        for task in Task.objects.filter(collection__contest = current_contest).order_by("collection__order","order"):
            s = 0
            n = 0
            for submission in TaskFileSubmission.objects.filter(task = task):
                try:
                    s += submission.score
                    n += 1
                except:
                    pass
            tasks_scores.append({"name": task.name, "collection": task.collection.order, "score": s/n if n>0 else "-"})

        collections = []
        for collection in TaskCollection.objects.filter(contest = current_contest):
            contestants = set()
            for submission in TaskFileSubmission.objects.filter(task__collection = collection):
                contestants.add(submission.contestant)

            submissions = TaskFileSubmission.objects.filter(task__collection = collection).count()

            collections.append({"order": collection.order, "contestants": len(contestants), "submissions": submissions})

        tasks_submissions = []
        for task_order in range(1,8):
            _collections = []
            for task in Task.objects.filter(collection__contest = current_contest, order = task_order).order_by("collection__order","order"):
                submissions = TaskFileSubmission.objects.filter(task = task).count()
                _collections.append({"order": task.collection.order, "submissions": submissions})

            tasks_submissions.append({"name": task_order, "collections": _collections})

        context["this_contest_number"] = len(Contestant.current_contest())
        context["tasks_scores"] = tasks_scores
        context["bygrade"] = {"g1": g1, "g2": g2, "g3": g3, "g4": g4}
        context["collections"] = collections
        context["tasks_submissions"] = tasks_submissions

        return TemplateResponse(request, "core/parts/stats.html", context)

    def has_add_permission(self, request, obj=None):
        return False
    
    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False
"""

@admin.register(Attempt)
class AttemptAdmin(admin.ModelAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['id', 'team', 'task', 'answer', 'converted_time']
    fields = ['team', 'task', 'answer', 'time', 'converted_time']
    readonly_fields = ['converted_time']
    list_filter = [('team',admin.RelatedOnlyFieldListFilter),('task',admin.RelatedOnlyFieldListFilter)]

    def converted_time(self,obj):
        try:
            return datetime.timedelta(seconds=obj.time)
        except:
            return 0
    converted_time.short_description = "čas"

    ordering = ('-time',)


@admin.register(ContestStatus)
class ContestStatusAdmin(admin.ModelAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['id', 'team', 'task', 'solved', 'wrong_attempts', 'converted_time']
    fields = ['team', 'task', 'solved', 'wrong_attempts', 'time', 'converted_time']
    readonly_fields = ['converted_time']
    list_filter = [('team',admin.RelatedOnlyFieldListFilter),('task',admin.RelatedOnlyFieldListFilter)]

    def converted_time(self,obj):
        try:
            return datetime.timedelta(seconds=obj.time)
        except:
            return 0
    converted_time.short_description = "čas"

    ordering = ('task','team')
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash, login, authenticate, logout, get_user_model
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import redirect, render

from contest.forms import ContestantForm, ContestantFormNoGDPR
from mathrace.models import Contest
from core.forms import RegisterForm, AccountForm

import json

from django.utils.http import urlsafe_base64_decode
from django.contrib.auth.tokens import default_token_generator

User = get_user_model()


@login_required()
def page_account(request,tag):
    """
    Renders a users account overview including task submissions and evaluations.

    Template: contest/account.html

    Privacy policy: LOGIN_REQUIRED

    :param request: HTTP request
    :return: HTTP response
    """

    context = {}
    try:
        current_contest = Contest.objects.order_by('order').last().order
        context['msg'] = None
        if tag == "updated": context['msg'] = "Nastavení bylo uloženo."
        if tag == "pwdch": context['msg'] = "Heslo bylo změněno."
    except ObjectDoesNotExist:
        pass

    return render(request, 'contest/account.html', context)


def page_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            if 'text_only' in request.GET:
                return HttpResponse('Uživatel přihlášen.')
            else:
                return redirect('mathrace:index')
        else:
            if 'text_only' in request.GET:
                return HttpResponseBadRequest('Přihlašovací jméno nebo heslo není správné.')
            else:
                return render(request, "contest/login.html", {'navbar_absolute_pos': True})
    else:
        return render(request, "contest/login.html", {'navbar_absolute_pos': True})


@login_required()
def handler_logout(request):
    logout(request)
    return redirect("mathrace:index")


def page_register(request):
    if request.method == "POST":
        registration_form = RegisterForm(request.POST)
        if registration_form.is_valid():
            user = registration_form.save()
            login(request, user)
            request.user.send_verification(request)
            return redirect('mathrace:verification_info')
        else:
            return render(request, 'contest/register.html',
                          {
                              'registration_form': registration_form,
                          })
    else:
        registration_form = RegisterForm()
        return render(request, 'contest/register.html',
                      {
                          'registration_form': registration_form,
                      })


def page_update_account(request):
    try:
        request.user.contestant
        return page_update_account_contestant(request)
    except:
        return page_update_account_other(request)


def page_update_account_contestant(request):
    f = open("data/storage/public/list_of_schools.json")
    schools = json.load(f)
    print("contestant")

    if request.method == "POST":
        account_form = AccountForm(request.POST, instance=request.user)
        contestant_form = ContestantFormNoGDPR(request.POST, instance=request.user.contestant)
        if account_form.is_valid() and contestant_form.is_valid():
            user = account_form.save()
            contestant = contestant_form.save()
            return redirect('mathrace:account_tagged',tag="updated")
        else:
            return render(request, 'contest/update_account.html',
                          {
                              'account_form': account_form,
                              'contestant_form': contestant_form,
                              'schools': schools,
                              'init_school': request.user.contestant.school,
                              'init_math_class': request.user.contestant.math_class,
                          })
    else:
        account_form = AccountForm(instance=request.user)
        contestant_form = ContestantFormNoGDPR(instance=request.user.contestant)
        return render(request, 'contest/update_account.html',
                      {
                          'account_form': account_form,
                          'contestant_form': contestant_form,
                          'schools': schools,
                          'init_school': request.user.contestant.school,
                          'init_math_class': request.user.contestant.math_class,
                      })


def page_update_account_other(request):
    
    if request.method == "POST":
        account_form = AccountForm(request.POST, instance=request.user)
        if account_form.is_valid():
            user = account_form.save()
            return redirect('mathrace:account_tagged',tag="updated")
        else:
            return render(request, 'contest/update_account.html',
                          {
                              'account_form': account_form,
                          })
    else:
        account_form = AccountForm(instance=request.user)
        return render(request, 'contest/update_account.html',
                      {
                          'account_form': account_form,
                      })
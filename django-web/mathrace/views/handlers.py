from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.http import Http404, HttpResponse, HttpResponseForbidden
from django.shortcuts import render, get_object_or_404
from django.template.loader import select_template
from django.core.paginator import Paginator
from django.shortcuts import redirect, render
from django.utils import timezone
from django.db.models import Prefetch

from mathrace.forms import RegisterTeamForm, RegisterTeamFormNoGDPR
from mathrace.models import Contest,TaskCollection, Task, Team
from core.forms import UploadForm
from core.models import MediaFile, Organizer, ContestType
from forum.models import Contribution
from django.views.generic import TemplateView

from core.views import EditablePage, render_editable
from core.utils import frinedly_base62_encode

import json

"""
Views
"""


@staff_member_required(redirect_field_name='/')
def collection_file_upload(request, collection_id):
    form = UploadForm(['pdf'],4,request.POST,request.FILES)
    if not form.is_valid():
        return HttpResponseForbidden(form.errors["file"])

    f = request.FILES['file']
    collection = get_object_or_404(TaskCollection, pk=collection_id)
    file_type = request.GET["file_type"]
    attrname = f"{file_type}_pdf"
    target_filename = f'mathrace/{file_type}_{collection.contest.order}_{collection.order}'

    collection.upload_file(attrname, f, target_filename, [request.user], 1)

    return HttpResponse('Soubor byl úspěšně nahrán.')


@staff_member_required(redirect_field_name='/')
def contest_file_upload(request, contest_id):
    form = UploadForm(['html'],4,request.POST,request.FILES)
    if not form.is_valid():
        return HttpResponseForbidden(form.errors["file"])

    f = request.FILES['file']
    contest = get_object_or_404(Contest, pk=contest_id)
    file_type = request.GET["file_type"]
    attrname = f"{file_type}_html"
    target_filename = f'mathrace/{file_type}_{contest.order}'

    contest.upload_file(attrname, f, target_filename, [request.user], 0)

    return HttpResponse('Soubor byl úspěšně nahrán.')


@login_required(redirect_field_name='/')
def delete_team_photo(request):

    team = Team.active_team(request)
    team.photo.delete()
    team.photo = None
    team.save()

    return HttpResponse('Týmová fotka byla odstraněna.')


@login_required(redirect_field_name='/')
def upload_team_photo(request):
    team = Team.active_team(request)

    form = UploadForm(['jpg','jpeg','png'],4,request.POST,request.FILES)
    if not form.is_valid():
        return HttpResponseForbidden(form.errors["file"])

    try:
        team.photo.delete()
    except:
        pass

    f = request.FILES['file']

    id62 = frinedly_base62_encode(team.id)

    media_file = MediaFile.objects.create(
        content=f,
        access=0,
        name="team_photos/"+id62
    )

    media_file.owners.add(request.user)

    team.photo = media_file
    team.save()

    return HttpResponse('Týmová fotka byla úspěšně nahrána.')
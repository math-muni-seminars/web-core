from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.views.generic import TemplateView
from django.utils.html import format_html

from mathrace.models import Contest, TaskCollection
from core.models import PublishableModel
from core.utils import int_to_roman

"""
Helpers
"""


def render_latex_results(request, head, res, contest, collection=None, cumulative=False):
    template_name = 'contest/latex/results.tex'

    contest_year_roman = int_to_roman(contest.order)
    if cumulative:
        title = 'Výsledky po {}. sérii {}. ročníku'.format(collection.order, contest_year_roman)
    else:
        title = 'Výsledky {}. série {}. ročníku'.format(collection.order, contest_year_roman)

    column_definition = '|r|l|c|l|' + 'c|' * (len(head)-4)

    context = {
        'collection': collection,
        'contest': contest,
        'head': head,
        'results': res,
        'column_definition': column_definition,
        'title': title,
    }

    return HttpResponse(render_to_string(template_name, context), content_type='text/plain')


"""
Views
"""


def find_collection(contest_order=None, collection_order=None):

    try:
        if contest_order is None:
            contest = Contest.public_objects()\
                .order_by('-order').first()
        else:
            contest = Contest.public_objects().get(contest_type=contest_type,
                                                   order=contest_order)
    except (ObjectDoesNotExist, IndexError):
        raise Http404("Soutěž nenalezena.")

    if collection_order is None:
        collection = TaskCollection.current_collection_with_results(contest_type)
    else:
        collection = contest.task_collections.filter(PublishableModel.q_public(), order=collection_order).last()
    if collection is None:
        raise Http404("Pro tuto soutěž zatím není zveřejněna žádná série.")

    return contest, collection


class PageResults(TemplateView):
    template_name = 'mathrace/results.html'

    def get_context_data(self, **kwargs):
        contest_order = kwargs.get('contest_order', None)
        if contest_order is None:
            try: fr = open("core/static/mathrace/results.html").read()
            except: fr = None

            try: fs = open("core/static/mathrace/stats.html").read()
            except: fs = None

            return {"results_html": fr, "stats_html": fs, "contest_order": "aktuálního"}
        else:
            try:
                contest = Contest.objects.get(order=contest_order)
            except:
                raise Http404("Ročník nenalezen")

            try: fr = contest.scoreboard_html.content.open("r").read()
            except: fr = None

            try: fs = contest.scoreboard_html.content.open("r").read()
            except: fs = None

            return {"results_html": fr, "stats_html": fs, "contest_order": int_to_roman(int(contest_order))+"."}



def handler_latex_results_cumulative(request, id):
    collection = get_object_or_404(TaskCollection, pk=id)
    head, res = ResultsTable(collection.contest, collection).get_results_cumulative()
    return render_latex_results(request, head, res, collection.contest, collection, cumulative=True)


def handler_latex_results(request, id):
    collection = get_object_or_404(TaskCollection, pk=id)
    head, res = ResultsTable(collection.contest, collection).get_results_collection()
    return render_latex_results(request, head, res, collection.contest, collection)

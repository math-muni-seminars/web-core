from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.http import Http404, HttpResponse, HttpResponseForbidden
from django.shortcuts import render, get_object_or_404
from django.template.loader import select_template
from django.core.paginator import Paginator
from django.shortcuts import redirect, render
from django.utils import timezone
from django.db.models import Prefetch

from mathrace.forms import RegisterTeamForm, RegisterTeamFormNoGDPR
from mathrace.models import Contest,TaskCollection, Task, Team
from core.forms import UploadForm
from core.models import MediaFile, Organizer, ContestType
from forum.models import Contribution
from django.views.generic import TemplateView

from core.views import EditablePage, render_editable
from django.forms import Form

import json

"""
Views
"""


class PageRules(EditablePage):
    template_name = "mathrace/rules.html"


class PageOldContribs(EditablePage):
    template_name = "contest/old_contribs.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        try:
            contest_type = ContestType.objects.get(abbreviation="mathrace")
        except ObjectDoesNotExist:
            raise Http404("Typ soutěže nenalezen.")

        p = Paginator(Contribution.public_objects().filter(contest_type=contest_type),20)
        page_no = kwargs.get('page_no', 1)

        context['contribs'] = p.page(page_no).object_list
        context['page_no'] = page_no
        context['pages'] = p.page_range

        return context

"""
def page_register_team(request,tag):
    team = Team.active_team(request)

    if team is None:
        f = open("core/static/core/files/list_of_schools.json")
        schools = json.load(f)
            
        contest = Contest.public_objects().order_by('order').last()
        try:
            contest_start = TaskCollection.objects.filter(contest=contest).order_by('order').first().public_from
            reg_closed = contest_start < timezone.now()
        except:
            reg_closed = False

        if request.method == "POST":
            registration_form = RegisterTeamForm(request.POST)
            if reg_closed:
                raise Http404()
            if registration_form.is_valid():
                teamO = registration_form.save()
                teamO.user = request.user
                teamO.contest = Contest.objects.all().order_by('order').last()
                teamO.save()
                return redirect('mathrace:register_team_tagged', tag="reg")
            else:
                return render_editable(request, 'mathrace/register_team.html',
                            {
                                'team': team,
                                'registration_form': registration_form,
                                'schools': schools,
                                'reg_closed': reg_closed,
                                'msg': "Nastavení bylo uloženo." if tag=="updated" else None,
                            })
        else:
            registration_form = RegisterTeamForm()
            return render_editable(request, 'mathrace/register_team.html',
                            {
                                'team': team,
                                'registration_form': registration_form,
                                'schools': schools,
                                'reg_closed': reg_closed,
                                'msg': "Nastavení bylo uloženo." if tag=="updated" else None,
                            })
        
    else:
        if team.third_member is None: team.third_member = ""
        if team.fourth_member is None: team.fourth_member = ""
        return render_editable(request, 'mathrace/register_team.html', {'team': team, 'msg': "Nastavení bylo uloženo." if tag=="updated" else None,})
"""

def page_update_team(request):
    team = Team.active_team(request)

    if request.method == "POST":
        registration_form = RegisterTeamFormNoGDPR(request.POST, request.FILES, instance=team)
        if registration_form.is_valid():
            teamO = registration_form.save()
            teamO.save()
            return redirect('mathrace:teams_tagged', tag="updated")
        else:
            return render(request, 'mathrace/update_team.html',
                        {
                            'registration_form': registration_form,
                        })
    else:
        registration_form = RegisterTeamFormNoGDPR(instance=team)
        return render(request, 'mathrace/update_team.html',
                        {
                            'registration_form': registration_form,
                        })

"""
class PageAboutUs(EditablePage):
    template_name = "mathrace/about_us.html"

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['active_organizers'] = Organizer.objects.filter(active=True)
        data['former_organizers'] = Organizer.objects.filter(active=False)
        return data
"""

def page_teams(request,tag):
    ordinary = Team.current_ordinary()
    underground = Team.current_underground()

    for team in ordinary:
        if team.third_member is None: team.third_member = ""
        if team.fourth_member is None: team.fourth_member = ""

    for team in underground:
        if team.third_member is None: team.third_member = ""
        if team.fourth_member is None: team.fourth_member = ""

    team = Team.active_team(request)

    if team is None:
        f = open("core/static/core/files/list_of_schools.json")
        schools = json.load(f)
            
        contest = Contest.public_objects().order_by('order').last()
        try:
            contest_start = TaskCollection.objects.filter(contest=contest).order_by('order').first().public_from
            reg_closed = contest_start < timezone.now()
        except:
            reg_closed = False

        if request.method == "POST":
            registration_form = RegisterTeamForm(request.POST)
            if reg_closed or not request.user.is_verified():
                return HttpResponseForbidden()
            if registration_form.is_valid():
                teamO = registration_form.save()
                teamO.user = request.user
                teamO.contest = Contest.objects.all().order_by('order').last()
                teamO.save()
                return redirect('mathrace:teams_tagged', tag="reg")
            else:
                return render_editable(request, 'mathrace/teams.html',
                            {
                                'team': team,
                                'registration_form': registration_form,
                                'schools': schools,
                                'reg_closed': reg_closed,
                                'msg': "Nastavení bylo uloženo." if tag=="updated" else None,
                                "ordinary": ordinary,
                                "underground": underground,
                            })
        else:
            registration_form = RegisterTeamForm()
            return render_editable(request, 'mathrace/teams.html',
                            {
                                'team': team,
                                'registration_form': registration_form,
                                'schools': schools,
                                'reg_closed': reg_closed,
                                'msg': "Nastavení bylo uloženo." if tag=="updated" else None,
                                "ordinary": ordinary,
                                "underground": underground,
                            })
        
    else:
        if team.third_member is None: team.third_member = ""
        if team.fourth_member is None: team.fourth_member = ""
        return render_editable(request, 'mathrace/teams.html', {
            'team': team,
            'msg': "Nastavení bylo uloženo." if tag=="updated" else None,
            "ordinary": ordinary,
            "underground": underground,
        })


def page_teams_old(request,contest_order):
    
    try:
        contest = Contest.objects.get(order=contest_order)
    except:
        raise Http404("Ročník nenalezen.")

    ordinary = Team.objects.filter(contest=contest,underground=False).all()
    underground = Team.objects.filter(contest=contest,underground=True).all()

    for team in ordinary:
        if team.third_member is None: team.third_member = ""
        if team.fourth_member is None: team.fourth_member = ""

    for team in underground:
        if team.third_member is None: team.third_member = ""
        if team.fourth_member is None: team.fourth_member = ""

    return render(request, "mathrace/teams_old.html", {
        "contest_order": contest_order,
        "ordinary": ordinary,
        "underground": underground,
    })


class PageIndex(EditablePage):
    # template_names = select_template([namesapce + '/index.html', 'contest/index.html'])

    template_name = "mathrace/index.html"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        
        try:
            contest_type = ContestType.objects.get(abbreviation="mathrace")
        except ObjectDoesNotExist:
            contest_type = None

        contest = Contest.public_objects().order_by('order').last()
        context['contest_start'] = contest.start

        context['contribs'] = Contribution.public_objects().filter(contest_type=contest_type)[:2]
        context['navbar_absolute_pos'] = True

        return context


# def page_index(request):
#     def get_context_data(self, **kwargs):
#         data = super().get_context_data(**kwargs)
#         namesapce = request.resolver_match.namespace
#         template = select_template([namesapce + '/index.html', 'contest/index.html'])
#
#         contest_type_abbreviation = request.resolver_match.namespace
#         try:
#             contest_type = ContestType.objects.get(abbreviation=contest_type_abbreviation)
#         except ObjectDoesNotExist:
#             contest_type = None
#         collection = TaskCollection.current_collection(contest_type) if contest_type is not None else None
#         contest = collection.contest if collection is not None else None
#         return {
#             'contribs': Contribution.public_objects()[:2],
#             'navbar_absolute_pos': True,
#             'active_contest': contest,
#             'active_collection': collection,
#         }
#
#     return HttpResponse(template.render(context, request))


def page_info(request):
    """
    Renders the info page from template. Does not add an extra context.

    template: contest/info.html

    Privacy policy: PUBLIC

    :param request: HTTP request
    :return: HTTP response
    """
    return render(request, 'contest/info.html')


def page_archive(request):
    contests = Contest.public_objects().order_by('-order')\
        .prefetch_related(Prefetch('task_collections',queryset=TaskCollection.public_objects()))
    context = {
        'contests': contests
    }

    return render(request, 'mathrace/archive.html', context=context)
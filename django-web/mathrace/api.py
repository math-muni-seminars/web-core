from django.urls import path

import contest.views as views
from contest.views import tasks, results

urlpatterns = [
    path('collection_file/<int:collection_id>', views.handler_collection_file_upload, name='collection_file_upload'),
    path('task/feedback/<int:submission_id>', views.handler_feedback_upload, name='feedback_upload'),
    path('tasks/submissions_zip/<int:task_id>', tasks.handler_task_submissions_zip, name='task_submissions_zip'),
    path('latex/tasks/<int:id>', tasks.handler_task_collection_latex, name='tasks_latex'),
    path('latex/solutions/<int:id>', tasks.handler_solutions_latex, name='solutions_latex'),
    path('latex/results/<int:id>/cumulative', results.handler_latex_results_cumulative, name='latex_results_cumulative'),
    path('latex/results/<int:id>', results.handler_latex_results, name='latex_results'),
]

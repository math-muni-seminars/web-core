from itertools import groupby

from django.db.models import Q
from django.utils import timezone

from django.contrib.auth import get_user_model
from django.db import models
from django.utils.html import format_html
from django.core.exceptions import ValidationError
from django.utils.crypto import get_random_string

from core.models import PublishableModel, BaseModel, MediaFile

User = get_user_model()


class Contest(PublishableModel):
    class Meta:
        verbose_name = "ročník"
        verbose_name_plural = "ročníky"

    order = models.PositiveSmallIntegerField("pořadí", default=1, unique=True)
    year = models.PositiveSmallIntegerField("rok")
    start = models.DateTimeField("datum a čas zahájení", help_text="<p>Od této chvíle se bude počítat soutěžní čas</p>")

    scoreboard_html = models.ForeignKey(MediaFile,
                                        on_delete=models.SET_NULL,
                                        verbose_name='výsledky html',
                                        related_name='mathrace_scoreboard_for',
                                        null=True, blank=True)

    stats_html = models.ForeignKey(MediaFile,
                                        on_delete=models.SET_NULL,
                                        verbose_name='statistika html',
                                        related_name='mathrace_stats_for',
                                        null=True, blank=True)

    def __str__(self):
        return "{0}".format(self.order)


class TaskCollection(PublishableModel):
    class Meta:
        verbose_name = "série úloh"
        verbose_name_plural = "série úloh"

        unique_together = ('order', 'contest')

    order = models.PositiveSmallIntegerField("pořadí")
    close_date = models.DateTimeField("datum uzavření", help_text="<p>Po tomto datu nebude možné odevzdávat řešení</p>")
    name = models.CharField("název", max_length=200)
    contest = models.ForeignKey(Contest,
                                on_delete=models.SET_NULL,
                                verbose_name='ročník',
                                related_name='task_collections',
                                default=None,
                                null=True,
                                )

    release_solution = models.BooleanField("zveřejnit řešení", default=False)

    assignment_pdf = models.ForeignKey(MediaFile,
                                          on_delete=models.SET_NULL,
                                          verbose_name='zadání pdf',
                                          related_name='mathrace_assignment_for',
                                          null=True, blank=True)

    solution_pdf = models.ForeignKey(MediaFile,
                                        on_delete=models.SET_NULL,
                                        verbose_name='řešení pdf',
                                        related_name='mathrace_solution_for',
                                        null=True, blank=True)

    def __str__(self):
        return "{0}-{1} {2}".format(self.contest,self.order,self.name)

    """
    @classmethod
    def open_collections(cls):
        ## returns all open collections
        return cls.public_objects().filter(close_date__gte=timezone.now())

    @classmethod
    def closed_collections(cls):
        ## returns all closed collections
        return cls.public_objects().filter(close_date__lt=timezone.now())
    """

    @staticmethod
    def q_open():
        return Q(PublishableModel.q_public(), close_date__gte=timezone.now())

    @staticmethod
    def q_closed():
        return Q(PublishableModel.q_public(), close_date__lt=timezone.now())

    def is_open(self):
        return self.close_date >= timezone.now()

    def is_closed(self):
        return self.close_date < timezone.now()

    """
    @staticmethod
    def latest_closed_collection(contest_type):
        return TaskCollection.closed_collections().filter(contest__contest_type=contest_type) \
            .select_related('contest').order_by('contest__order', 'order').last()

    @staticmethod
    def first_open_collection(contest_type):
        return TaskCollection.open_collections().filter(contest__contest_type=contest_type) \
            .select_related('contest').order_by('contest__order', 'order').first()

    @staticmethod
    def latest_results_collection(contest_type):
        return TaskCollection.closed_collections().filter(contest__contest_type=contest_type, release_results=True) \
            .select_related('contest').order_by('contest__order', 'order').last()
    """

    @staticmethod
    def current_collection():
        return TaskCollection.public_objects().order_by('contest__order','-order').last()

    """
    @staticmethod
    def current_collection_with_results(contest_type):
        collection = TaskCollection.latest_results_collection(contest_type)
        if collection is None:
            collection = TaskCollection.latest_closed_collection(contest_type)
        return collection
    """

    def ready(self):
        for task in Task.objects.filter(collection=self):
            if not task.ready:
                return False
        return True
        
    ready.short_description = "úlohy připraveny pro zadání"
    ready.boolean = True


class Task(BaseModel):
    class Meta:
        verbose_name = "úloha"
        verbose_name_plural = "úlohy"

        unique_together = ('order', 'collection')

    order = models.SmallIntegerField("pořadí", null=True, blank=True)
    collection = models.ForeignKey(TaskCollection,
                                   on_delete=models.CASCADE,
                                   verbose_name='série',
                                   related_name='tasks',
                                   null=True, blank=True,
                                   )
    name = models.CharField("název", max_length=200)

    text = models.TextField("text úlohy", null=True, blank=True)

    model_solution = models.TextField("vzorové řešení", null=True, blank=True)
    solution_value = models.CharField("hodnota řešení", max_length=200, null=True, blank=True, help_text="""
        <p>Více variant řešení oddělte <code>|</code>, nepoužívejte žádné mezery naví</p>
        <p>Např.: <code>reseni|RESENI</code></p>
        """)

    #comment = models.TextField("komentář ($)", null=True, blank=True)

    ready = models.BooleanField("připraveno pro zadání", default=False)

    def __str__(self):
        if self.collection is None: return self.name
        return "{0}-{1}-{2}".format(self.collection.contest, self.collection.order, self.name)

    def clean(self):
        if self.ready:
            errors = ""

            if self.text is None or self.text=="":
                errors += 'Chybí text úlohy. '
            if self.solution_value is None or self.solution_value=="":
                errors += 'Chybí hodnota řešení. '
                
            if errors != "":
                raise ValidationError({'ready':errors.rstrip(" ")})


class Team(models.Model):
    """
    Extends the user model by adding the contestant data.
    """

    class Meta:
        verbose_name = "tým"
        verbose_name_plural = "týmy"

    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    name = models.CharField("název", max_length=30)
    underground = models.BooleanField("underground", default=False)
    contest = models.ForeignKey(Contest,
                                   on_delete=models.SET_NULL,
                                   verbose_name='ročník',
                                   related_name='contest',
                                   null=True)
    school = models.CharField("škola", max_length=250, null=True)
    second_member = models.CharField("jméno a příjmení 2. člena", max_length=60)
    third_member = models.CharField("jméno a příjmení 3. člena", max_length=60, null=True, blank=True)
    fourth_member = models.CharField("jméno a příjmení 4. člena", max_length=60, null=True, blank=True)
    photo = models.ForeignKey(MediaFile,
                                on_delete=models.SET_NULL,
                                verbose_name='týmová fotka',
                                null=True, blank=True)
    last_wrong_attempt = models.PositiveSmallIntegerField("poslední pokus", null=True)
    token = models.CharField("token", max_length=32, default=get_random_string(length=32))
    solved_count = models.PositiveSmallIntegerField("počet vyřešených úloh", default=0)
    decisive_time = models.PositiveIntegerField("rozhodný čas", default=0)

    def is_current(self):
        return contest == Contest.objects.filter(contest_type=2).order_by("order").last()

    def __str__(self):
        return self.name

    def first_member(self):
        return self.user.full_name()

    first_member.short_description = 'jméno a příjmení 1. člena'

    def email(self):
        return self.user.email

    email.short_description = 'kontaktní email'

    """
    def get_school(self):
        if(self.underground): return "underground"
        else:
            try:
                return self.user.contestant.school
            except:
                return ""

    get_school.short_description = "škola"
    """

    @staticmethod
    def current_contest():
        contest = Contest.public_objects().order_by('order').last()
        return Team.objects.filter(contest=contest)

    @staticmethod
    def current_ordinary():
        contest = Contest.public_objects().order_by('order').last()
        return Team.objects.filter(contest=contest, underground=False)

    @staticmethod
    def current_underground():
        contest = Contest.public_objects().order_by('order').last()
        return Team.objects.filter(contest=contest, underground=True)

    @staticmethod
    def active_team(request):
        try:
            contest = Contest.public_objects().order_by('order').last()
            return Team.objects.filter(contest=contest, user=request.user).first()
        except:
            return None


class Attempt(models.Model):
    class Meta:
        verbose_name = "pokus"
        verbose_name_plural = "pokusy"

    team = models.ForeignKey(Team, on_delete=models.SET_NULL, null=True)
    task = models.ForeignKey(Task, on_delete=models.SET_NULL, null=True)
    answer = models.CharField("odpověď", max_length=60)
    time = models.PositiveSmallIntegerField("čas")


class ContestStatus(models.Model):
    class Meta:
        verbose_name = "stav soutěže"
        verbose_name_plural = "stav soutěže"
        unique_together = ("team","task")

    team = models.ForeignKey(Team, on_delete=models.SET_NULL, null=True, related_name="contest_status")
    task = models.ForeignKey(Task, on_delete=models.SET_NULL, null=True, related_name="contest_status")
    solved = models.BooleanField("vyřešeno", default=False)
    wrong_attempts = models.PositiveSmallIntegerField("chybné pokusy", default=0)
    time = models.PositiveSmallIntegerField("čas", null=True, blank=True)
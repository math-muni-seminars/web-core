from django.urls import path, include
from mathrace import views
from core.views import page_static
from mathrace.views import users, tasks, results, handlers
from contest.views import users as br_users

app_name = 'mathrace'

urlpatterns = [
    path('login/', views.users.page_login, name='login'),
    path('logout/', views.users.handler_logout, name='logout'),
    path('register/', views.users.page_register, name='register'),
    path('reset_password/', br_users.page_reset_password, name='reset_password'),
    path('reset_password/confirm/<uidb64>/<token>/', br_users.page_reset_password_confirm, name='reset_password_confirm'),
    path('reset_password/done/', br_users.page_reset_password_done, name='reset_password_done'),

    path('verification/info', br_users.PageVerificationInfo.as_view(tags=['verification']), name='verification_info'),
    path('verification/send', br_users.page_verification_send, name='verification_send'),
    path('verification/verify/<uidb64>/<str:token>/', br_users.page_verification_verify, name='verification_verify'),

    
    path('account/view_change_password/', br_users.page_change_password, name='change_password'),
    path('account/', lambda request: views.users.page_account(request,None), name='account'),
    path('account/<str:tag>', views.users.page_account, name='account_tagged'),
    path('update_account/', views.users.page_update_account, name='update_account'),

    path('forum/', include('forum.urls')),
    
    path('', views.PageIndex.as_view(tags=['claim', 'subclaim', 'intro']), name='index'), #template_name='contest/index.html', 
    path('archive/', views.page_archive, name='archive'),
    
    path('teams/', lambda request: views.page_teams(request,None), name='teams'),
    path('teams/<int:contest_order>', views.page_teams_old, name='teams_old'),
    path('teams/<str:tag>', views.page_teams, name='teams_tagged'),
    #path('register_team/', lambda request: views.page_register_team(request,None), name='register_team'),
    #path('register_team/<str:tag>', views.page_register_team, name='register_team_tagged'),
    path('update_team/', views.page_update_team, name='update_team'),
    path('update_team/delete_photo', handlers.delete_team_photo, name='delete_team_photo'),
    path('update_team/upload_photo', handlers.upload_team_photo, name='upload_team_photo'),

    #path('tasks/<int:contest_order>/<int:collection_order>/<int:task_order>', views.tasks.page_task, name='task'),
    path('tasks/<int:contest_order>/<int:collection_order>', views.tasks.page_task_collection, name='tasks'),
    path('tasks/', views.tasks.page_task_collection_current, name='tasks_newest'),

    #path('solutions/<int:contest_order>/<int:collection_order>', views.tasks.page_sample_solutions, name='sample_solutions'),

    path('results/', views.results.PageResults.as_view(), name='results'),
    path('results/<int:contest_order>', views.results.PageResults.as_view(), name='old_results'),
    
    path('old_contribs/', views.PageOldContribs.as_view(), name='old_contribs_def'),
    path('old_contribs/<int:page_no>', views.PageOldContribs.as_view(), name='old_contribs'),
    path('rules/', views.PageRules.as_view(tags=['rules']), name='rules'),
    #path('about/', views.PageAboutUs.as_view(tags=['about']), name='about'),

    path('page/<path:path>', page_static, name='static_page'),
]
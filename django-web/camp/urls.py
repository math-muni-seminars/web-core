from django.urls import path
from camp import views

app_name = 'camp'

urlpatterns = [
    path('', views.PageCamp.as_view(tags=['pretext']), name='camp'),
    path('more', views.PageCamp.as_view(tags=['pretext'],more=True), name='camp_more'),
]
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist, SuspiciousOperation
from django.db import IntegrityError
from django.http import Http404
from django.shortcuts import render
import datetime

from camp.forms import ApplicationForm
from core.views import EditablePage
from .models import Camp, CampApplication
from django.utils import timezone


class PageCamp(EditablePage):
    template_name = "camp/camp_page.html"
    more = False

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        try:
            camp = Camp.objects.filter(end__gte=timezone.now())[0]
            context['camp'] = camp
            context['application_list'] = CampApplication.objects.filter(camp=camp).order_by('created')
        except IndexError:
            camp = None

        if self.request.method == "GET":
            try:
                context['own_application'] = camp.participants.filter(participant=self.request.user).get()
            except Exception:
                pass

            if camp is not None and camp.registration_opened() and self.request.user.is_verified() and not 'own_application' in context:
                context['form'] = ApplicationForm()

        if self.more:
            old_camps = Camp.objects.filter(end__lt=timezone.now())
        else:
            old_camps = Camp.objects.filter(end__lt=timezone.now())[0:2]
            context['link_more'] = True

        for old_camp in old_camps:
            old_camp.start = old_camp.start.strftime("%-d. %-m.")
            old_camp.end = old_camp.end.strftime("%-d. %-m. %Y")

        context['old_camps'] = old_camps

        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)

        if not context['camp'].registration_opened():
            raise SuspiciousOperation("Registrace není otevřena.")

        application_form = ApplicationForm(self.request.POST)
        if application_form.is_valid() and self.request.user.verified:
            application = application_form.save(commit=False)
            application.participant = self.request.user
            application.camp = context['camp']
            try:
                application.save()
            except IntegrityError:
                messages.error(self.request, 'Registrace se nezdařila.')
                messages.debug(self.request, 'Na toto soustředění jsi již registrovaný.')
                context['form'] = application_form
                return render(request, 'camp/camp_page.html', context)

            context['own_application'] = application
            return render(request, 'camp/camp_page.html', context)
        else:
            messages.error(self.request, 'Registrace se nezdařila.')
            messages.debug(self.request, application_form.errors)
            context['form'] = application_form
            return render(request, 'camp/camp_page.html', context)


from django.contrib import admin
from .models import Camp, CampApplication

import csv
from django.http import HttpResponse

def export_as_csv(self, request, queryset):
    camp = queryset[0]

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=ucastnici.csv'
    writer = csv.writer(response,delimiter=";",quotechar=";")

    head = ["Jméno","Email","Škola"]
    field_names = []
    for field in CampApplication._meta.get_fields()[5:]:
        head.append(field.verbose_name)
        field_names.append(field.name)
    writer.writerow(head)
    writer.writerow([])

    for obj in CampApplication.objects.filter(camp=camp).order_by('created'):
        row = [obj.participant.full_name(), obj.participant.email, obj.participant.contestant.school if hasattr(obj.participant,"contestant") else ""]
        for field in field_names:
            row.append(getattr(obj, field))
        writer.writerow(row)

    return response

export_as_csv.short_description = "Stáhnout tabulku účastníků v CSV"

@admin.register(Camp)
class CampAdmin(admin.ModelAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['name', 'registration_to', 'num_of_participants', 'num_of_accepted_participants']
    actions = [export_as_csv]


def accept(self, request, queryset):
    queryset.update(status="ACC")

accept.short_description = "Označnení > přijatí"

def tosub(self, request, queryset):
    queryset.update(status="SUB")

tosub.short_description = "Označnení > náhradníci"

@admin.register(CampApplication)
class CampApplicationAdmin(admin.ModelAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['get_name', 'get_email', 'status', 'sub_order', 'camp']
    list_filter = [('camp',admin.RelatedOnlyFieldListFilter),]
    fields = ['camp','participant','get_name','status','sub_order','get_school','get_email','phone_number','attendance_entire','attendance_spec',
            'transport_common','transport_spec','discount_card','medicaments','medicaments_spec','allergies','allergies_spec','diets','diets_spec',
            'adult','parent_phone_number']
    readonly_fields = ['get_name','get_school','get_email']
    actions = [accept, tosub]
    list_editable = ['status','sub_order']
    radio_fields = {'status': admin.HORIZONTAL}
    autocomplete_fields = ['participant']

    def get_name(self, obj):
        return obj.participant.full_name()

    def get_email(self, obj):
        return obj.participant.email

    def get_school(self, obj):
        try:
            return obj.participant.contestant.school
        except:
            return "-"

    get_name.short_description = "jméno"
    get_email.short_description = "email"
    get_school.short_description = "škola"

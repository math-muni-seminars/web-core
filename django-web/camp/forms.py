from .models import CampApplication
from camp.widgets import CheckboxSpecifyWidget, TextSpecifyWidget
from django.core.exceptions import ValidationError

from core import forms


class ApplicationForm(forms.ModelForm):
    phone_number = forms.CharField(validators=[CampApplication.phone_regex], required=True, max_length=64, label="Telefon")
    attendance_entire = forms.BooleanField(widget=CheckboxSpecifyWidget({'bind_id':'attendance','condition':'no'}), required=False, label="Můžeš se zúčastnit celého soustředění?")
    attendance_spec = forms.CharField(widget=TextSpecifyWidget({'bind_id':'attendance','condition':'no'}), required=False, max_length=255, label="Které části soustředění se budeš moci zúčastnit?")
    transport_common = forms.BooleanField(widget=CheckboxSpecifyWidget({'bind_id':'transport','condition':'no'}), required=False, label="Pojedeš na soustředění společným odjezdem z Brna s částí organizátorů?")
    transport_spec = forms.CharField(widget=TextSpecifyWidget({'bind_id':'transport','condition':'no'}), required=False, max_length=255, label="Jak se na soustředění dopravíš?")
    discount_card = forms.BooleanField(required=False, label="Máš průkaz poskytující studentskou slevu na jízdné v hromadné dopravě?")
    medicaments = forms.BooleanField(widget=CheckboxSpecifyWidget({'bind_id':'medicaments','condition':'yes'}), required=False, label="Bereš pravidelně nějaké léky, o kterých bychom raději měli vědět?")
    medicaments_spec = forms.CharField(widget=TextSpecifyWidget({'bind_id':'medicaments','condition':'yes'}), required=False, max_length=255, label="Jaké?")
    allergies = forms.BooleanField(widget=CheckboxSpecifyWidget({'bind_id':'allergies','condition':'yes'}), required=False, label="Máš nějaká zdravotní omezení nebo alergie?")
    allergies_spec = forms.CharField(widget=TextSpecifyWidget({'bind_id':'allergies','condition':'yes'}), required=False, max_length=255, label="Jaké?")
    diets = forms.BooleanField(widget=CheckboxSpecifyWidget({'bind_id':'diets','condition':'yes'}), required=False, label="Dodržuješ nějaké diety či máš stravovací omezení (např. bezlepková dieta, vegetariánská strava, atd.)?")
    diets_spec = forms.CharField(widget=TextSpecifyWidget({'bind_id':'diets','condition':'yes'}), required=False, max_length=255, label="Jaké?")
    adult = forms.BooleanField(widget=CheckboxSpecifyWidget({'bind_id':'adult','condition':'no'}), required=False, label="Bude ti k datu začátku soustředění 18 nebo více let?")
    parent_phone_number = forms.CharField(widget=TextSpecifyWidget({'bind_id':'adult','condition':'no'}), validators=[CampApplication.phone_regex], required=False, max_length=64, label="Telefon na rodiče")
    gdpr = forms.BooleanField(required=True, label="Souhlasím s podmínkami zpracování osobních údajů")

    def __init__(self, *args, **kwargs):
        super(ApplicationForm, self).__init__(*args, **kwargs)
        for key in self.fields:
            self.fields[key].widget.attrs['secondary_class'] = "input-primary"

    class Meta:
        model = CampApplication
        fields = ['phone_number','attendance_entire','attendance_spec','transport_common','transport_spec','discount_card',
            'medicaments','medicaments_spec','allergies','allergies_spec','diets','diets_spec','adult','parent_phone_number']

    def clean(self):
        data = self.cleaned_data

        if not data.get('attendance_entire',False) and not data.get('attendance_spec',None):
            self.add_error('attendance_spec',forms.ValidationError('Upřesni prosím, které části soustředění se budeš moci zúčastnit'))
        if data.get('attendance_entire',False):
            data['attendance_spec'] = None

        if not data.get('transport_common',False) and not data.get('transport_spec',None):
            self.add_error('transport_spec',forms.ValidationError('Upřesni prosím, jak se na soustředění dopravíš'))
        if data.get('transport_common',False):
            data['transport_spec'] = None

        if data.get('medicaments',False) and not data.get('medicaments_spec',None):
            self.add_error('medicaments_spec',forms.ValidationError('Upřesni prosím, jaké léky bereš'))
        if not data.get('medicaments',False):
            data['medicaments_spec'] = None

        if data.get('allergies',False) and not data.get('allergies_spec',None):
            self.add_error('allergies_spec',forms.ValidationError('Upřesni prosím, jaké máš alergie'))
        if not data.get('allergies',False):
            data['allergies_spec'] = None

        if data.get('diets',False) and not data.get('diets_spec',None):
            self.add_error('diets_spec',forms.ValidationError('Upřesni prosím, jaké diety dodržuješ'))
        if not data.get('diets',False):
            data['diets_spec'] = None

        if not data.get('adult',False) and not data.get('parent_phone_number',None):
            self.add_error('parent_phone_number',forms.ValidationError('Zadej prosím telefonní číslo na rodiče'))
        if data.get('adult',False):
            data['parent_phone_number'] = None

        return data

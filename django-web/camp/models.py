from django.db import models
from django.utils import timezone

from core.models import User, BaseModel
from django.core.validators import RegexValidator
import datetime
from django.core.exceptions import ValidationError


class Camp(BaseModel):
    class Meta:
        verbose_name = 'soustředění'
        verbose_name_plural = 'soustředění'
        ordering = ('-start',)

    name = models.CharField("název", max_length=64)
    place = models.CharField("místo", max_length=128)
    description = models.TextField("popis", default="")
    photo_link = models.CharField("odkaz na fotky", max_length=255, null=True, blank=True)

    start = models.DateTimeField("začátek")
    end = models.DateTimeField("konec")

    registration_from = models.DateTimeField("začátek registrace")
    registration_to = models.DateTimeField("konec registrace")

    def __str__(self):
        return self.name

    def registration_opened(self):
        return self.registration_from <= timezone.now() <= self.registration_to

    def num_of_participants(self):
        return len(self.participants.all())
    num_of_participants.short_description = "počet přihlášených účastníků"

    def num_of_accepted_participants(self):
        return len(self.participants.filter(status=CampApplication.ApplicationState.ACCEPTED).all())
    num_of_accepted_participants.short_description = "počet přijatých účastníků"


class CampApplication(BaseModel):
    class Meta:
        verbose_name = 'přihláška'
        verbose_name_plural = 'přihlášky'
        unique_together = [('camp', 'participant'),('camp','sub_order')]

    participant = models.ForeignKey(User, related_name="camps", on_delete=models.CASCADE, verbose_name="uživatel")
    camp = models.ForeignKey(Camp, related_name="participants", on_delete=models.CASCADE, verbose_name="soustředění")

    phone_regex = RegexValidator(regex=r'(^\d{9}$)|(^\+\d{12}$)',
                                 message="Zadejte telefonní číslo bez mezer ve formátu 123456789 nebo +420123456789.")
    phone_number = models.CharField("telefon", validators=[phone_regex], max_length=13)
    attendance_entire = models.BooleanField("celková účast")
    attendance_spec = models.CharField("účast - specifikace", null=True, blank=True, max_length=255)
    transport_common = models.BooleanField("společná doprava")
    transport_spec = models.CharField("doprava - specifikace", null=True, blank=True, max_length=255)
    discount_card = models.BooleanField("slevová kartička")
    medicaments = models.BooleanField("léky")
    medicaments_spec = models.CharField("léky - specifikace", null=True, blank=True, max_length=255)
    allergies = models.BooleanField("alergie")
    allergies_spec = models.CharField("alergie - specifikace", null=True, blank=True, max_length=255)
    diets = models.BooleanField("diety")
    diets_spec = models.CharField("diety - specifikace", null=True, blank=True, max_length=255)
    adult = models.BooleanField("dospělý")
    parent_phone_number = models.CharField("telefon na rodiče", validators=[phone_regex], null=True, blank=True, max_length=13)

    class ApplicationState(models.TextChoices):
        WAITING = 'WTG', ('čekající')
        ACCEPTED = 'ACC', ('přijat')
        SUB = 'SUB', ('náhradník')

    status = models.CharField(
        max_length=3,
        choices=ApplicationState.choices,
        default=ApplicationState.WAITING,
    )

    sub_order = models.PositiveSmallIntegerField("pořadí náhradníka", null=True, blank=True)

    def __str__(self):
        return f"{self.participant} ({self.camp})"

    def clean(self):
        if self.status != 'SUB' and self.sub_order is not None:
            raise ValidationError({'sub_order':'Pořadí náhradníka může mít jen náhradník.'})

    @staticmethod
    def last_camp():
        camp = Camp.objects.all().order_by('-start').first()
        return CampApplication.objects.filter(camp = camp)

    def get_status(self):
        if self.status == 'SUB' and self.sub_order is not None:
            return str(self.sub_order)+". "+self.get_status_display()
        else: return self.get_status_display()

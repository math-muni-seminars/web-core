from django import forms


class CheckboxSpecifyWidget(forms.widgets.CheckboxInput):
    template_name = 'camp/widgets/checkbox_specify.html'


class TextSpecifyWidget(forms.widgets.TextInput):
    template_name = 'camp/widgets/input_specify.html'

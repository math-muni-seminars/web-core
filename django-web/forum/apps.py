from django.apps import AppConfig


class SeminarForumConfig(AppConfig):
    name = 'forum'
    verbose_name = "forum"

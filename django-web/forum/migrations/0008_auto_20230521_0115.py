# Generated by Django 3.1.6 on 2023-05-20 23:15

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('forum', '0007_auto_20230514_1311'),
    ]

    operations = [
        migrations.AlterField(
            model_name='text',
            name='author',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL, verbose_name='autor'),
        ),
    ]

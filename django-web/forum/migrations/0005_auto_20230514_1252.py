# Generated by Django 3.1.6 on 2023-05-14 10:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0019_auto_20230514_1252'),
        ('forum', '0004_contribution'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contribution',
            name='contest',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='core.contesttype', verbose_name='soutěž'),
        ),
        migrations.AlterField(
            model_name='topic',
            name='contest',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='core.contesttype', verbose_name='soutěž'),
        ),
    ]

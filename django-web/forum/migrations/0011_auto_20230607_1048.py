# Generated by Django 3.1.6 on 2023-06-07 08:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0026_auto_20230607_1048'),
        ('forum', '0010_auto_20230523_2236'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contribution',
            name='contest_type',
            field=models.ForeignKey(help_text='<p>Soutěž, ke které novinka patří</p>', null=True, on_delete=django.db.models.deletion.PROTECT, to='core.contesttype', verbose_name='soutěž'),
        ),
    ]

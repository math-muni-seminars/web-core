from django.contrib import admin

from forum.models import Topic, Post, Thread, Comment, Contribution, TagField
from core.models import RichTextField
from core import widgets
from django.db import models

from core.admin import PublishableAdmin


class TextAdmin(PublishableAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['id', 'author', 'banned', 'deleted', 'is_public', 'published']


@admin.register(Topic)
class TopicAdmin(PublishableAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['title', 'contest_type', 'is_public', 'private', 'created']
    fields = ['contest_type', 'title', 'private', 'created', 'published','public_from','public_to']
    readonly_fields = ['created']


@admin.register(Thread)
class ThreadAdmin(TextAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['title', 'topic', 'created']
    fields = ['topic','title','text','author', 'created','deleted','banned','published','public_from','public_to']
    readonly_fields = ['created']

    formfield_overrides = {
        models.TextField: {'widget': widgets.MarkdownTextField}
    }


@admin.register(Post)
class PostAdmin(TextAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['id','author', 'created','text']
    fields = ['text','author', 'created','deleted','banned','published','public_from','public_to']
    readonly_fields = ['created']

    formfield_overrides = {
        models.TextField: {'widget': widgets.MarkdownTextField}
    }

    date_hierarchy = 'created'


@admin.register(Comment)
class CommentAdmin(TextAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['id','author', 'created','text']
    fields = ['text','author', 'created','deleted','banned','published','public_from','public_to']
    readonly_fields = ['created']

    formfield_overrides = {
        models.TextField: {'widget': widgets.MarkdownTextField}
    }

    date_hierarchy = 'created'


@admin.register(Contribution)
class ContributionAdmin(PublishableAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['id','contest_type','title', 'author', 'created', 'is_public', 'tag_list_str']
    search_fields = ['title','text']
    fields = ['contest_type','title','author', 'created','published','public_from','public_to','text','tags']
    readonly_fields = ['created']
    autocomplete_fields = ['author']
    list_filter = [('contest_type',admin.RelatedOnlyFieldListFilter)]

    date_hierarchy = 'created'

    formfield_overrides = {
        RichTextField: {'widget': widgets.MarkdownTextField},
        TagField: {'widget': widgets.TagField},
    }
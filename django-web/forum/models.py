from django.db import models

from core.models import PublishableModel, User, RichTextField, Organizer, ContestType


class Topic(PublishableModel):
    class Meta:
        verbose_name = 'téma'
        verbose_name_plural = 'témata'
    title = models.CharField("název", max_length=64)
    private = models.BooleanField("interní",default=False)
    contest_type = models.ForeignKey(ContestType, on_delete=models.PROTECT, verbose_name="soutěž", null=True)

    def __str__(self):
        return self.title


class Text(PublishableModel):
    class Meta:
        verbose_name = 'text'
        verbose_name_plural = 'texty'

    author = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name="autor", null=True)
    text = models.TextField("text")

    deleted = models.BooleanField('smazáno', default=False)
    banned = models.BooleanField("zabanováno", default=False)


class Thread(Text):
    class Meta:
        verbose_name = 'vlákno'
        verbose_name_plural = 'vlákna'

    title = models.CharField("název", max_length=128, null=True, blank=True)
    topic = models.ForeignKey(
        Topic,
        on_delete=models.PROTECT,
        related_name='threads',
        verbose_name='téma',
        null=True,
        blank=True
    )


class Post(Text):
    class Meta:
        verbose_name = 'příspěvek'
        verbose_name_plural = 'příspěvky'

        ordering = ['id']

    parent = models.ForeignKey(
        Thread,
        on_delete=models.CASCADE,
        related_name='children',
        verbose_name="rodič",
    )


class Comment(Text):
    class Meta:
        verbose_name = 'komentář'
        verbose_name_plural = 'komentáře'

        ordering = ['id']

    parent = models.ForeignKey(
        Post,
        on_delete=models.CASCADE,
        related_name='children',
        verbose_name="rodič",
    )


class TagField(models.TextField):
    pass


class Contribution(PublishableModel):
    class Meta:
        verbose_name = 'novinka'
        verbose_name_plural = 'novinky'
        ordering = ['-public_from', '-created']

    contest_type = models.ForeignKey(ContestType, on_delete=models.PROTECT, verbose_name='soutěž', null=True, help_text="<p>Soutěž, ke které novinka patří</p>")
    title = models.CharField('nadpis', max_length=60)
    text = RichTextField('text novinky')
    author = models.ForeignKey(Organizer, on_delete=models.SET_NULL, blank=True, null=True, verbose_name='autor')
    tags = TagField('tagy', default="", blank=True)

    def tag_list(self):
        return [t.strip().split('#') if '#' in t else (t, None) for t in self.tags.split(',') if t.strip()]

    def tag_list_str(self):
        tags = self.tag_list()
        if not tags:
            return ''
        else:
            return ' | '.join(list(zip(*tags))[0])

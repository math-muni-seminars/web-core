from django.core.exceptions import PermissionDenied, ObjectDoesNotExist
from django.db.models import Prefetch
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, DetailView
from django.contrib.auth.decorators import login_required

from core.models import ContestType
from forum.forms import PostForm, CommentForm
from forum.models import Topic, Post, Thread, Comment, Text


class TopicsListView(ListView):
    model = Topic
    paginate_by = 20
    context_object_name = 'topics'

    def get_queryset(self):
        contest_type_abbreviation = self.request.resolver_match.namespace.split(":")[0]

        try:
            contest_type = ContestType.objects.get(abbreviation=contest_type_abbreviation)
        except ObjectDoesNotExist:
            raise Http404("Typ soutěže nenalezen.")

        if self.request.user.is_staff:
            queryset = Topic.objects.filter(contest_type=contest_type).all()
        else:
            queryset = Topic.public_objects().filter(contest_type=contest_type, private=False)
        return queryset.order_by('title').prefetch_related(
            Prefetch('threads', queryset=Thread.objects.all())
        ).all()


class DetailThreadView(DetailView):
    model = Thread

    def get_queryset(self, **kwargs):
        if self.request.user.is_staff:
            queryset = Thread.objects.all()
        else:
            queryset = Thread.objects.filter(topic__in=Topic.public_objects())

        return queryset.prefetch_related(
            Prefetch('children', queryset=Post.objects.prefetch_related(
                Prefetch('children', queryset=Comment.objects.all())
            ))
        )


class ThreadView(DetailThreadView):
    def post(self, request, *args, **kwargs):
        form = PostForm(request.POST)

        thread = get_object_or_404(Thread, pk=self.kwargs['pk'])
        if not thread.topic.is_public() and not request.user.is_staff:
            raise PermissionDenied()

        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.parent_id = kwargs['pk']
            post.published = True
            post.save()
        else:
            pass

        return self.get(request, args, kwargs)


class PostView(DetailThreadView):
    def post(self, request, *args, **kwargs):
        form = CommentForm(request.POST)

        parent = get_object_or_404(Post, pk=self.kwargs['post_pk'])
        if not parent.parent.topic.is_public and not request.user.is_staff:
            raise PermissionDenied()

        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            comment.parent_id = kwargs['post_pk']
            comment.published = True
            comment.save()
        else:
            pass

        return self.get(request, args, kwargs)
    
    def delete(self, request, *args, **kwargs):
        post = Post.objects.get(pk=self.kwargs['post_pk'])
        if request.user.id != post.author_id:
            raise PermissionDenied()

        post.deleted = true
        post.save()

        return self.get(request, args, kwargs)

class CommentView(DetailThreadView):
    def delete(self, request, *args, **kwargs):
        post = Comment.objects.get(pk=self.kwargs['comment_pk'])
        if request.user.id != post.author_id:
            raise PermissionDenied()

        post.deleted = true
        post.save()

        return self.get(request, args, kwargs)


@login_required(redirect_field_name='/')
def handler_delete(request,post_pk):
    try:
        post = Text.objects.get(pk=post_pk)
    except:
        HttpResponseForbidden()
    
    if request.user.id != post.author_id:
            raise PermissionDenied()

    post.deleted = True
    post.save()

    return HttpResponse()


@login_required(redirect_field_name='/')
def handler_respond(request,parent_pk):
    if not request.user.verified:
        HttpResponseForbidden()

    try:
        text = request.POST["text"]
    except:
        HttpResponseForbidden()

    try:
        parent = Thread.objects.get(pk=parent_pk)
        Post.objects.create(text=text,author=request.user,parent=parent,published=True)
    except:
        try:
            parent = Post.objects.get(pk=parent_pk)
            Comment.objects.create(text=text,author=request.user,parent=parent,published=True)
        except:
            try:
                parent = Comment.objects.get(pk=parent_pk).parent
                Comment.objects.create(text=text,author=request.user,parent=parent,published=True)
            except:
                HttpResponseForbidden()

    return HttpResponse()


@login_required(redirect_field_name='/')
def handler_ban(request,post_pk):
    if not request.user.is_staff:
        PermissionDenied()

    try:
        post = Text.objects.get(pk=post_pk)
    except:
        HttpResponseForbidden()

    post.banned = not post.banned
    post.save()

    return HttpResponse()

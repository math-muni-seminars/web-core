from django.urls import path
from forum.views import TopicsListView, ThreadView, PostView, CommentView, handler_delete, handler_respond, handler_ban

app_name = 'forum'

urlpatterns = [
    path('', TopicsListView.as_view(), name='topics'),
    path('threads/<int:pk>', ThreadView.as_view(), name='thread'),
    #path('threads/<int:pk>/response', ThreadView.as_view(), name='thread_response'),
    #path('threads/<int:pk>/posts/<int:post_pk>/delete', PostView.as_view(), name='post_delete'),
    #path('threads/<int:pk>/posts/<int:post_pk>/response', PostView.as_view(), name='post_response'),
    #path('threads/<int:pk>/comment/<int:comment_pk>/delete', CommentView.as_view(), name='comment_delete'),
    path('delete/<int:post_pk>', handler_delete, name='delete'),
    path('ban/<int:post_pk>', handler_ban, name='ban'),
    path('respond/<int:parent_pk>', handler_respond, name='respond'),
]

from django.urls import path

import contest.views as views
from contest.views import tasks, results, handlers
from mathrace.views import handlers as mhandlers
from core.views import handler_send_mail, handler_test_mail

urlpatterns = [
    path('collection_file/brkos/<int:collection_id>', handlers.collection_file_upload, name='collection_file_upload_brkos'),
    path('collection_file/mathrace/<int:collection_id>', mhandlers.collection_file_upload, name='collection_file_upload_mathrace'),
    path('contest_file/mathrace/<int:contest_id>', mhandlers.contest_file_upload, name='contest_file_upload_mathrace'),
    path('task/feedback/<int:submission_id>', handlers.feedback_upload, name='feedback_upload'),
    path('task/upload/<int:task_id>', handlers.multiple_upload, name='multiple_upload'),
    path('tasks/submissions_zip/<int:task_id>', tasks.handler_task_submissions_zip, name='task_submissions_zip'),
    path('latex/tasks/<int:id>', tasks.handler_task_collection_latex, name='tasks_latex'),
    path('latex/solutions/<int:id>', tasks.handler_solutions_latex, name='solutions_latex'),
    path('latex/results/<int:id>/cumulative', results.handler_latex_results_cumulative, name='latex_results_cumulative'),
    path('latex/results/<int:id>', results.handler_latex_results, name='latex_results'),
    path('mail/send/<int:id>', handler_send_mail, name='send_mail'),
    path('mail/test/<int:id>', handler_test_mail, name='test_mail'),
]

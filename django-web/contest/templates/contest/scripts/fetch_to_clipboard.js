function fetch_to_clipboard(url) {
    $.ajax({
        url: url,
        type: 'get',
        contentType: false,
        processData: false,
        headers: { "X-CSRFToken": '{{ csrf_token }}' },
        success: function(response){
            navigator.clipboard.writeText(response).then(function() {
                alert("Zdrojový kód uložen do clipboardu.");
            }, function(err) {
                console.log(err)
                alert("Zdrojový kód byl získán, ale nepodařilo se ho uložit do clipboardu.\n" + response);
            });
        },
        error: function (request, status, error) {
            alert(request.responseText);
        }
    });
}
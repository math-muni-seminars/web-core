from django.urls import path, include
from contest import views
from core.views import page_static
from contest.views import users, tasks, results, handlers

app_name = 'contest'

urlpatterns = [
    path('login/', views.users.page_login, name='login'),
    path('logout/', views.users.handler_logout, name='logout'),
    path('register/', views.users.page_register, name='register'),

    path('reset_password/', views.users.page_reset_password, name='reset_password'),
    path('reset_password/confirm/<uidb64>/<token>/', views.users.page_reset_password_confirm, name='reset_password_confirm'),
    path('reset_password/done/', views.users.page_reset_password_done, name='reset_password_done'),

    path('verification/info', views.users.PageVerificationInfo.as_view(tags=['verification']), name='verification_info'),
    path('verification/send', views.users.page_verification_send, name='verification_send'),
    path('verification/verify/<uidb64>/<str:token>', views.users.page_verification_verify, name='verification_verify'),

    path('account/view_change_password/', views.users.page_change_password, name='change_password'),
    path('account/', lambda request: views.users.page_account(request,None), name='account'),
    path('account/<str:tag>', views.users.page_account, name='account_tagged'),
    path('update_account/', views.users.page_update_account, name='update_account'),
    path('contestant/<str:username>', views.users.page_public_account, name='public_account'),

    path('forum/', include('forum.urls')),
    path('camp/', include('camp.urls')),
    
    path('', views.PageIndex.as_view(tags=['claim', 'subclaim', 'intro']), name='index'), #template_name='contest/index.html', 
    path('archive/', views.page_archive, name='archive'),

    #path('tasks/<int:contest_order>/<int:collection_order>/<int:task_order>', views.tasks.page_task, name='task'),
    path('tasks/<int:contest_order>/<int:collection_order>', views.tasks.page_task_collection, name='tasks'),
    path('tasks/', views.tasks.page_task_collection_current, name='tasks_newest'),

    #path('solutions/<int:contest_order>/<int:collection_order>', views.tasks.page_sample_solutions, name='sample_solutions'),

    path('results/<int:contest_order>/<int:collection_order>/cumulative', views.results.PageResults.as_view(cumulative=True), name='results_cumulative'),
    path('results/<int:contest_order>/<int:collection_order>', views.results.PageResults.as_view(), name='results'),
    path('results/<int:contest_order>', views.results.PageResults.as_view(), name='results_contest'),
    path('results/', views.results.PageResults.as_view(), name='results_newest'),

    path('old_contribs/', views.PageOldContribs.as_view(), name='old_contribs_def'),
    path('old_contribs/<int:page_no>', views.PageOldContribs.as_view(), name='old_contribs'),
    path('rules/', views.PageRules.as_view(tags=['rules']), name='rules'),
    path('about/', views.PageAboutUs.as_view(tags=['about']), name='about'),
    #path('other_seminars/', views.PageOtherSeminars.as_view(tags=['other_seminars']), name='other_seminars'),
    path('how_to/', views.PageHowTo.as_view(tags=['how_to']), name='how_to'),
    path('user/submission/<int:contest_order>/<int:collection_order>/<int:task_order>', handlers.user_submission, name='user_submission'),
    path('user/submission/<int:contest_order>/<int:collection_order>/<int:task_order>/delete', handlers.user_submission_delete, name='user_submission_delete'),
    path('user/submission/<int:contest_order>/<int:collection_order>/<int:task_order>/write', views.page_write_submission, name='user_submission_write'),
    path('user/submission/<int:contest_order>/<int:collection_order>/<int:task_order>/save', handlers.user_submission_save, name='user_submission_save'),

    path('page/<path:path>', page_static, name='static_page'),
]
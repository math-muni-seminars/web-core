from django.apps import AppConfig
from django.contrib.admin.apps import AdminConfig


class ContestConfig(AppConfig):
    name = 'contest'
    verbose_name = "BRKOS"

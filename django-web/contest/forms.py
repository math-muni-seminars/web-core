from django.contrib.auth.forms import PasswordChangeForm, SetPasswordForm
from django.core.validators import FileExtensionValidator, MinValueValidator, MaxValueValidator
from contest.models import Contestant
from django.utils import timezone

from django.template import loader
from django.contrib.auth import get_user_model
from django.conf import settings
from django.core.mail import EmailMessage
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from django.utils.translation import gettext, gettext_lazy as _
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes

from core import forms
from contest.widgets import SchoolWidget
from core.widgets import CheckboxGDPRWidget

User = get_user_model()


class StyledPasswordChangeForm(PasswordChangeForm):
    old_password = forms.PasswordField(
        label="Staré heslo",
        strip=False,
    )
    new_password1 = forms.PasswordField(
        label="Nové heslo",
        strip=False,
    )
    new_password2 = forms.PasswordField(
        label="Nové heslo znovu",
        strip=False,
    )



class StyledPasswordResetForm(forms.Form):
    username = forms.CharField(
        label="Přihlašovací jméno",
        max_length=255
    )

    def send_mail(self, context, to_email):
        subject = "BRKOS Obnova hesla"
        content = loader.render_to_string('pwd/email.html', context)

        email = EmailMessage(subject,content,'"BRKOS" <'+settings.EMAIL_HOST_USER+'>',[to_email])
        email.send(fail_silently = False)

    def get_users(self, username):
        active_users = User._default_manager.filter(**{
            'username__iexact': username,
            'is_active': True,
        })
        return (
            u for u in active_users
            if u.has_usable_password()
        )

    def save(self,request):
        username = self.cleaned_data["username"]
        current_site = get_current_site(request)
        site_name = current_site.name
        domain = current_site.domain
        for user in self.get_users(username):
            user_email = getattr(user, "email")
            context = {
                'email': user_email,
                'domain': domain,
                'site_name': site_name,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'user': user,
                'token': default_token_generator.make_token(user),
                'protocol': 'http',
            }
            self.send_mail(context, user_email)

class StyledSetPasswordForm(SetPasswordForm):
    new_password1 = forms.PasswordField(
        label="Nové heslo",
        strip=False,
    )
    new_password2 = forms.PasswordField(
        label="Nové heslo znovu",
        strip=False,
    )



class ContestantFormNoGDPR(forms.ModelForm):
    school = forms.CharField(widget=SchoolWidget, required=True, max_length=250, label="Škola")
    math_class = forms.BooleanField(required=False, label="Matematická třída")
    graduate_year = forms.IntegerField(required=True, label="Rok maturity", labeled=True)

    class Meta:
        model = Contestant
        fields = ['school', 'math_class', 'graduate_year']


class ContestantForm(ContestantFormNoGDPR):
    graduate_year = forms.IntegerField(required=True, label="Rok maturity", labeled=False)
    gdpr = forms.BooleanField(widget=CheckboxGDPRWidget(attrs={'gdpr_url':'/static/brkos/GDPR.pdf'}), required=True, label="Souhlasím s podmínkami zpracování osobních údajů")
from django import forms

import json

class SchoolWidget(forms.widgets.TextInput):
    template_name = 'contest/widgets/school.html'

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        f = open("core/static/core/files/list_of_schools.json")
        context['schools'] = json.load(f)
        return context

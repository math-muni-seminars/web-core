from django.contrib import admin, messages
from django.template.loader import render_to_string

from .models import Contestant, Contest, TaskCollection, Task, Story, TaskFileSubmission
from django.utils.html import format_html, urlencode
from django.urls import reverse, path
from django.forms import ModelForm
from django.core.validators import MaxValueValidator
from django.db import models
from core import widgets

from core.templatetags.extras import markdown_to_html
from django.utils.html import mark_safe
from django.template.response import TemplateResponse
from django.http import HttpResponseForbidden, HttpResponseRedirect
from django.forms import BaseInlineFormSet

from core.admin import PublishableAdmin


@admin.register(Contestant)
class ContestantAdmin(admin.ModelAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['get_name', 'get_username', 'get_email','school', 'grade', 'math_class']
    search_fields = ['school','user__username','user__first_name','user__last_name','user__email']
    fields = ['user','get_name','get_email','school','graduate_year','math_class']
    readonly_fields = ['get_name','get_email']
    autocomplete_fields = ['user']

    def get_username(self, obj):
        return obj.user.username
    get_username.short_description = "uživatelské jméno"

    def get_name(self, obj):
        return obj.user.full_name()
    get_name.short_description = "jméno"

    def get_email(self, obj):
        return obj.user.email
    get_email.short_description = 'email'


@admin.register(Contest)
class ContestAdmin(PublishableAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['order', 'end_year', 'is_public', 'view_collections_link']
    fields = ['order','end_year','published','public_from','public_to']

    ordering = ('-order',)

    def view_collections_link(self, obj):
        count = obj.task_collections.count()
        link_url = (
                reverse("admin:contest_taskcollection_changelist")
                + "?"
                + urlencode({"contest__id": f"{obj.id}"})
        )
        return format_html('<a href="{}">{} sérií</a>', link_url, count)

    view_collections_link.short_description = "Série"


class TaskInlineFormSet(BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        kwargs['initial'] = [
            {'order': 1, 'name':'1', 'max_score':3},
            {'order': 2, 'name':'2', 'max_score':3},
            {'order': 3, 'name':'3', 'max_score':4},
            {'order': 4, 'name':'4', 'max_score':5},
            {'order': 5, 'name':'A', 'max_score':3},
            {'order': 6, 'name':'B', 'max_score':3},
            {'order': 7, 'name':'C', 'max_score':4},
            {'order': 8, 'name':'D', 'max_score':5},
        ]
        super(TaskInlineFormSet, self).__init__(*args, **kwargs)


class TaskInline(admin.TabularInline):
    model = Task
    formset = TaskInlineFormSet

    fields = [
        'order',
        'collection',
        'name',
        'max_score',
        'text',
        'model_solution',
    ]

    def get_extra(self, request, obj=None, **kwargs):
        return 8 if obj is None or Task.objects.filter(collection=obj).count()==0 else 0 #max(0, 8 - obj.tasks.count())


@admin.register(TaskCollection)
class TaskCollectionAdmin(PublishableAdmin):
    fields = ['contest','order','name','close_date','published','public_from','public_to','ready','completed','release_solution',
        'release_results','assignment_pdf','helptext_pdf','solution_pdf','scoreboard_pdf']
    readonly_fields = ['ready','completed']
    list_display = ['__str__', 'is_public', 'close_date', 'ready', 'completed', 'release_solution', 'release_results', 'view_tasks_link', 'file_management']
    list_filter = [('contest',admin.RelatedOnlyFieldListFilter)]

    ordering = ('-contest__order', '-order')

    inlines = [
        TaskInline,
    ]

    def file_management(self, obj):
        upload_form = render_to_string('contest/parts/collection_file_upload.html', {'collection': obj})
        return format_html(upload_form)

    def view_tasks_link(self, obj):
        count = obj.tasks.count()
        link_url = (
                reverse("admin:contest_task_changelist")
                + "?"
                + urlencode({"collection__id": f"{obj.id}"})
        )
        return format_html('<a href="{}">{} úloh</a>', link_url, count)

    def save_model(self, request, obj, form, change):
        super(TaskCollectionAdmin,self).save_model(request, obj, form, change)
        if (obj.is_public() or obj.public_from) and not obj.ready():
            messages.add_message(request, messages.WARNING, "Pozor, série úloh {} není připravena pro zadání.".format(obj))

    file_management.short_description = "soubory"
    view_tasks_link.short_description = "úlohy"


class Form(ModelForm):
    def __init__(self, *args, **kwargs):
        super(Form, self).__init__(*args, **kwargs)
        if 'instance' in kwargs:
            self.fields['score'].validators.append(MaxValueValidator(kwargs['instance'].task.max_score))


class TaskSubmissionInline(admin.TabularInline):
    model = TaskFileSubmission
    fields = ('contestant_detail', 'online_submission_math', 'file_link', 'score', 'feedback_text', 'feedback_file_manager')
    extra = 0
    can_delete = False
    readonly_fields = ('contestant_detail', 'online_submission_math', 'file_link', 'feedback_file_manager')

    formfield_overrides = {
        models.TextField: {'widget': widgets.MarkdownTextField}
    }

    def feedback_file_manager(self, obj):
        upload_form = render_to_string('contest/parts/feedback_upload.html', {'submission': obj})
        return format_html(upload_form)

    feedback_file_manager.short_description = "komentář (soubor)"

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def online_submission_math(self, obj):
        return mark_safe(markdown_to_html(obj.online_submission))

    online_submission_math.short_description = "online odevzdané řešení"

    def contestant_detail(self,obj):
        url = reverse('admin:contest_ratesubmission_change', args=[obj.id])
        return format_html("<b><a href='{0}'>{1}</a></b>", url, obj.contestant)
    
    contestant_detail.short_description = "soutěžící"


class RateTask(Task):
    class Meta:
        verbose_name = "opravování"
        verbose_name_plural = "opravování"
        proxy = True


@admin.register(RateTask)
class RateTaskAdmin(admin.ModelAdmin):
    inlines = [TaskSubmissionInline]
    list_display = ['__str__','proxy','completed', 'zip_link', 'upload_files']
    fields = ['contest','collection','name','proxy','completed','max_score','text_html','model_solution','comment']
    readonly_fields = ['name','contest', 'collection', 'proxy', 'max_score','text_html']
    list_filter = [
        ('proxy',admin.RelatedOnlyFieldListFilter),
        ('collection__contest',admin.RelatedOnlyFieldListFilter),
        ('collection',admin.RelatedOnlyFieldListFilter)]

    formfield_overrides = {
        models.TextField: {'widget': widgets.MarkdownTextField}
    }

    ordering = ('-collection__contest__order','-collection__order', 'order')

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def zip_link(self, obj):
        url = reverse('task_submissions_zip', kwargs={'task_id': obj.id})
        return format_html("<a href='{}'>ZIP</a>", url)
    zip_link.short_description = "nahraná řešení"

    def contest(self,obj):
        return obj.collection.contest
    contest.short_description = "ročník"

    def text_html(self,obj):
        return mark_safe(markdown_to_html(obj.text))
    text_html.short_description = "zadání"

    def upload_files(self,obj):
        upload_btn = render_to_string('contest/parts/upload_files.html', {'task': obj})
        return format_html(upload_btn)
    upload_files.short_description = "Nahrát opravené soubory"


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    change_list_template = 'smuggler/change_list.html'
    fields = [
        'name',
        'collection',
        'contest',
        'order',
        'proxy',
        'ready',
        'completed',
        'max_score',
        'text',
        'model_solution',
        'comment',
    ]
    readonly_fields = ['contest']
    list_display = ['__str__', 'proxy', 'ready', 'completed']
    autocomplete_fields = ['proxy']

    formfield_overrides = {
        models.TextField: {'widget': widgets.MarkdownTextField}
    }

    list_filter = [('collection__contest',admin.RelatedOnlyFieldListFilter),('collection',admin.RelatedOnlyFieldListFilter)]
    ordering = ('-collection__contest__order','-collection__order', 'order')

    def contest(self,obj):
        try:
            return obj.collection.contest
        except:
            return "-"
    contest.short_description = "ročník"


@admin.register(Story)
class StoryAdmin(admin.ModelAdmin):
    change_list_template = 'smuggler/change_list.html'
    fields = ['contest','collection','ready','text']
    readonly_fields = ['contest']
    list_display = ['collection', 'text', 'ready']
    list_filter = [('collection__contest',admin.RelatedOnlyFieldListFilter),('collection',admin.RelatedOnlyFieldListFilter)]

    formfield_overrides = {
        models.TextField: {'widget': widgets.MarkdownTextField}
    }

    ordering = ('-collection__contest__order','-collection__order')

    def contest(self,obj):
        return obj.collection.contest
    contest.short_description = "ročník"

"""
@admin.register(School)
class SchoolAdmin(admin.ModelAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['id', 'name', 'full_name']
"""

@admin.register(TaskFileSubmission)
class SubmissionAdmin(admin.ModelAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['id', 'contestant', 'task', 'score', 'file_link']
    
    list_filter = [('task__collection__contest',admin.RelatedOnlyFieldListFilter),('task__collection',admin.RelatedOnlyFieldListFilter),('task',admin.RelatedOnlyFieldListFilter)]
    ordering = ('-task__collection__contest__order', '-task__collection__order', 'task__order')


class RateSubmission(TaskFileSubmission):
    class Meta:
        verbose_name = "detail opravování"
        verbose_name_plural = "detaily opravování"
        proxy = True


@admin.register(RateSubmission)
class RateSubmissionAdmin(admin.ModelAdmin):
    fields = ['score','feedback_text']
    
    formfield_overrides = {
        models.TextField: {'widget': widgets.MarkdownTextField}
    }

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        if extra_context is None:
            extra_context = {}

        obj = TaskFileSubmission.objects.get(id=object_id)

        extra_context["submission"] = obj
        extra_context["is_pdf"] = (obj.content.url().split('.')[-1].lower()=='pdf')
        extra_context["text_submission"] = mark_safe(markdown_to_html(obj.online_submission))
        extra_context["prev"] = TaskFileSubmission.objects.filter(task=obj.task, id__lt=obj.id).last()
        extra_context["next"] = TaskFileSubmission.objects.filter(task=obj.task, id__gt=obj.id).first()
        
        return super().changeform_view(request, object_id, form_url, extra_context)

    def response_change(self, request, obj):

        if "_next" in request.POST:
            msg = 'Oprava byla úspěšně uložena.'
            self.message_user(request, msg, messages.SUCCESS)
            next_obj = TaskFileSubmission.objects.filter(task=obj.task, id__gt=obj.id).first()
            redirect_url = reverse('admin:contest_ratesubmission_change',args=[next_obj.id])
            print(redirect_url)
            return HttpResponseRedirect(redirect_url)
        else:
            return super().response_change(request, obj)

class Stats(models.Model):
    class Meta:
        verbose_name = "statistika"
        verbose_name_plural = "statistika"
        #proxy = True


@admin.register(Stats)
class StatsAdmin(admin.ModelAdmin):
    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path('', self.stats),
        ]
        return my_urls + urls

    def stats(self, request):
        if not request.user.is_staff:
            return HttpResponseForbidden("")

        context = dict(
            self.admin_site.each_context(request)
        )

        g1=0
        g2=0
        g3=0
        g4=0
        for contestant in Contestant.current_contest():
            if contestant.grade()==1:
                g1+=1
            if contestant.grade()==2:
                g2+=1
            if contestant.grade()==3:
                g3+=1
            if contestant.grade()==4:
                g4+=1

        current_contest = Contest.objects.order_by('order').last();
        tasks_scores = []
        for task in Task.objects.filter(collection__contest = current_contest).order_by("collection__order","order"):
            s = 0
            n = 0
            for submission in TaskFileSubmission.objects.filter(task = task):
                try:
                    s += submission.score
                    n += 1
                except:
                    pass
            tasks_scores.append({"name": task.name, "collection": task.collection.order, "score": s/n if n>0 else "-"})

        collections = []
        for collection in TaskCollection.objects.filter(contest = current_contest):
            contestants = set()
            for submission in TaskFileSubmission.objects.filter(task__collection = collection):
                contestants.add(submission.contestant)

            submissions = TaskFileSubmission.objects.filter(task__collection = collection).count()

            collections.append({"order": collection.order, "contestants": len(contestants), "submissions": submissions})

        tasks_submissions = []
        for task_order in range(1,8):
            _collections = []
            for task in Task.objects.filter(collection__contest = current_contest, order = task_order).order_by("collection__order","order"):
                submissions = TaskFileSubmission.objects.filter(task = task).count()
                _collections.append({"order": task.collection.order, "submissions": submissions})

            tasks_submissions.append({"name": task_order, "collections": _collections})

        context["this_contest_number"] = len(Contestant.current_contest())
        context["tasks_scores"] = tasks_scores
        context["bygrade"] = {"g1": g1, "g2": g2, "g3": g3, "g4": g4}
        context["collections"] = collections
        context["tasks_submissions"] = tasks_submissions

        return TemplateResponse(request, "core/parts/stats.html", context)

    def has_add_permission(self, request, obj=None):
        return False
    
    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False
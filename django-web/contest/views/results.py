from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, HttpResponse
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.views.generic import TemplateView

from contest.models import Contest, TaskCollection, ResultsTable
from core.models import PublishableModel
from core.utils import int_to_roman

"""
Helpers
"""


def render_latex_results(request, head, res, contest, collection=None, cumulative=False):
    template_name = 'contest/latex/results.tex'

    contest_year_roman = int_to_roman(contest.order)
    if cumulative:
        title = 'Výsledky po {}. sérii {}. ročníku'.format(collection.order, contest_year_roman)
    else:
        title = 'Výsledky {}. série {}. ročníku'.format(collection.order, contest_year_roman)

    column_definition = '|r|l|c|l|' + 'c|' * (len(head)-4)

    context = {
        'collection': collection,
        'contest': contest,
        'head': head,
        'results': res,
        'column_definition': column_definition,
        'title': title,
    }

    return HttpResponse(render_to_string(template_name, context), content_type='text/plain')


"""
Views
"""


def find_collection(contest_order=None, collection_order=None):
    try:
        if contest_order is None:
            contest = Contest.public_objects()\
                .order_by('-order').first()
        else:
            contest = Contest.public_objects().get(order=contest_order)
    except (ObjectDoesNotExist, IndexError):
        raise Http404("Soutěž nenalezena.")

    if collection_order is None:
        collection = TaskCollection.current_collection_with_results()
    else:
        collection = contest.task_collections.filter(PublishableModel.q_public(), order=collection_order).last()
    if collection is None:
        raise Http404("Pro tuto soutěž zatím není zveřejněna žádná série.")

    return contest, collection


class PageResults(TemplateView):
    template_name = 'contest/results.html'
    cumulative = False

    def get_context_data(self, **kwargs):
        contest, collection = find_collection(kwargs.get('contest_order', None),
                                              kwargs.get('collection_order', None))
        table = ResultsTable(contest, collection)
        if collection.release_results:
            if self.cumulative:
                head, res = table.get_results_cumulative()
            else:
                head, res = table.get_results_collection()
        else:
            res = []
            head = []

        contests = Contest.objects.all()
        collections = contest.task_collections.all()

        return {
            'results': res,
            'displayed_contest': contest,
            'displayed_collection_order': collection.order,
            'contests': contests,
            'task_collections': collections,
            'this_contest_order': contest.order,
            'head': zip(range(1, len(head) + 1), head),
            'cumulative': self.cumulative,
        }


def handler_latex_results_cumulative(request, id):
    collection = get_object_or_404(TaskCollection, pk=id)
    head, res = ResultsTable(collection.contest, collection).get_results_cumulative()
    return render_latex_results(request, head, res, collection.contest, collection, cumulative=True)


def handler_latex_results(request, id):
    collection = get_object_or_404(TaskCollection, pk=id)
    head, res = ResultsTable(collection.contest, collection).get_results_collection()
    return render_latex_results(request, head, res, collection.contest, collection)

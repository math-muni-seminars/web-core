from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.http import Http404, HttpResponse, HttpResponseForbidden
from django.shortcuts import render, get_object_or_404
from django.template.loader import select_template
from django.core.paginator import Paginator
from django.shortcuts import redirect, render
from django.utils import timezone
from django.db.models import Prefetch
from django.db.models.functions import Lower

from contest.models import Contest, TaskCollection, TaskFileSubmission, Task, Contestant
from core.forms import UploadForm
from core.models import MediaFile, Organizer, ContestType
from forum.models import Contribution
from django.views.generic import TemplateView

from core.views import EditablePage, render_editable
from django.forms import Form

import json

"""
Views
"""


class PageRules(EditablePage):
    template_name = "contest/rules.html"

"""
class PageOtherSeminars(EditablePage):
    template_name = "contest/other_seminars.html"
"""

class PageHowTo(EditablePage):
    template_name = "contest/how_to.html"


class PageOldContribs(EditablePage):
    template_name = "contest/old_contribs.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        try:
            contest_type = ContestType.objects.get(abbreviation="brkos")
        except ObjectDoesNotExist:
            raise Http404("Typ soutěže nenalezen.")

        p = Paginator(Contribution.public_objects().filter(contest_type=contest_type),20)
        page_no = kwargs.get('page_no', 1)

        context['contribs'] = p.page(page_no).object_list
        context['page_no'] = page_no
        context['pages'] = p.page_range

        return context


class PageAboutUs(EditablePage):
    template_name = "contest/about_us.html"

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['active_organizers'] = Organizer.objects.filter(active=True).order_by(Lower('nickname'))
        data['former_organizers'] = Organizer.objects.filter(active=False).order_by(Lower('nickname'))
        return data


@login_required(redirect_field_name='/')
def page_write_submission(request, contest_order, collection_order, task_order):
    try:
        task = Task.objects.get(collection__contest__order = contest_order, collection__order = collection_order, order = task_order)
    except ObjectDoesNotExist:
        raise Http404("Úloha nenalezena.")

    try:
        former_text = TaskFileSubmission.objects.get(task = task, contestant = request.user.contestant).online_submission
    except:
        former_text = ""

    context = {
        'collection_order': collection_order,
        'task': task,
        'former_text': former_text
    }
    
    return render_editable(request, 'contest/write_submission.html', context)


class PageIndex(EditablePage):
    # template_names = select_template([namesapce + '/index.html', 'contest/index.html'])

    template_name = "contest/index.html"
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        contest_type_abbreviation = "brkos"
        try:
            contest_type = ContestType.objects.get(abbreviation=contest_type_abbreviation)
        except ObjectDoesNotExist:
            contest_type = None

        collection = TaskCollection.current_collection()
        context['active_collection'] = collection
        context['active_contest'] = collection.contest if collection is not None else None
        context['contribs'] = Contribution.public_objects().filter(contest_type=contest_type)[:2]
        context['navbar_absolute_pos'] = True

        return context


# def page_index(request):
#     def get_context_data(self, **kwargs):
#         data = super().get_context_data(**kwargs)
#         namesapce = request.resolver_match.namespace
#         template = select_template([namesapce + '/index.html', 'contest/index.html'])
#
#         contest_type_abbreviation = request.resolver_match.namespace
#         try:
#             contest_type = ContestType.objects.get(abbreviation=contest_type_abbreviation)
#         except ObjectDoesNotExist:
#             contest_type = None
#         collection = TaskCollection.current_collection(contest_type) if contest_type is not None else None
#         contest = collection.contest if collection is not None else None
#         return {
#             'contribs': Contribution.public_objects()[:2],
#             'navbar_absolute_pos': True,
#             'active_contest': contest,
#             'active_collection': collection,
#         }
#
#     return HttpResponse(template.render(context, request))


def page_info(request):
    """
    Renders the info page from template. Does not add an extra context.

    template: contest/info.html

    Privacy policy: PUBLIC

    :param request: HTTP request
    :return: HTTP response
    """
    return render(request, 'contest/info.html')


def page_archive(request):
    contests = Contest.public_objects().order_by('-order')\
        .prefetch_related(Prefetch('task_collections',queryset=TaskCollection.public_objects()))
    context = {
        'contests': contests
    }

    return render(request, 'contest/archive.html', context=context)
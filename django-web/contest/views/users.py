from django.contrib import messages
from django.contrib.auth import update_session_auth_hash, login, authenticate, logout, get_user_model
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404, HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import redirect, render

from contest.forms import ContestantForm, ContestantFormNoGDPR, StyledPasswordChangeForm, StyledPasswordResetForm, StyledSetPasswordForm
from contest.models import Contestant, TaskFileSubmission, Contest
from core.forms import RegisterForm, AccountForm
from core.views import EditablePage

import json

from django.utils.http import urlsafe_base64_decode
from django.contrib.auth.tokens import default_token_generator

User = get_user_model()

@login_required()
def page_change_password(request):
    """
    POST: Updates users password and redirect to 'change_password' or render form if data are invalid.

    GET: Renders change password form.

    Template: contest/change_password.html

    Privacy policy: LOGIN_REQUIRED

    :param request: HTTP request
    :return: HTTP response
    """

    render_form = StyledPasswordChangeForm(request.user)

    if request.method == 'POST':
        form = StyledPasswordChangeForm(request.user, request.POST)

        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            render_form = form
            return redirect('contest:account_tagged',tag="pwdch")
        else:
            render_form = form
            return render(request, 'contest/change_password.html', {'form': render_form})
    else:
        return render(request, 'contest/change_password.html', {'form': render_form})


def page_reset_password(request):
    render_form = StyledPasswordResetForm()

    if request.method == 'POST':
        form = StyledPasswordResetForm(request.POST)

        if form.is_valid():
            form.save(request)
            return render(request, 'contest/reset_password.html', {'form': render_form, 'email_sent': True})
        else:
            render_form = form
            return render(request, 'contest/reset_password.html', {'form': render_form, 'email_sent': False})
    else:
        return render(request, 'contest/reset_password.html', {'form': render_form, 'email_sent': False})


def page_reset_password_confirm(request,*args,**kwargs):
    user = None
    try:
        # urlsafe_base64_decode() decodes to bytestring
        uid = urlsafe_base64_decode(kwargs.get('uidb64',None)).decode()
        user = User._default_manager.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist, ValidationError):
        user = None

    if user is not None:
        render_form = StyledSetPasswordForm(user)
        
        token = kwargs['token']

        if token == 'set-password':
            session_token = request.session.get('_password_reset_token')
            if default_token_generator.check_token(user, session_token):
                if request.method == 'POST':
                    form = StyledSetPasswordForm(user,request.POST)
                    if form.is_valid():
                        form.save()
                        return redirect('contest:reset_password_done')
                    else:
                        render_form = form
                        return render(request, 'contest/reset_password_confirm.html', {'form': render_form, 'email_sent': False})
                else:
                        return render(request, 'contest/reset_password_confirm.html', {'form': render_form, 'email_sent': False})
            else: return HttpResponseForbidden("Invalid token")
                    
        else:
            if default_token_generator.check_token(user, token):
                # Store the token in the session and redirect to the
                # password reset form at a URL without the token. That
                # avoids the possibility of leaking the token in the
                # HTTP Referer header.
                request.session['_password_reset_token'] = token
                return HttpResponseRedirect(request.path.replace(token, 'set-password'))
            else: return HttpResponseForbidden("Invalid token")


def page_reset_password_done(request):
    return render(request, 'contest/reset_password_done.html', {})


class PageVerificationInfo(EditablePage):
    template_name = "contest/verification_info.html"


def page_verification_send(request):
    request.user.send_verification(request)
    return render(request, 'contest/verification_send.html', {})


def page_verification_verify(request,uidb64,token):
    user = None
    try:
        # urlsafe_base64_decode() decodes to bytestring
        uid = urlsafe_base64_decode(uidb64).decode()
        user = User.objects.get(pk=uid)
    except:
        user = None

    if user is not None:
        if token == user.verification_token():
            user.verified = True
            user.save()
            return render(request, 'contest/verification_verify.html', {'success': True})
        else:
            return render(request, 'contest/verification_verify.html', {'success': False})


def page_public_account(request, username):
    # View account of someone else.
    contestant = Contestant.objects.get(user__username=username)
    if not contestant:
        raise Http404("Uživatel neexistuje.")
    return render(request, 'contest/public_account.html', {'contestant': contestant})


@login_required()
def page_account(request,tag):
    """
    Renders a users account overview including task submissions and evaluations.

    Template: contest/account.html

    Privacy policy: LOGIN_REQUIRED

    :param request: HTTP request
    :return: HTTP response
    """

    context = {}
    try:
        if tag == "old":
            context['submissions'] = TaskFileSubmission.objects.filter(
                contestant=request.user.contestant
            ).all().order_by('-task__collection__contest__order','task__collection__order','task__order')
            context['old'] = True
        else:
            current_contest = Contest.objects.order_by('order').last()
            context['submissions'] = TaskFileSubmission.objects.filter(
                task__collection__contest=current_contest,
                contestant=request.user.contestant
            ).all().order_by('task__collection__order','task__order')

        context['msg'] = None
        if tag == "updated": context['msg'] = "Nastavení bylo uloženo."
        if tag == "pwdch": context['msg'] = "Heslo bylo změněno."
    except ObjectDoesNotExist:
        pass

    return render(request, 'contest/account.html', context)


def page_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            if 'text_only' in request.GET:
                return HttpResponse('Uživatel přihlášen.')
            else:
                try:
                    next = request.get_full_path().split("?next=")[1]
                    return redirect(next)
                except:
                    return redirect('contest:index')
        else:
            if 'text_only' in request.GET:
                return HttpResponseBadRequest('Přihlašovací jméno nebo heslo není správné.')
            else:
                return render(request, "contest/login.html", {'navbar_absolute_pos': True})
    else:
        return render(request, "contest/login.html", {'navbar_absolute_pos': True})


@login_required()
def handler_logout(request):
    logout(request)
    return redirect("contest:index")


def page_register(request):

    if request.method == "POST":
        registration_form = RegisterForm(request.POST)
        contestant_form = ContestantForm(request.POST)
        if registration_form.is_valid() and contestant_form.is_valid():
            user = registration_form.save()
            contestant = contestant_form.save(commit=False)
            contestant.user = user
            contestant.save()
            login(request, user)
            request.user.send_verification(request)
            return redirect('contest:verification_info')
        else:
            return render(request, 'contest/register.html',
                          {
                              'registration_form': registration_form,
                              'contestant_form': contestant_form,
                          })
    else:
        registration_form = RegisterForm()
        contestant_form = ContestantForm()
        return render(request, 'contest/register.html',
                      {
                          'registration_form': registration_form,
                          'contestant_form': contestant_form,
                      })


def page_update_account(request):
    try:
        request.user.contestant
        return page_update_account_contestant(request)
    except:
        return page_update_account_other(request)


def page_update_account_contestant(request):

    if request.method == "POST":
        account_form = AccountForm(request.POST, instance=request.user)
        contestant_form = ContestantFormNoGDPR(request.POST, instance=request.user.contestant)
        if account_form.is_valid() and contestant_form.is_valid():
            user = account_form.save()
            contestant = contestant_form.save()
            return redirect('contest:account_tagged',tag="updated")
        else:
            return render(request, 'contest/update_account.html',
                          {
                              'account_form': account_form,
                              'contestant_form': contestant_form,
                          })
    else:
        account_form = AccountForm(instance=request.user)
        contestant_form = ContestantFormNoGDPR(instance=request.user.contestant)
        return render(request, 'contest/update_account.html',
                      {
                          'account_form': account_form,
                          'contestant_form': contestant_form,
                      })


def page_update_account_other(request):
    
    if request.method == "POST":
        account_form = AccountForm(request.POST, instance=request.user)
        if account_form.is_valid():
            user = account_form.save()
            return redirect('contest:account_tagged',tag="updated")
        else:
            return render(request, 'contest/update_account.html',
                          {
                              'account_form': account_form,
                          })
    else:
        account_form = AccountForm(instance=request.user)
        return render(request, 'contest/update_account.html',
                      {
                          'account_form': account_form,
                      })

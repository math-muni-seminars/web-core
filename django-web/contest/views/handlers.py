from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.http import Http404, HttpResponse, HttpResponseForbidden
from django.shortcuts import render, get_object_or_404
from django.template.loader import select_template
from django.core.paginator import Paginator
from django.shortcuts import redirect, render
from django.utils import timezone
from django.db.models import Prefetch
from django.db.models.functions import Lower

from contest.models import Contest, TaskCollection, TaskFileSubmission, Task, Contestant
from core.forms import UploadForm
from core.models import MediaFile, Organizer, ContestType
from forum.models import Contribution
from django.views.generic import TemplateView

from core.views import EditablePage, render_editable
from django.forms import Form

import json

"""
Views
"""

@staff_member_required(redirect_field_name='/')
def collection_file_upload(request, collection_id):
    form = UploadForm(['pdf'],4,request.POST,request.FILES)
    if not form.is_valid():
        return HttpResponseForbidden(form.errors["file"])

    f = request.FILES['file']
    collection = get_object_or_404(TaskCollection, pk=collection_id)
    file_type = request.GET["file_type"]
    attrname = f"{file_type}_pdf"
    target_filename = f'brkos/{file_type}_{collection.contest.order}_{collection.order}'

    collection.upload_file(attrname, f, target_filename, [request.user], 1)

    return HttpResponse('Soubor byl úspěšně nahrán.')


@staff_member_required(redirect_field_name='/')
def feedback_upload(request, submission_id):
    form = UploadForm(['pdf','jpg','jpeg','png'],4,request.POST,request.FILES)
    if not form.is_valid():
        return HttpResponseForbidden(form.errors["file"])

    f = request.FILES['file']
    submission = get_object_or_404(TaskFileSubmission, pk=submission_id)
    target_filename = 'feedbacks/'+submission.compose_file_name_feedback()

    submission.upload_file('feedback_file', f, target_filename, owners=[submission.contestant.user, request.user])

    return HttpResponse('Soubor byl úspěšně nahrán.')


@staff_member_required(redirect_field_name='/')
def multiple_upload(request, task_id):
    
    task = get_object_or_404(Task, pk=task_id)
    problem_files = []

    for fname, f in request.FILES.items():
        info = fname.split('_')

        try:
            submission = TaskFileSubmission.get_from_id62(info[0])

            target_filename = 'feedbacks/'+submission.compose_file_name_feedback()

            submission.upload_file('feedback_file', f, target_filename, owners=[submission.contestant.user, request.user])
        except:
            problem_files.append(fname)

    if len(problem_files) == 0:
        return HttpResponse('Soubor byl úspěšně nahrán.')
    else:
        msg = 'Některé soubory se nepodařilo nahrát:'
        for pf in problem_files: msg += '\n'+pf
        return HttpResponse(msg)


@login_required(redirect_field_name='/')
def user_submission(request, contest_order, collection_order, task_order):
    """
    Uploads contestant task submission to the private storage.
    Creates a new submission object in the database.

    Fails if contest, collection or task does not exist or the collection is not active.

    Privacy policy: LOGIN_REQUIRED

    :param request: HTTP request
    :param contest_order: (int)  order of the contest
    :param collection_order: (int) order of the collection int the contest
    :param task_order: (int) order of the task in the collection
    :return: HTTP response
    """

    form = UploadForm(['pdf','jpg','jpeg','png'],4,request.POST,request.FILES)
    if not form.is_valid():
        return HttpResponseForbidden(form.errors["file"])

    f = request.FILES['file']

    task = Task.objects.get(
        collection__order=collection_order,
        collection__contest__order=contest_order,
        order=task_order
    )

    if task.collection.is_closed():
        return HttpResponseForbidden('Nelze odevzdávat po uzavření série.')

    try:
        contestant = request.user.contestant
    except ObjectDoesNotExist:
        return HttpResponseForbidden('Může odevzdávat pouze registrovaný soutěžící.')

    if not contestant.is_allowed():
        return HttpResponseForbidden('Může odevzdávat pouze student střední školy.')

    try:
        submission = TaskFileSubmission.objects.get(contestant=contestant, task=task)
        try:
            submission.content.delete()
        except:
            pass
    except ObjectDoesNotExist:
        submission = TaskFileSubmission(
            contestant=contestant,
            task=task,
        )
        submission.save()

    media_file = MediaFile.objects.create(
        content=f,
        access=2,
        name="submissions/"+submission.compose_file_name()
    )

    media_file.owners.add(request.user)

    submission.score = None
    submission.content = media_file
    submission.save()

    return HttpResponse('Soubor byl úspěšně nahrán.')


@login_required(redirect_field_name='/')
def user_submission_delete(request, contest_order, collection_order, task_order):

    task = Task.objects.get(
        collection__order=collection_order,
        collection__contest__order=contest_order,
        order=task_order
    )

    if task.collection.is_closed():
        return HttpResponseForbidden('Nelze smazat řešení po uzavření série.')

    try:
        contestant = request.user.contestant
    except ObjectDoesNotExist:
        return HttpResponseForbidden('Může odevzdávat pouze registrovaný soutěžící.')

    if not contestant.is_allowed():
        return HttpResponseForbidden('Může odevzdávat pouze student střední školy.')

    try:
        submission = TaskFileSubmission.objects.get(contestant=contestant, task=task)
        submission.content.delete()
    except ObjectDoesNotExist:
        return HttpResponseForbidden('Nebyla dříve odevzdána žádná úloha.')

    submission.content = None
    submission.save()
    submission.delete_if_empty()

    return HttpResponse('Soubor byl odstraněn.')


@login_required(redirect_field_name='/')
def user_submission_save(request, contest_order, collection_order, task_order):

    content = request.POST['submission']

    task = Task.objects.get(
        collection__order=collection_order,
        collection__contest__order=contest_order,
        order=task_order
    )

    if task.collection.is_closed():
        return HttpResponseForbidden('Nelze odevzdat řešení po uzavření série.')

    try:
        contestant = request.user.contestant
    except ObjectDoesNotExist:
        return HttpResponseForbidden('Může odevzdávat pouze registrovaný soutěžící.')

    if not contestant.is_allowed():
        return HttpResponseForbidden('Může odevzdávat pouze student střední školy.')

    try:
        submission = TaskFileSubmission.objects.get(contestant=contestant, task=task)
    except ObjectDoesNotExist:
        submission = TaskFileSubmission(
            contestant=contestant,
            task=task
        )

    submission.online_submission = content
    submission.save()
    submission.delete_if_empty()

    return HttpResponse('Řešení bylo uloženo.')
from itertools import groupby

import unicodedata

from django.db.models import Q
from django.utils import timezone

from django.contrib.auth import get_user_model
from django.db import models
from django.utils.html import format_html
from django.core.exceptions import ValidationError
from django.utils.crypto import get_random_string
from django.shortcuts import get_object_or_404

from core.models import PublishableModel, BaseModel, MediaFile, Organizer

from core.utils import frinedly_base62_encode, frinedly_base62_decode

User = get_user_model()


class Contestant(models.Model):
    """
    Extends the user model by adding the contestant data.
    """

    class Meta:
        verbose_name = "soutěžící"
        verbose_name_plural = "soutěžící"

    user = models.OneToOneField(User, on_delete=models.SET_NULL, null=True)
    graduate_year = models.PositiveSmallIntegerField("rok maturity")
    school = models.CharField("škola", max_length=250)
    math_class = models.BooleanField("matematická třída", default=False)
    # profile_picture = models.ImageField(blank=True, null=True)
    # email_public = models.BooleanField(default=False)

    def grade(self, year=None):
        """
        Computes the grade of the contestant based on the current date and the students graduation year.
        :return: Integer
        """
        if year is None:
            today = timezone.now()
            year = today.year
            if today.month >= 9:
                year += 1
        return max(0, year - self.graduate_year + 4)

    def is_allowed(self):
        if not self.user.verified: return False
        return False if self.grade() > 4 else True

    def __str__(self):
        return self.user.full_name()

    @staticmethod
    def current_contest():
        contest = Contest.objects.order_by('order').last()
        ret = []

        for contestant in Contestant.objects.all():
            if TaskFileSubmission.objects.filter(task__collection__contest = contest, contestant=contestant).count() != 0:
                ret.append(contestant)

        return ret
    
    @staticmethod
    def latest_closed_collection():
        collection = TaskCollection.latest_closed_collection()
        ret = []

        for contestant in Contestant.objects.all():
            if TaskFileSubmission.objects.filter(task__collection = collection, contestant=contestant).count() != 0:
                ret.append(contestant)
            
        return ret
    
    @staticmethod
    def potential():
        ret = []

        for contestant in Contestant.objects.all():
            if contestant.is_allowed(): ret.append(contestant)
            
        return ret


class Contest(PublishableModel):
    class Meta:
        verbose_name = "ročník"
        verbose_name_plural = "ročníky"

    order = models.PositiveSmallIntegerField("pořadí", default=1, unique=True)
    end_year = models.PositiveSmallIntegerField("rok ukončení")

    def __str__(self):
        return "{0}".format(self.order)


class TaskCollection(PublishableModel):
    class Meta:
        verbose_name = "série úloh"
        verbose_name_plural = "série úloh"

        unique_together = ('order', 'contest')

    order = models.PositiveSmallIntegerField("pořadí")
    close_date = models.DateTimeField("datum uzavření", help_text="<p>Po tomto datu nebude možné odevzdávat řešení</p>")
    name = models.CharField("název", max_length=200)
    contest = models.ForeignKey(Contest,
                                on_delete=models.SET_NULL,
                                verbose_name='ročník',
                                related_name='task_collections',
                                default=None,
                                null=True,
                                )

    release_solution = models.BooleanField("zveřejnit řešení", default=False)
    release_results = models.BooleanField("zveřejnit výsledky", default=False)

    assignment_pdf = models.ForeignKey(MediaFile,
                                          on_delete=models.SET_NULL,
                                          verbose_name='zadání pdf',
                                          related_name='brkos_assignment_for',
                                          null=True, blank=True)

    helptext_pdf = models.ForeignKey(MediaFile,
                                          on_delete=models.SET_NULL,
                                          verbose_name='pomocný text pdf',
                                          related_name='brkos_helptext_for',
                                          null=True, blank=True)

    solution_pdf = models.ForeignKey(MediaFile,
                                        on_delete=models.SET_NULL,
                                        verbose_name='řešení pdf',
                                        related_name='brkos_solution_for',
                                        null=True, blank=True)

    scoreboard_pdf = models.ForeignKey(MediaFile,
                                        on_delete=models.SET_NULL,
                                        verbose_name='výsledkovka pdf',
                                        related_name='brkos_scoreboard_for',
                                        null=True, blank=True)

    def __str__(self):
        return "{0}-{1} {2}".format(self.contest,self.order,self.name)

    def clean(self):
        if self.release_results and not self.completed():
            raise ValidationError({'release_results':'Některé úlohy nejsou hotové.'})
        super(TaskCollection,self).clean()


    @classmethod
    def open_collections(cls):
        ## returns all open collections
        return cls.public_objects().filter(close_date__gte=timezone.now())

    @classmethod
    def closed_collections(cls):
        ## returns all closed collections
        return cls.public_objects().filter(close_date__lt=timezone.now())

    @staticmethod
    def q_open():
        return Q(PublishableModel.q_public(), close_date__gte=timezone.now())

    @staticmethod
    def q_closed():
        return Q(PublishableModel.q_public(), close_date__lt=timezone.now())

    def is_open(self):
        return self.close_date >= timezone.now()

    def is_closed(self):
        return self.close_date < timezone.now()

    @staticmethod
    def latest_closed_collection():
        return TaskCollection.closed_collections() \
            .select_related('contest').order_by('contest__order', 'order').last()

    @staticmethod
    def first_open_collection():
        return TaskCollection.open_collections() \
            .select_related('contest').order_by('contest__order', 'order').first()

    @staticmethod
    def latest_results_collection():
        return TaskCollection.closed_collections().filter(release_results=True) \
            .select_related('contest').order_by('contest__order', 'order').last()

    @staticmethod
    def current_collection():
        collection = TaskCollection.first_open_collection()
        if collection is None:
            collection = TaskCollection.latest_closed_collection()
        return collection

    @staticmethod
    def current_collection_with_results():
        collection = TaskCollection.latest_results_collection()
        if collection is None:
            collection = TaskCollection.latest_closed_collection()
        return collection

    def ready(self):
        try:
            story = Story.objects.get(collection=self)
            if not story.ready:
                return False
        except:
            return False
        for task in Task.objects.filter(collection=self):
            if not task.ready:
                return False
        return True

    def completed(self):
        for task in Task.objects.filter(collection=self):
            if not task.completed:
                return False
        return True

    ready.short_description = "úlohy připraveny pro zadání"
    ready.boolean = True
    completed.short_description = "hotovo"
    completed.boolean = True


class Task(BaseModel):
    class Meta:
        verbose_name = "úloha"
        verbose_name_plural = "úlohy"

        unique_together = ('order', 'collection')

    order = models.SmallIntegerField("pořadí", null=True, blank=True, help_text="<p>1–8</p>")
    collection = models.ForeignKey(TaskCollection,
                                   on_delete=models.CASCADE,
                                   verbose_name='série',
                                   related_name='tasks',
                                   null=True, blank=True,
                                   )
    name = models.CharField("název", max_length=200, help_text="<p>1–4, A–D</p>")

    text = models.TextField("text úlohy", null=True, blank=True)

    model_solution = models.TextField("vzorové řešení", null=True, blank=True)

    comment = models.TextField("komentář", null=True, blank=True)

    max_score = models.PositiveSmallIntegerField("maximální skóre", null=True, blank=True)

    proxy = models.ForeignKey(Organizer,
                                   on_delete=models.SET_NULL,
                                   verbose_name='zmocněnec',
                                   related_name='assigned_tasks',
                                   null=True, blank=True,
                                   help_text="<p>Organizátor, který má úlohu na starost</p>")

    ready = models.BooleanField("připraveno pro zadání", default=False)
    completed = models.BooleanField("hotovo", default=False, help_text="<p>Nachystané řešení a komentář a opravená všechna řešení</p>")

    def __str__(self):
        if self.collection is None: return self.name
        return "{0}-{1}-{2}".format(self.collection.contest, self.collection.order, self.name)

    def submissions(self):
        return self.file_submissions.prefetch_related('content').all()

    def submission_files(self):
        files = []
        for f in self.file_submissions.prefetch_related('content').all():
            if f.content is not None:
                files.append(f.content.content.path)
        return files

    def clean(self):
        if self.completed:
            errors = ""

            if self.model_solution is None or self.model_solution=="":
                errors += 'Chybí řešení. '
            if self.comment is None or self.comment=="":
                errors += 'Chybí komentář. '
            for submission in TaskFileSubmission.objects.filter(task=self):
                if submission.score is None or (submission.feedback_file is None and submission.feedback_text==""):
                    errors += 'Nebyly opraveny všechna odevzdaná řešení. '
        
            if errors != "":
                raise ValidationError({'completed':errors.rstrip(" ")})
        
        if self.ready:
            errors = ""

            if self.text is None or self.text=="":
                errors += 'Chybí text úlohy. '
            if self.max_score is None:
                errors += 'Chybí maximální skóre. '
                
            if errors != "":
                raise ValidationError({'ready':errors.rstrip(" ")})


class Story(BaseModel):
    class Meta:
        verbose_name = "příběh"
        verbose_name_plural = "příběhy"

    text = models.TextField("text příběhu",
                            help_text="""
                            <p>Pro rozdělení příběhu úlohou, vložte na místo úlohy řetězec `&lt;task&gt;`.</p>
                            <p>Příklad: Tohle je úvod pohádky. &lt;task&gt; Tohle bude za první úlohou.
                            &lt;task&gt; Tohle bude za druhou úlohou.</p>
                            """)
    collection = models.OneToOneField(TaskCollection,
                                      on_delete=models.CASCADE,
                                      verbose_name='série',
                                      related_name='story',
                                      )
    ready = models.BooleanField("připraveno pro zadání", default=False)


class TaskFileSubmission(BaseModel):
    class Meta:
        verbose_name = "řešení"
        verbose_name_plural = "řešení"
        unique_together = ('contestant', 'task')
        ordering = ('-id',)

    contestant = models.ForeignKey(Contestant,
                                   models.CASCADE,
                                   verbose_name='soutěžící',
                                   related_name='file_submissions',
                                   )
    task = models.ForeignKey(Task,
                             models.CASCADE,
                             verbose_name='úloha',
                             related_name='file_submissions',
                             )

    content = models.ForeignKey(
        MediaFile,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name="soubor",
        related_name="submission_to",
    )

    online_submission = models.TextField("řešení", default="", blank=True)

    score = models.FloatField("body", default=None, blank=True, null=True)

    feedback_text = models.TextField("komentář", default="", blank=True)
    feedback_file = models.ForeignKey(
        MediaFile,
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name="komentář (soubor)",
        related_name="feedback_to",
    )

    def clean(self):
        if self.score is not None:
            if self.score > self.task.max_score:
                raise ValidationError({'score':'Přesažen maximální počet bodů této úlohy.'})
            if self.score < 0:
                raise ValidationError({'score':'Počet bodů musí být nezáporný.'})

    @property
    def graded(self):
        return self.score is not None

    def file_link(self):
        if self.content is None:
            return "No file"
        else:
            return format_html("<a href='{}'>Soubor<a>", self.content.content.url)
    file_link.short_description = "řešení (soubor)"

    def file_url(self):
        return self.content.content.url if self.content is not None else ""

    def max_score(self):
        return self.task.max_score
    max_score.short_description = "maximální skóre"

    def delete_if_empty(self):
        if(self.content is None and self.online_submission == "" and self.score is None and self.feedback_text == "" and self.feedback_file is None):
            self.delete()

    def compose_file_name(self):
        return "{0}_{1}_{2}-{3}-{4}{5}".format(frinedly_base62_encode(self.id), get_random_string(length=3), self.task.collection.order, self.task.name,
                                                unicodedata.normalize('NFKD',self.contestant.user.last_name).encode("ASCII","ignore").decode(),
                                                unicodedata.normalize('NFKD',self.contestant.user.first_name).encode("ASCII","ignore").decode())

    def compose_file_name_feedback(self):
        return "{0}_{1}_{2}-{3}".format(frinedly_base62_encode(self.id), get_random_string(length=3), self.task.collection.order, self.task.name)

    @staticmethod
    def get_from_id62(id62):
        return TaskFileSubmission.objects.get(id=frinedly_base62_decode(id62))

"""
class School(BaseModel):
    class Meta:
        verbose_name = "škola"
        verbose_name_plural = "školy"

    name = models.CharField('název', max_length=60)
    full_name = models.CharField('celý název', max_length=150)
    hidden = models.BooleanField(default=False, blank=True)

    city = models.CharField('město', max_length=60)
    street = models.CharField('ulice', max_length=60)
    postal_code = models.CharField('PSČ', max_length=10)

    COUNTRY_CHOICES = (
        ('cz', 'Česko'),
        ('sk', 'Slovensko'),
    )
    country = models.CharField('stát', max_length=4, choices=COUNTRY_CHOICES)
"""

class ResultsTable:
    def __init__(self, contest, collection):
        self.contest = contest
        self.collection = collection

    no_submission_char = '-'
    not_graded_char = '?'

    def get_head_cumulative(self):
        head = ['', 'Jméno', 'Ročník', 'Škola', 'M'] \
               + [str(i) for i in range(1, self.collection.order + 1)] \
               + ['$\Sigma$']
        return head

    def get_head(self):
        head = ['', 'Jméno', 'Ročník', 'Škola', 'M'] \
               + list(self.collection.tasks.order_by('order').values_list('name', flat=True)) \
               + ['$\Sigma$']
        return head

    @staticmethod
    def final_score(score, year, math_class=False):
        """
            Compute the final score for collection according to: score sum for all tasks, grade, math class.
        """
        if math_class:
            year += 1

        method_1 = score * (18 - year) / 12
        method_2 = 24 - (24 - score) * (6 + year) / 12

        return min(method_1,method_2)

        """
        if score > 12:
            return score + (24.0 - score) * (5 - year) / 12.0
        else:
            return score + score * (5 - year) / 12.0
        """

    @staticmethod
    def compute_overall_score(collection_submissions, year, math_class=False):
        """
            Compute score sum and final score for given set of submissions in one collection.
        """
        score_sum = sum(sorted([sub.score if sub.score is not None else 0 for sub in collection_submissions],
                      key=lambda x: -x)[:6])
        return score_sum, ResultsTable.final_score(score_sum, year, math_class)

    def get_empty_row(self, contestant, cumulative=False):
        row = []
        head_len = len(self.get_head_cumulative() if cumulative else self.get_head())
        for _ in range(head_len):
            cell = {"type_cell": "plain", "value": self.no_submission_char}
            row.append(cell)
        row[1]["type_cell"] = "user_url"
        row[1]["username"] = contestant.user.username
        row[1]["value"] = contestant.user.full_name()
        row[2]["value"] = contestant.grade(self.contest.end_year)
        row[3]["value"] = contestant.school
        row[4]["value"] = 'M' if contestant.math_class else ''
        return row

    def get_results_table(self, submissions, cumulative=False):
        table = []
        for contestant, subs in submissions:
            if contestant.user.is_staff:
                continue
            subs = list(subs)
            if cumulative:
                row = self.get_empty_row(contestant, True)
                suma = 0
                collections = groupby(subs, key=lambda sub: sub.task.collection.order)

                for order, collection_submissions in collections:
                    collection_submissions = list(collection_submissions)
                    _, final_score = self.compute_overall_score(
                        collection_submissions,
                        contestant.grade(self.contest.end_year),
                        contestant.math_class
                    )
                    row[order + 4]["value"] = round(final_score, 2)
                    suma += final_score
                row[-1]["value"] = round(suma, 2)
            else:
                row = self.get_empty_row(contestant)
                for task_sub in subs:
                    if task_sub.score is None:
                        row[task_sub.task.order + 4]["value"] = self.not_graded_char
                    elif row[task_sub.task.order + 4]["value"] == self.no_submission_char:
                        row[task_sub.task.order + 4]["value"] = round(task_sub.score, 2)
                    else:
                        row[task_sub.task.order + 4]["value"] += round(task_sub.score, 2)
                _, final_score = self.compute_overall_score(subs, contestant.grade(self.contest.end_year),
                                                            contestant.math_class)
                row[-1]["value"] = round(final_score, 2)
            table.append(row)
        table.sort(key=lambda x: x[-1]["value"], reverse=True)

        for i in range(1, len(table) + 1):
            table[i - 1][0]["value"] = i
        return table

    def get_results_cumulative(self):
        """
            Generates head and result table for specific contest (entries of the table are task collections)
        """

        submissions = TaskFileSubmission.objects.filter(task__collection__contest=self.collection.contest,
                                                        task__collection__order__lte=self.collection.order)\
            .order_by('contestant') \
            .select_related('task').prefetch_related('task__collection')
        submissions = groupby(submissions, key=lambda x: x.contestant)

        return self.get_head_cumulative(), self.get_results_table(submissions, True)

    def get_results_collection(self):
        """
            Generates head and result table for specific collection (entries of the table are tasks).
        """
        submissions = TaskFileSubmission.objects.filter(task__collection=self.collection)\
            .order_by('contestant') \
            .select_related('task')
        submissions = groupby(submissions, key=lambda x: x.contestant)
        return self.get_head(), self.get_results_table(submissions)
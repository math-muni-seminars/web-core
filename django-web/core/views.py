from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden, HttpResponse, Http404
from django.shortcuts import get_object_or_404, render
from django.template.loader import select_template
from django.views.generic import TemplateView
from django.views.static import serve

from core.models import MediaFile, Text, StaticPage, ContestType, Mail
from core.templatetags.extras import gen_file_refs, markdown_to_html, latex_to_html
from contest.models import Contest as BContest
from mathrace.models import Contest as MContest

from django.core.mail import EmailMessage
from django.conf import settings
from django.utils import timezone


@login_required
def private_serve(request, path, document_root=None, show_indices=None):
    """
    Serves a file from the private storage. The file is returned only if it's requested by its owner or by an admin.

    Privacy policy: LOGIN REQUIRED

    :param request: HTTP request
    :param path: Path to the file
    :param document_root: Prefix to be prepended to the path
    :return: HTTP response
    """

    name = '.'.join(path.split('.')[:-1])

    file = get_object_or_404(MediaFile, name=name)

    if request.user.is_staff or (request.user in file.owners.all()):
        return serve(request, path, document_root, show_indices)
    else:
        return HttpResponseForbidden("Přístup odepřen")


def protected_serve(request, path, document_root=None, show_indices=None):
    allowed = False
    contest_type = path.split('/')[0]
    name = path.split('/')[1].split('.')[0]
    info = name.split('_')
    
    try:
        if contest_type == "brkos":
            contest = BContest.public_objects().get(order=info[1])
        if contest_type == "mathrace":
            contest = MContest.public_objects().get(order=info[1])
    except:
        raise Http404("Ročník nenalezen")

    collection = contest.task_collections.filter(order=info[2])[0]
    if collection is None:
        raise Http404("Série nenalezena")
    
    if info[0] in {"assignment", "helptext"} and collection.is_public():
        allowed = True
    if info[0] == "solution" and collection.release_solution:
        allowed = True
    if info[0] == "scoreboard" and collection.release_results:
        allowed = True
        
    if request.user.is_staff or allowed:
        return serve(request, '%s/%s_%s_%s.pdf' % (contest_type, info[0], info[1], info[2]), document_root, show_indices)
    else:
        return HttpResponseForbidden("Přístup odepřen")


def page_static(request, path):
    page = get_object_or_404(StaticPage, endpoint='/' + path)

    namesapce = request.resolver_match.namespace

    template = select_template([namesapce+'/static_page.html', 'core/static_page.html'])

    return HttpResponse(template.render({'page': page}, request))


def handler_markdown_to_html(request):
    text = request.body.decode('utf-8')
    html = markdown_to_html(gen_file_refs(text))
    html = html.replace("<task>","<div style='background-color: #ddd; border: 1px solid black; border-radius: 5px; padding: 5px; margin: 10px 0px;'>Úloha</div>")
    return HttpResponse(html)


def handler_latex_to_html(request):
    text = request.body.decode('utf-8')
    html = latex_to_html(gen_file_refs(text))
    return HttpResponse(html)


class EditablePage(TemplateView):
    tags = []
    
    """
    def get_template_names(self):
        if self.template_name is None:
            raise ImproperlyConfigured(
                "TemplateResponseMixin requires either a definition of "
                "'template_name' or an implementation of 'get_template_names()'")
        else:
            namespace = self.request.resolver_match.namespace.split(':')[-1]
            template = ("contest" if namespace=="brkos" else namespace) + "/" + self.template_name
            return [template] 
    """
    

    def get_context_data(self, **kwargs):
        texts = Text.objects.filter(template=self.template_name, name__in=self.tags)
        context = {text.name: text.content for text in texts}
        editable = [{"name": text.name, "id": text.id} for text in texts]
        context["editable"] = editable
        return context


def render_editable(request, template, context):
    texts = Text.objects.filter(template=template)
    for text in texts: context[text.name] = text.content
    editable = [{"name": text.name, "id": text.id} for text in texts]
    context["editable"] = editable

    return render(request, template, context)


def handler_test_mail(request,id):
    mail = Mail.objects.get(id=id)
    content = mail.content

    try:
        if mail.in_html:
            content = markdown_to_html(content)
        email = EmailMessage(mail.subject,content,'"BRKOS" <'+settings.EMAIL_HOST_USER+'>',[],[request.user.email])
        if mail.in_html:
            email.content_subtype = "html"
        if mail.attachment1.name: email.attach_file(mail.attachment1.path)
        if mail.attachment2.name: email.attach_file(mail.attachment2.path)
        if mail.attachment3.name: email.attach_file(mail.attachment3.path)

        if request.user.is_staff: email.send(fail_silently = False)
        else: raise Exception()

        return HttpResponse("<script>if(!alert('Odeslání bylo úspěšné.')) location.href = '/admin/core/mail/';</script>")
    except:
        return HttpResponse("<script>if(!alert('Odeslání se nezdařilo.')) location.href = '/admin/core/mail/';</script>")

def handler_send_mail(request,id):
    mail = Mail.objects.get(id=id)
    content = mail.content

    try:
        if mail.in_html:
            content = markdown_to_html(content)
        email = EmailMessage(mail.subject,content,'"BRKOS" <'+settings.EMAIL_HOST_USER+'>',[],mail.recipients())
        if mail.in_html:
            email.content_subtype = "html"
        if mail.attachment1.name: email.attach_file(mail.attachment1.path)
        if mail.attachment2.name: email.attach_file(mail.attachment2.path)
        if mail.attachment3.name: email.attach_file(mail.attachment3.path)

        if request.user.is_staff: email.send(fail_silently = False)
        else: raise Exception()

        mail.sent = True
        mail.sent_time = timezone.now()
        mail.save()

        return HttpResponse("<script>if(!alert('Odeslání bylo úspěšné.')) location.href = '/admin/core/mail/';</script>")
    except:
        return HttpResponse("<script>if(!alert('Odeslání se nezdařilo.')) location.href = '/admin/core/mail/';</script>")

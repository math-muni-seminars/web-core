# Generated by Django 3.1.6 on 2023-06-24 19:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0027_auto_20230623_1927'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mail',
            name='subject',
            field=models.CharField(default='BRKOS ', max_length=255, verbose_name='předmět'),
        ),
    ]

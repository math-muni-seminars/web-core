# Generated by Django 3.1.6 on 2022-03-20 18:08

import core.models
from django.conf import settings
import django.contrib.auth.models
import django.contrib.auth.validators
import django.core.files.storage
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('first_name', models.CharField(max_length=30, verbose_name='jméno')),
                ('last_name', models.CharField(max_length=30, verbose_name='příjmení')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
                'abstract': False,
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='StaticPage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(default=django.utils.timezone.now, editable=False, verbose_name='vytvořeno')),
                ('updated', models.DateTimeField(default=django.utils.timezone.now, editable=False, verbose_name='upraveno')),
                ('endpoint', models.CharField(default='', max_length=30, verbose_name='URL lokace')),
                ('title', models.CharField(default='', max_length=30, verbose_name='title')),
                ('name', models.CharField(default='', max_length=30, verbose_name='název v menu')),
                ('content', models.TextField(default='', verbose_name='obsah')),
            ],
            options={
                'verbose_name': 'statická stránka',
                'verbose_name_plural': 'statické stránky',
            },
        ),
        migrations.CreateModel(
            name='Text',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('template', models.CharField(max_length=255, verbose_name='template')),
                ('name', models.SlugField(max_length=255, verbose_name='jméno textu')),
                ('content', models.TextField(verbose_name='obsah')),
            ],
            options={
                'verbose_name': 'text',
                'verbose_name_plural': 'texty',
            },
        ),
        migrations.CreateModel(
            name='Organizer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nickname', models.CharField(max_length=30, unique=True, verbose_name='přezdívka')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'organizátor',
                'verbose_name_plural': 'organizátoři',
            },
        ),
        migrations.CreateModel(
            name='Menu',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30, verbose_name='jméno')),
                ('locations', models.TextField(verbose_name='lokace')),
                ('pages', models.ManyToManyField(to='core.StaticPage')),
            ],
            options={
                'verbose_name': 'menu',
                'verbose_name_plural': 'menu',
            },
        ),
        migrations.CreateModel(
            name='MediaFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(default=django.utils.timezone.now, editable=False, verbose_name='vytvořeno')),
                ('updated', models.DateTimeField(default=django.utils.timezone.now, editable=False, verbose_name='upraveno')),
                ('public', models.BooleanField(verbose_name='veřejné')),
                ('content', models.FileField(storage=django.core.files.storage.FileSystemStorage('/Users/martin/Work/brkos/web-core/django-web/data/storage', '/'), upload_to=core.models.MediaFile.path_in_storage, verbose_name='soubor')),
                ('name', models.CharField(max_length=255, unique=True, verbose_name='cesta/jméno')),
                ('owners', models.ManyToManyField(to=settings.AUTH_USER_MODEL, verbose_name='vlastník')),
            ],
            options={
                'verbose_name': 'soubor',
                'verbose_name_plural': 'soubory',
            },
        ),
        migrations.CreateModel(
            name='Contribution',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(default=django.utils.timezone.now, editable=False, verbose_name='vytvořeno')),
                ('updated', models.DateTimeField(default=django.utils.timezone.now, editable=False, verbose_name='upraveno')),
                ('published', models.BooleanField(blank=True, default=False, verbose_name='publikováno')),
                ('public_from', models.DateTimeField(blank=True, null=True, verbose_name='publikováno od')),
                ('public_to', models.DateTimeField(blank=True, null=True, verbose_name='publikováno do')),
                ('title', models.CharField(max_length=60, verbose_name='nadpis')),
                ('text', core.models.RichTextField(verbose_name='text novinky')),
                ('tags', core.models.TagField(blank=True, default='', verbose_name='tagy')),
                ('author', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='core.organizer', verbose_name='autor')),
            ],
            options={
                'verbose_name': 'novinka',
                'verbose_name_plural': 'novinky',
                'ordering': ['-public_from', '-created'],
            },
        ),
    ]

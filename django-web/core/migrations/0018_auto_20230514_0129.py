# Generated by Django 3.1.6 on 2023-05-13 23:29

import django.core.files.storage
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0017_auto_20230514_0112'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mail',
            name='attachment1',
            field=models.FileField(blank=True, null=True, storage=django.core.files.storage.FileSystemStorage('/Users/Ondrasek/Downloads/BRKOS/web-core/django-web/data/storage/private', 'private/'), upload_to='attachments/', verbose_name='příloha 1'),
        ),
        migrations.AlterField(
            model_name='mail',
            name='attachment2',
            field=models.FileField(blank=True, null=True, storage=django.core.files.storage.FileSystemStorage('/Users/Ondrasek/Downloads/BRKOS/web-core/django-web/data/storage/private', 'private/'), upload_to='attachments/', verbose_name='příloha 2'),
        ),
        migrations.AlterField(
            model_name='mail',
            name='attachment3',
            field=models.FileField(blank=True, null=True, storage=django.core.files.storage.FileSystemStorage('/Users/Ondrasek/Downloads/BRKOS/web-core/django-web/data/storage/private', 'private/'), upload_to='attachments/', verbose_name='příloha 3'),
        ),
        migrations.AlterField(
            model_name='organizer',
            name='photo',
            field=models.FileField(blank=True, null=True, upload_to='organizers/', verbose_name='fotka'),
        ),
    ]

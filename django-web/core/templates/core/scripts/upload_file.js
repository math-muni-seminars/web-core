function upload_file(form_descriptor, url) {
    var fd = new FormData();
    var files = $(form_descriptor)[0].files[0];
    fd.append('file', files);

    $.ajax({
        url: url,
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
        headers: { "X-CSRFToken": '{{ csrf_token }}' },
        success: function(response){
            alert(response);
        },
        error: function (request, status, error) {
            alert(request.responseText);
        }
    });
}
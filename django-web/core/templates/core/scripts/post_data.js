function post_data(url, data, fn_succ, fn_err) {
    $.ajax({
        type: "POST",
        data: data,
        url: url,
        headers: { "X-CSRFToken": '{{ csrf_token }}' },
        success: function(response){
            fn_succ(response);
        },
        error: function (response, status, error) {
            fn_err(response.responseText);
        }
    });
}
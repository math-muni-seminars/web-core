from django.db import models
from django.forms import forms
from django.urls import path
from django.template.response import TemplateResponse

from . import widgets, settings
from .models import User, Organizer, MediaFile, Text, RichTextField, Mail, StaticPage, ContestType
from django.contrib import admin, messages

import csv
from django.http import HttpResponse

from contest.models import Contestant
from camp.models import CampApplication
from mathrace.models import Team
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.admin.models import LogEntry
from django.utils.html import format_html
from django.template.loader import render_to_string


ADMIN_ORDERING = (
    ('core', ('User', 'Organizer', 'Mail', 'ContestType', 'Text', 'MediaFile')),
    ('contest', ('Contestant', 'Contest', 'TaskCollection', 'Story', 'Task', 'TaskFileSubmission', 'RateTask', 'Stats')),
    ('mathrace', ('Team', 'Contest', 'TaskCollection', 'Task', 'ContestStatus', 'Attempt')),
    ('camp', ('Camp', 'CampApplication')),
    ('forum', ('Contribution', 'Topic', 'Thread', 'Post', 'Comment')),
    ('auth', ('Group')),
    ('admin', ('LogEntry')),
)

def get_app_list(self, request, app_label=None):
    app_dict = self._build_app_dict(request, app_label)
    
    if not app_dict:
        return
        
    NEW_ADMIN_ORDERING = []
    if app_label:
        for ao in ADMIN_ORDERING:
            if ao[0] == app_label:
                NEW_ADMIN_ORDERING.append(ao)
                break
    
    if not app_label:
        for app_key in list(app_dict.keys()):
            if not any(app_key in ao_app for ao_app in ADMIN_ORDERING):
                app_dict.pop(app_key)
    
    app_list = sorted(
        app_dict.values(), 
        key=lambda x: [ao[0] for ao in ADMIN_ORDERING].index(x['app_label'])
    )
     
    for app, ao in zip(app_list, NEW_ADMIN_ORDERING or ADMIN_ORDERING):
        if app['app_label'] == ao[0]:
            for model in list(app['models']):
                if not model['object_name'] in ao[1]:
                    app['models'].remove(model)
        app['models'].sort(key=lambda x: ao[1].index(x['object_name']))
    return app_list

admin.AdminSite.get_app_list = get_app_list


class PublishableAdmin(admin.ModelAdmin):

    def save_model(self, request, obj, form, change):
        super(PublishableAdmin,self).save_model(request, obj, form, change)
        name = obj.__class__._meta.verbose_name + " " + str(obj)
        if obj.is_public():
            if obj.public_to:
                messages.add_message(request, messages.INFO, "{} je momentálně veřejný/á/é (do {}).".format(name.capitalize(),obj.public_to.strftime("%d.%m.%Y %H:%M:%S")))
            else:
                messages.add_message(request, messages.INFO, "{} je veřejný/á/é.".format(name.capitalize()))
        else:
            if obj.public_from:
                messages.add_message(request, messages.WARNING, "Pozor, {} je momentálně neveřejný/á/é. (Bude zveřejněn/a/o {}.)".format(name,obj.public_from.strftime("%d.%m.%Y %H:%M:%S")))
            else:
                messages.add_message(request, messages.WARNING, "Pozor, {} je neveřejný/á/é.".format(name))


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['username', 'full_name', 'email', 'verified', 'is_staff', 'is_organizer', 'is_contestant']
    fields = ['username','first_name','last_name','email', 'verified','is_staff','groups','last_login','date_joined']
    search_fields = ['username','first_name','last_name','email']
    readonly_fields = ['last_login','date_joined']

    def get_name(self, obj):
        return obj.full_name()
    get_name.short_description = "jméno"

    def is_organizer(self, obj):
        try:
            obj.organizer
            return True
        except:
            return False

    is_organizer.boolean = True
    is_organizer.short_description = "organizátor"

    def is_contestant(self, obj):
        try:
            obj.contestant
            return True
        except:
            return False

    is_contestant.boolean = True
    is_contestant.short_description = "soutěžící"


@admin.register(Organizer)
class OrganizerAdmin(admin.ModelAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['nickname', 'get_name', 'get_email', 'active']
    fields = ['user','get_name','get_email','nickname','photo','active']
    readonly_fields = ['get_name','get_email']
    search_fields = ['nickname','user__username','user__first_name','user__last_name']
    autocomplete_fields = ['user']

    def get_name(self, obj):
        return obj.user.full_name()
    get_name.short_description = "jméno"

    def get_email(self, obj):
        return obj.user.email
    get_email.short_description = 'email'

"""
@admin.register(StaticPage)
class StaticPageAdmin(admin.ModelAdmin):
    list_display = ['name']
    formfield_overrides = {
        RichTextField: {'widget': widgets.MarkdownTextField}
    }


@admin.register(Menu)
class MenuAdmin(admin.ModelAdmin):
    list_display = ['name']
"""

@admin.register(MediaFile)
class MediaFileAdmin(admin.ModelAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['name', 'created','size']
    fields = ['name','access','owners','size','content']
    readonly_fields = ['size']
    radio_fields = {'access': admin.HORIZONTAL}

    def size(self, obj):
        try:
            sz = obj.content.size / 1000
            if sz < 10:
                return str(round(sz,1)) + " kB"
            if sz < 1000:
                return str(int(sz)) + " kB"
            if sz < 10000:
                return str(round(sz/1000,1)) + " MB"
            return str(int(sz // 1000)) + " MB"
        except:
            return None
    size.short_description = "velikost"


@admin.register(Text)
class TextAdmin(admin.ModelAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['template', 'name']
    formfield_overrides = {
        models.TextField: {'widget': widgets.MarkdownTextField}
    }

@admin.register(Mail)
class MailAdmin(admin.ModelAdmin):
    list_display = [ 'subject', 'to', 'sent', 'sent_time','send_links']
    fields = ['to','subject','sent','sent_time', 'attachment1','attachment2','attachment3','in_html','content']
    readonly_fields = ['sent','sent_time']
    search_fields = ['subject','content']
    radio_fields = {'to': admin.VERTICAL}
    formfield_overrides = {
        models.TextField: {'widget': widgets.MarkdownTextField}
    }
    save_as = True

    def send_links(self, obj):
        return format_html(render_to_string('core/parts/send_links.html', {'mail': obj}))
    send_links.short_description = "Odeslání"

@admin.register(LogEntry)
class LogEntryAdmin(admin.ModelAdmin):
    list_display = ['action_time','user','__str__']
    
    def has_add_permission(self, request, obj=None):
        return False
    
    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


@admin.register(ContestType)
class ContestTypeAdmin(admin.ModelAdmin):
    change_list_template = 'smuggler/change_list.html'
    list_display = ['name', 'abbreviation']
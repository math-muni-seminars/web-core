from django.contrib import admin
from django.urls import path, include
from core import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', include('smuggler.urls')),
    path('admin/', admin.site.urls),

    path('api/md_to_html', views.handler_markdown_to_html, name='md_to_html'),
    path('api/latex_to_html', views.handler_latex_to_html, name='latex_to_html'),
    path('api/', include('contest.api')),

    path('mathrace/', include('mathrace.urls', namespace='mathrace')),
    path('', include('contest.urls', namespace='brkos')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + [
    path('%s<path:path>' % settings.PRIVATE_URL, views.private_serve, {'document_root': settings.PRIVATE_ROOT}),
    path('%s<path:path>' % settings.PROTECTED_URL, views.protected_serve, {'document_root': settings.PROTECTED_ROOT}),
]


from datetime import datetime

from django.utils import timezone
from django.utils.timezone import make_aware

from camp.models import Camp
from core.models import User, ContestType, Organizer
from contest.models import Contest, TaskCollection, Contestant, Task, TaskFileSubmission
from forum.models import Topic, Post, Comment, Thread, Contribution
from os.path import join
import csv
from django.conf import settings
from django.contrib.auth.models import Group

from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Loads data from the csv folder.'

    # def add_arguments(self, parser):
    #     parser.add_argument('poll_ids', nargs='+', type=int)

    def handle(self, *args, **options):
        """
        Comment.objects.all().delete()
        Post.objects.all().delete()
        Thread.objects.all().delete()
        Topic.objects.all().delete()
        """
        
        print('parsing: csv/br_schools.csv')
        with open(join(settings.BASE_DIR, 'csv/br_schools.csv'), 'r') as f:
            reader = csv.reader(f)
            headers = next(reader)[1:]
            headers.insert(0, 'id')
            print(headers)
            schools = []
            for row in reader:
                schools.append({key: value for key, value in zip(headers, row)})

            school_dict = {}
            for school_data in schools:
                school_dict[int(school_data['id'])] = school_data['fullname']

            """
            for school_data in schools:
                try:
                    School.objects.get_or_create(
                        id=int(school_data['id']),
                        name=school_data['name'],
                        full_name=school_data['fullname'],
                        postal_code=school_data['postalcode'],
                        country=school_data['state'],
                        street=school_data['street'],
                        city=school_data['city'],
                        hidden=(school_data['hidden'] == '1'),
                    )
                except Exception as e:
                    print("error occurred")
                    print(school_data.values())
                    print(e)
            """
        

        print('parsing csv/br_users.csv')
        with open(join(settings.BASE_DIR, 'csv/br_users.csv'), 'r') as f:
            reader = csv.reader(f)
            headers = next(reader)[1:]
            headers.insert(0, 'id')
            print(headers)
            users = []
            for row in reader:
                users.append({key: value for key, value in zip(headers, row)})

            #User.objects.filter(is_staff=False, is_superuser=False).delete()

            contestant_dict = {}
            user_dict = {}

            organizer_group, _ = Group.objects.get_or_create(name="organizátor")

            for i, user_data in enumerate(users):
                try:
                    user, created = User.objects.get_or_create(
                        #id=user_data['id'],
                        username=user_data['nick'],
                        email=user_data['mail'],
                        #password=user_data['pass'],
                        first_name=user_data['first'],
                        last_name=user_data['last'],
                        verified=True,
                    )
                    user_dict[int(user_data['id'])] = user
                except Exception as e:
                    print("error occurred")
                    print(user_data)
                    print(e)

                """
                try:
                    o = School.objects.get(id=int(user_data['schoolid']))
                    school = o.name + " " + o.city
                except:
                    pass
                    # school = user_data.get('school')
                """

                school = user_data['school']
                if school=="":
                    try:
                        school = school_dict[int(user_data['schoolid'])]
                    except:
                        pass

                try:
                    contestant, created = Contestant.objects.get_or_create(
                        school=school,
                        math_class=(user_data['math'] == 'mat'),
                        user=user,
                        graduate_year=user_data['graduate'],
                    )
                    contestant_dict[int(user_data['id'])] = contestant
                except Exception as e:
                    print("error occurred")
                    print(user_data.values())
                    print(e)

                if(int(user_data['rights'])==4):
                    Organizer.objects.get_or_create(
                        user=user,
                        nickname=user_data['nick'],
                    )
                    user.groups.add(organizer_group)
                    user.is_staff = True
                    user.save()

                if(int(user_data['rights'])==3):
                    Organizer.objects.get_or_create(
                        user=user,
                        nickname=user_data['nick'],
                        active=False
                    )


        print('parsing csv/br_series.csv')
        with open(join(settings.BASE_DIR, 'csv/br_series.csv'), 'r') as f:
            reader = csv.reader(f)
            headers = next(reader)
            headers[0] = 'year'
            print(headers)
            series = []
            for row in reader:
                series.append({key: value for key, value in zip(headers, row)})

            """
            ContestType.objects.all().delete()
            Contest.objects.all().delete()
            TaskCollection.objects.all().delete()
            Task.objects.all().delete()
            """

            try:
                for s_data in series:
                    contest, _ = Contest.objects.get_or_create(
                        order=s_data['year'],
                        published=True,
                        end_year=int(s_data['year']) - 27 + 2021
                    )

                    names = ['1', '2', '3', '4', 'A', 'B', 'C', 'D']

                    collection, _ = TaskCollection.objects.get_or_create(
                        order=s_data['no'],
                        published=True,
                        contest=contest,
                        name=s_data['topic'],
                        close_date=make_aware(datetime.fromtimestamp(int(s_data['date']))),
                        release_solution=True,
                        release_results=True,
                    )

                    if int(contest.order) >= 25:
                        tn = 8
                        max_scores = [3,3,4,5,3,3,4,5]
                    else:
                        tn = 7
                        max_scores = [4]*7

                    for i in range(tn):
                        Task.objects.get_or_create(
                            order=i+1,
                            name=names[i],
                            collection=collection,
                            max_score=max_scores[i],
                        )
            except Exception as e:
                print("error occurred")
                print(s_data.values())
                print(e)

        """
        print('parsing csv/br_results.csv')
        with open(join(settings.BASE_DIR, 'csv/br_results.csv'), 'r') as f:
            reader = csv.reader(f)
            headers = next(reader)
            headers[0] = 'id'
            print(headers)
            results = []
            for row in reader:
                results.append({key: value for key, value in zip(headers, row)})

            for result_data in results:
                collection_order = int(result_data['serie']) % 10
                contest_order = int(result_data['serie']) // 10

                if contest_order < 25:
                    continue

                try:
                    TaskFileSubmission.objects.get_or_create(
                        contestant=contestant_dict[int(result_data['userid'])],
                        task=Task.objects.get(order=int(result_data['problem']),
                                              collection__order=collection_order,
                                              collection__contest__order=contest_order),
                        score=result_data['points'],
                    )
                except Exception as e:
                    print('error occurred')
                    print(result_data.values())
                    print(e)
        """

        print('parsing csv/br_news.csv')
        with open(join(settings.BASE_DIR, 'csv/br_news.csv'), 'r') as f:
            #Contribution.objects.all().delete()
            reader = csv.reader(f)
            headers = next(reader)
            headers[0] = 'id'
            print(headers)
            rows = []
            for row in reader:
                rows.append({key: value for key, value in zip(headers, row)})

            #Contribution.objects.all().delete()
            contest_type, _ = ContestType.objects.get_or_create(
                    name="BRKOS",
                    abbreviation="brkos")

            for row in rows:
                text = row['content']
                name = row['name']
                date_created = make_aware(datetime.fromtimestamp(int(row['date'])))

                try:
                    Contribution.objects.get_or_create(title=name, published=True, text=text, created=date_created, contest_type=contest_type)
                except Exception as e:
                    print('error occurred')
                    print(result_data.values())
                    print(e)

        print('parsing csv/br_topics.csv')
        with open(join(settings.BASE_DIR, 'csv/br_topics.csv'), 'r') as f:
            reader = csv.reader(f)
            headers = next(reader)
            headers[0] = 'id'
            print(headers)
            rows = []
            for row in reader:
                rows.append({key: value for key, value in zip(headers, row)})

            thread_dict = {}
            for row in rows:
                id = int(row['id'])
                title = row['name']

                try:
                    topic, _ = Topic.objects.get_or_create(title=title,contest_type=contest_type)
                    thread, _ = Thread.objects.get_or_create(author_id=1, topic=topic, title="Začátek vlákna " + title)
                    thread_dict[id] = thread
                except Exception as e:
                    print('error occurred')
                    print(row)
                    print(e)
                    return


        print('parsing csv/br_posts.csv')
        with open(join(settings.BASE_DIR, 'csv/br_posts.csv'), 'r') as f:
            #Post.objects.all().delete()
            reader = csv.reader(f)
            headers = next(reader)
            headers[0] = 'id'
            print(headers)
            rows = []
            for row in reader:
                rows.append({key: value for key, value in zip(headers, row)})

            for row in rows:
                user_id = int(row['userid'])
                text = row['content']
                date_created = make_aware(datetime.fromtimestamp(int(row['date'])))
                topic = int(row['topic'])

                try:
                    Post.objects.get_or_create(
                        author= (user_dict[user_id] if user_id in user_dict else None),
                        parent=thread_dict[topic],
                        text=text,
                        created=date_created,
                    )
                except Exception as e:
                    print('error occurred')
                    print(row)
                    print(e)

        print('parsing csv/br_events.csv')
        with open(join(settings.BASE_DIR, 'csv/br_events.csv'), 'r') as f:
            reader = csv.reader(f)
            headers = next(reader)
            headers[0] = 'id'
            print(headers)
            rows = []
            for row in reader:
                rows.append({key: value for key, value in zip(headers, row)})

            for row in rows:
                name = row['name']
                place = row['place']
                description = row['about']
                start = make_aware(datetime.fromtimestamp(int(row['begin'])))
                end = make_aware(datetime.fromtimestamp(int(row['end'])))
                try:
                    Camp.objects.get_or_create(
                        name=name,
                        place=place,
                        description=description,
                        start=start,
                        end=end,
                        registration_from=timezone.now(),
                        registration_to=timezone.now(),
                    )
                except Exception as e:
                    print('error occurred')
                    print(row)
                    print(e)

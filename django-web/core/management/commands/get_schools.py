import xml.etree.ElementTree as ET
import json
import requests
import re
from unidecode import unidecode

from django.core.management.base import BaseCommand


def has_highschool(subjekt):
    types = list(subjekt.iter("SkolaDruhTyp"))
    for t in types:
        if t.text == "C00":
            return True
    
    return False


def get_hash(name):
    joined = re.sub("[\s.,\/]","",name)
    return unidecode(joined).lower()


class Command(BaseCommand):
    help = 'Downloads and process the list of schools'

    # def add_arguments(self, parser):
    #     parser.add_argument('poll_ids', nargs='+', type=int)

    def handle(self, *args, **options):
        xml = requests.get('https://rejstriky.msmt.cz/opendata/VREJcelk.xml')

        root = ET.fromstring(xml.text)

        schools = []


        for subjekt in root.iter("PravniSubjekt"):
            if has_highschool(subjekt):
                name = list(subjekt.iter("RedPlnyNazev"))[0].text
                schools.append({"name":name, "hash":get_hash(name)})


        with open("core/static/core/files/list_of_schools.json", "w") as outfile:
            json.dump(schools,outfile)
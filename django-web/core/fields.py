from django import forms

from core import widgets


class FieldMixin:

    def set_label(self, labeled=False):
        if labeled:
            self.widget = self.__class__.widget(labeled=True)
        self.widget.attrs['label'] = self.label


class CharField(forms.CharField,FieldMixin):
    widget = widgets.TextWidget

    def __init__(self, labeled=False, **kwargs):
        super().__init__(**kwargs)
        self.set_label(labeled)
        


class PasswordField(CharField):
    widget = widgets.PasswordWidget


class EmailField(forms.EmailField,FieldMixin):
    widget = widgets.EmailWidget

    def __init__(self, labeled=False, **kwargs):
        super().__init__(**kwargs)
        self.set_label(labeled)


class BooleanField(forms.BooleanField,FieldMixin):
    widget = widgets.CheckboxWidget

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.set_label()


class IntegerField(forms.IntegerField,FieldMixin):
    widget = widgets.NumberWidget

    def __init__(self, labeled=False, **kwargs):
        super().__init__(**kwargs)
        self.set_label(labeled)
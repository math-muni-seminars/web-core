from django import forms
from django.forms import Form, ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from django.core.validators import FileExtensionValidator
from django.forms import ValidationError

from core.fields import *

User = get_user_model()

class RegisterForm(UserCreationForm):
    username = CharField(required=True, label="Uživatelské jméno")
    email = EmailField(required=True, label="Email")
    first_name = CharField(required=True, max_length=30, label="Jméno")
    last_name = CharField(required=True, max_length=30, label="Příjmení")
    password1 = PasswordField(
        label="Heslo",
        strip=False,
    )
    password2 = PasswordField(
        label="Heslo znovu",
        strip=False,
    )

    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name', 'password1', 'password2']


class AccountForm(ModelForm):
    email = EmailField(required=True, label="Email", labeled=True)
    first_name = CharField(required=True, max_length=30, label="Jméno", labeled=True)
    last_name = CharField(required=True, max_length=30, label="Příjmení", labeled=True)

    def clean(self):
        if "email" in self.changed_data:
            user = self.instance
            user.verified = False
            user.save()

    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name']

class UploadForm(Form):
    file = forms.FileField(
        required=True,
    )

    def __init__(self,extensions,max_size,*args,**kwargs):
        self.extensions = extensions
        self.max_size = max_size
        super(UploadForm,self).__init__(*args,**kwargs)

    def clean(self):
        file = self.cleaned_data.get('file',None)
        if file is None: return
        errors = ""

        if file.size > self.max_size*1000000:
            errors += 'Soubor je příliš velký. Limit je {} MB. '.format(self.max_size)
        if file.name.split('.')[1].lower() not in self.extensions:
            msg = 'Soubor nemá správný formát. Povolené formáty jsou: '
            for extension in self.extensions: msg += extension+', '
            errors += msg.rstrip(", ")

        if errors != "":
            raise ValidationError({'file':errors.rstrip(" ")})
    
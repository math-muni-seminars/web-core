from django.db import models
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from os import path, rename, makedirs

import datetime
import hashlib

from django.apps import apps

from django.core.exceptions import ValidationError
from django.core.mail import EmailMessage
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.contrib.sites.shortcuts import get_current_site
from django.template import loader


class User(AbstractUser):
    first_name = models.CharField("jméno", max_length=30)
    last_name = models.CharField("příjmení", max_length=30)
    verified = models.BooleanField("ověřený", default=False)
    last_email = models.DateTimeField("poslední odeslání emailu", null=True)

    def full_name(self):
        return "{0} {1}".format(self.first_name, self.last_name)
    full_name.short_description = "jméno a příjmení"

    def is_verified(self):
        return self.verified and self.is_authenticated

    def to_be_verified(self):
        return self.is_authenticated and not self.verified

    def can_email(self):
        if self.last_email is None: return True
        return timezone.now() >= self.last_email + datetime.timedelta(minutes=1)

    def verification_token(self):
        string = self.username + self.first_name + self.last_name + self.email + str(self.date_joined)
        bhash = hashlib.md5(string.encode('utf-8')).digest()
        return urlsafe_base64_encode(bhash)

    def send_verification(self,request):
        if self.can_email() and not self.verified:
            subject = "BRKOS Ověření uživatelského účtu"
            context = {
                'token': self.verification_token(),
                'uid': urlsafe_base64_encode(force_bytes(self.pk)),
                'protocol': 'http',
                'domain': get_current_site(request).domain,
            }
            content = loader.render_to_string('verification/email.html', context)
            email = EmailMessage(subject,content,'"BRKOS" <'+settings.EMAIL_HOST_USER+'>',[self.email])
            email.send(fail_silently = False)
            self.last_email = timezone.now()
            self.save()
        else:
            raise Exception("Není možné poslat mail")

    def __str__(self):
        return "{0} - {1} {2}".format(self.username, self.first_name, self.last_name)

    def clean(self):
        if self.first_name != self.first_name.capitalize():
            raise ValidationError({"first_name":"Jméno musí začínat velkým písmenem."})
        if self.last_name != self.last_name.capitalize():
            raise ValidationError({"last_name":"Příjmení musí začínat velkým písmenem."})


class TracedModel(models.Model):
    class Meta:
        abstract = True

    created = models.DateTimeField("vytvořeno", default=timezone.now, editable=False)
    updated = models.DateTimeField("upraveno", default=timezone.now, editable=False)

    def save(self, *args, **kwargs):
        self.updated = timezone.now()
        super().save(*args, **kwargs)

        return self


class MediaFile(TracedModel):
    class Meta:
        verbose_name = 'soubor'
        verbose_name_plural = 'soubory'

    @staticmethod
    def userid_from_filename(filename):
        return filename.split('__')[0]

    class AccessPolicy(models.IntegerChoices):
        PUBLIC = 0, ('veřejné')
        PROTECTED = 1, ('chráněné')
        PRIVATE = 2, ('soukromé')

    def path_in_storage(self, orig_fname=None):
        if '.' in orig_fname:
            ext = '.' + orig_fname.split('.')[-1]
        else:
            ext = ''

        dirname, name = path.dirname(self.name), path.basename(self.name)

        if self.access == 2:
            prefix = settings.PRIVATE_DIR_NAME
        elif self.access == 1:
            prefix = settings.PROTECTED_DIR_NAME
        else:
            prefix = settings.MEDIA_DIR_NAME
        
        return path.join(prefix, dirname, "{0}{1}".format(name, ext))

    def save(self, *args, **kwargs):
        if self.content.storage.exists(self.content.name):
            new_path = self.content.storage.path(self.path_in_storage(self.content.name))
            makedirs(path.dirname(new_path), exist_ok=True)
            rename(self.content.path, new_path)
            self.content.name = self.path_in_storage(self.content.name)

        super(MediaFile, self).save(*args, **kwargs)

    def url(self):
        return self.content.url

    def __str__(self):
        return f"{self.name} ({str(self.id)})"


    owners = models.ManyToManyField(User, verbose_name='vlastník', blank=True)
    content = models.FileField('soubor', upload_to=path_in_storage, storage=FileSystemStorage(settings.STORAGE_ROOT,
                                                                                              settings.STORAGE_URL))
    name = models.CharField('cesta/jméno', max_length=255, unique=True)

    access = models.PositiveSmallIntegerField('přístup',
        choices=AccessPolicy.choices,
        default=AccessPolicy.PRIVATE,
        help_text="""
        <p>Soukromé = může přistupovat pouze vlastník a organizátor</p>
        <p>Chráněné = organizátor může přistupovat kdykoliv, ostatní pouze za určitých podmínek (např. když má být řešení zveřejněno)</p>
        """
    )


class Organizer(TracedModel):
    """
    Extends the user model by adding the organisator data.
    """

    class Meta:
        verbose_name = "organizátor"
        verbose_name_plural = "organizátoři"

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nickname = models.CharField("přezdívka", max_length=30, unique=True)
    photo = models.FileField('fotka', null=True, blank=True, upload_to="organizers/")
    active = models.BooleanField("aktivní", default=True)

    def __str__(self):
        return "{0} - {1}".format(self.nickname,self.user.full_name())


class BaseModel(TracedModel):
    class Meta:
        abstract = True

    def upload_file(self, attrname, file, target_filename, owners, access=2):
        current_media_file = getattr(self, attrname)

        if current_media_file is not None:
            setattr(self, attrname, None)
            self.save()
            current_media_file.delete()

        media_file = MediaFile.objects.create(
            content=file,
            access=access,
            name=target_filename,
        )

        for owner in owners:
            media_file.owners.add(owner)

        setattr(self, attrname, media_file)
        self.save()


class PublishableModel(BaseModel):
    class Meta:
        abstract = True

    published = models.BooleanField("publikováno", default=False, blank=True)
    public_from = models.DateTimeField("publikováno od", null=True, blank=True)
    public_to = models.DateTimeField("publikováno do", null=True, blank=True)

    def clean(self):
        if self.public_from or self.public_to:
            self.published = True

    def is_public(self):
        if not self.published:
            return False
        if self.public_from and self.public_from >= timezone.now():
            return False
        if self.public_to and self.public_to <= timezone.now():
            return False
        return True

    is_public.boolean = True
    is_public.short_description = "veřejné"

    @staticmethod
    def q_public():
        return Q(Q(public_to__gte=timezone.now()) | Q(public_to=None),
                 Q(public_from__lte=timezone.now()) | Q(public_from=None),
                 published=True)

    @classmethod
    def public_objects(cls):
        return cls.objects.filter(PublishableModel.q_public())


class RichTextField(models.TextField):
    pass


class StaticPage(BaseModel):
    class Meta:
        verbose_name = 'statická stránka'
        verbose_name_plural = 'statické stránky'
    
    endpoint = models.CharField('URL lokace', max_length=30, default="")
    title = models.CharField('title', max_length=30, default="")
    name = models.CharField('název v menu', max_length=30, default="")
    content = models.TextField('obsah', default="")

    def save(self, *args, **kwargs):
        if not self.endpoint or self.endpoint[0] != '/':
            self.endpoint = '/' + self.endpoint
        if self.endpoint[-1] == '/':
            self.endpoint = self.endpoint[:-1]
        super(StaticPage, self).save(*args, **kwargs)

"""
class Menu(models.Model):
    class Meta:
        verbose_name = 'menu'
        verbose_name_plural = 'menu'

    name = models.CharField('jméno', max_length=30)
    pages = models.ManyToManyField(StaticPage)
    locations = models.TextField('lokace')
"""

class Text(models.Model):
    class Meta:
        verbose_name = "text"
        verbose_name_plural = "texty"

    template = models.CharField('template', max_length=255)
    name = models.SlugField('jméno textu', max_length=255)
    content = models.TextField('obsah')


class Mail(models.Model):
    class Meta:
        verbose_name = "mail"
        verbose_name_plural = "maily"


    class Recipients(models.TextChoices):
        NO = 'NOO', ('nikdo')
        USERS_ALL = 'USA', ('uživatelé')
        CONTESTANTS_ALL = 'COA', ('BR soutěžící - všichni')
        CONTESTANTS_CURRENT = 'COC', ('BR soutěžící - tento ročník')
        CONTESTANTS_LATEST = 'COL', ('BR soutěžící - poslední série')
        CONTESTANTS_POTENTIAL = 'COP', ('BR soutěžící - potenciální')
        CAMP_ALL = 'CAA', ('soustředění - všichni přihlášení')
        CAMP_WAITING = 'CAW', ('soustředění - čekající')
        CAMP_ACCEPTED = 'CAC', ('soustředění - přijatí')
        CAMP_SUB = 'CAS', ('soustředění - náhradníci')
        TEAMS_CURRENT = 'TEC', ('MR týmy - tento ročník')


    to = models.CharField('komu', max_length=3, choices=Recipients.choices, default=Recipients.NO)
    subject = models.CharField('předmět', max_length=255, default="BRKOS ")
    in_html = models.BooleanField('v HTML', default=True, help_text="<p>Pokud je toto nastavení vypnuté, nelze psát email v HTML ani Markdownu. LaTeX není podporován nikdy.</p>")
    content = models.TextField('zpráva', default="<p style='margin-bottom: 15px'>Milí řešitelé,</p>\n\n<p style='margin-bottom: 15px'>text.</p>\n\n<p>S pozdravem<br>\nBRKOS Team</p>")
    attachment1 = models.FileField('příloha 1', null=True, blank=True, upload_to="attachments/", storage=FileSystemStorage(settings.PRIVATE_ROOT,settings.PRIVATE_URL))
    attachment2 = models.FileField('příloha 2', null=True, blank=True, upload_to="attachments/", storage=FileSystemStorage(settings.PRIVATE_ROOT,settings.PRIVATE_URL))
    attachment3 = models.FileField('příloha 3', null=True, blank=True, upload_to="attachments/", storage=FileSystemStorage(settings.PRIVATE_ROOT,settings.PRIVATE_URL))
    sent = models.BooleanField('odesláno', default=False)
    sent_time = models.DateTimeField('čas odeslání', null=True, blank=True)

    def recipients(self):
        Contestant = apps.get_model("contest.Contestant")
        CampApplication = apps.get_model("camp.CampApplication")
        Team = apps.get_model("mathrace.Team")

        if self.to == "USA": return [user.email for user in User.objects.all()]
        if self.to == "COA": return [contestant.user.email for contestant in Contestant.objects.all()]
        if self.to == "COC": return [contestant.user.email for contestant in Contestant.current_contest()]
        if self.to == "COL": return [contestant.user.email for contestant in Contestant.latest_closed_collection()]
        if self.to == "COP": return [contestant.user.email for contestant in Contestant.potential()]
        if self.to == "CAA": return [participant.participant.email for participant in CampApplication.last_camp()] 
        if self.to == "CAW": return [participant.participant.email for participant in CampApplication.last_camp().filter(status = "WTG")] 
        if self.to == "CAC": return [participant.participant.email for participant in CampApplication.last_camp().filter(status = "ACC")] 
        if self.to == "CAS": return [participant.participant.email for participant in CampApplication.last_camp().filter(status = "SUB")]
        if self.to == "TEC": return [team.user.email for team in Team.current_contest()]
        return []



class TagField(models.TextField):
    pass


class ContestType(BaseModel):
    class Meta:
        verbose_name = "typ soutěže"
        verbose_name_plural = "typy soutěží"

    name = models.CharField("název", max_length=200)
    abbreviation = models.SlugField("zkratka", unique=True)

    def __str__(self):
        return "{0}".format(self.name)
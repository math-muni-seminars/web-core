from django.forms import Widget
from django import forms
from django.contrib.admin.widgets import AdminSplitDateTime, AdminDateWidget, AdminTimeWidget
from . import settings


class MarkdownTextField(Widget):
    template_name = 'core/widgets/markdown_textarea.html'

    def __init__(self, attrs=None):
        default_attrs = {'cols': '70', 'rows': '10'}
        if attrs:
            default_attrs.update(attrs)
        super().__init__(default_attrs)


class TagField(Widget):
    template_name = 'core/widgets/tag_selector.html'

    colors = [("error", "error"), ("info", "info"), ("ok", "success"), ("warning", "warning")]

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)
        context['colors'] = self.colors
        print(context)
        return context

    def __init__(self, attrs=None):
        default_attrs = {}
        if attrs:
            default_attrs.update(attrs)
        super().__init__(default_attrs)


class CheckboxGDPRWidget(forms.widgets.CheckboxInput):
    template_name = 'core/widgets/checkbox_GDPR.html'


class CheckboxWidget(forms.widgets.CheckboxInput):
    template_name = 'core/widgets/checkbox.html'


class PasswordWidget(forms.widgets.PasswordInput):
    template_name = 'core/widgets/password.html'


class RegularInput:

    def label(self,labeled):
        if labeled:
            self.template_name = 'core/widgets/input_labeled.html'


class TextWidget(forms.widgets.TextInput,RegularInput):
    template_name = 'core/widgets/input.html'

    def __init__(self, labeled=False, attrs=None):
        self.label(labeled)
        super().__init__(attrs)


class NumberWidget(forms.widgets.NumberInput,RegularInput):
    template_name = 'core/widgets/input.html'

    def __init__(self, labeled=False, attrs=None):
        self.label(labeled)
        super().__init__(attrs)


class EmailWidget(forms.widgets.EmailInput,RegularInput):
    template_name = 'core/widgets/input.html'

    def __init__(self, labeled=False, attrs=None):
        self.label(labeled)
        super().__init__(attrs)

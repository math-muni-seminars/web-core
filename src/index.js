import ReactDOM from 'react-dom';
import React from 'react';
export {default as axios} from 'axios';
import jquery from 'jquery';

window.$ = window.JQuery = jquery;
window.React = React;

window.spawn = (...args) => {
    console.log(args);
}

// export * from './layout/Header.js';
export * from './components';

/*
export function deleteByUrl(url, redirect_url='') {
    $.ajax({
        url: url,
        type: 'DELETE',
        headers: {'X-CSRFToken': CSRF_TOKEN},
        success: function(result) {
            window.location.replace(redirect_url);
        }
    });
}

export function upload_file(form_descriptor, url) {
    var fd = new FormData();
    var files = $(form_descriptor)[0].files[0];
    fd.append('file', files);

    $.ajax({
        url: url,
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
        headers: { "X-CSRFToken": CSRF_TOKEN },
        success: function(response){
            alert(response);
        },
        error: function (request, status, error) {
            alert(request.responseText);
        }
    });
}
*/

export function spawn(cls, elem, props=null) {
    if (elem instanceof String || typeof elem === 'string') {
        elem = document.getElementById(elem);
    }
    ReactDOM.render(React.createElement(cls, props), elem);
}

export function upload(form_descriptor, url) {
    var fd = new FormData();
    var files = $(form_descriptor)[0].files[0];
    fd.append('file', files);

    $.ajax({
        url: url,
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
        headers: { "X-CSRFToken": CSRF_TOKEN },
        success: function(response){
            alert(response);
        },
        error: function (request, status, error) {
            alert(request.responseText);
        }
    });
}

export function upload_reload(form_descriptor, url) {
    var fd = new FormData();
    var files = $(form_descriptor)[0].files[0];
    fd.append('file', files);

    $.ajax({
        url: url,
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
        headers: { "X-CSRFToken": CSRF_TOKEN },
        success: function(response){
            if(!response || !alert(response)) location.reload();
        },
        error: function (request, status, error) {
            alert(request.responseText);
        }
    });
}

export function request_reload(url) {
    $.ajax({
        url: url,
        type: 'get',
        contentType: false,
        processData: false,
        headers: { "X-CSRFToken": CSRF_TOKEN },
        success: function(response){
            if(!response || !alert(response)) location.reload();
        },
        error: function (request, status, error) {
            alert(request.responseText);
        }
    });
}

export function message_reload(source_descriptor,url)
{
    var fd = new FormData();
    fd.append("text",$(source_descriptor).val());

    $.ajax({
        url: url,
        data: fd,
        type: 'post',
        contentType: false,
        processData: false,
        headers: { "X-CSRFToken": CSRF_TOKEN },
        success: function(response){
            location.reload();
        },
        error: function (request, status, error) {
            alert(request.responseText);
        }
    });
}

export function save_solution(url){
    var fd = new FormData();
    var submission = $('#textarea').val();
    fd.append('submission', submission);

    $.ajax({
        url: url,
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
        headers: { "X-CSRFToken": CSRF_TOKEN },
        success: function(response){
            var d = new Date();
            $("#last_save_time").text("Naposledy uloženo v "+d.toLocaleTimeString());
            touched = false;
            timeout_save(true);
        },
        error: function (request, status, error) {
            alert("Uložení se nezdařilo. Zkopírujte svoje řešení a uložte jej stranou a znovu načtěte stránku.\n"+request.responseText);
        }
    });
}

export function upload_files(task_id,url)
{
    var fd = new FormData();
    var files = document.getElementById("task-"+task_id+"-file-dialog").files;
    for(var i=0; i < files.length; i++) fd.append(files[i].name, files[i]);

    $.ajax({
        url: url,
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
        headers: { "X-CSRFToken": CSRF_TOKEN },
        success: function(response){
            alert(response);
        },
        error: function (request, status, error) {
            alert(request.responseText);
        }
    });
}

export function send_answer(task_id,team_id,token) {
    var fd = new FormData();
    fd.append('answer', $('#input-'+task_id).val());
    fd.append('task', task_id);
    fd.append('team', team_id);
    fd.append('token', token);

    $.ajax({
        url: "/apache/mathrace_submit.php",
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
        headers: { "X-CSRFToken": CSRF_TOKEN },
        success: function(response){
            if(response=="interval_viol") alert("Porušen časový rozestup, pokus nebyl platný.");
            else if(response=="wrong")
            {
                $('#wrong-'+task_id).val($('#wrong-'+task_id).val()+1);
                alert("Špatně.");
            }
            else if(response=="right")
            {
                $('#solved-'+task_id).show();
                alert("Správně.");
            }
            else alert("Chyba.");
        },
        error: function (request, status, error) {
            alert(request.responseText);
        }
    });
}

export function show_password(id)
{
    $("#"+id).attr("type","text");
    $("#"+id+"-show").hide();
    $("#"+id+"-hide").show();
}

export function hide_password(id)
{
    $("#"+id).attr("type","password");
    $("#"+id+"-show").show();
    $("#"+id+"-hide").hide();
}

export function trigger_checkbox_specify(bind_id,condition)
{
    if(condition=="yes")
    {
        if(!$('#checkbox-specify-'+bind_id).is(':checked')) $('#text-specify-'+bind_id).hide();
        else $('#text-specify-'+bind_id).show();
    }
    else
    {
        if($('#checkbox-specify-'+bind_id).is(':checked')) $('#text-specify-'+bind_id).hide();
        else $('#text-specify-'+bind_id).show();
    }
}
import React from "react"

export const Header = () => (
  <header className="sticky hrefp-0 z-50 bg-white shadow">
    <div className="container flex flex-col items-center justify-between px-8 py-4 mx-auhref sm:flex-row">
      <div className="flex items-center text-2xl">
        <a href="/#domu">
          {
            "Σ "
          }
          Maskot
        </a>
      </div>
      <div className="flex mt-4 font-medium md:text-xl sm:mt-0">
        <a className="px-4" href="/#info">
          Info
        </a>
        <a className="px-4" href="/#ocekavani">
          Co tě čeká
        </a>
        <a className="px-4" href="/#org">
          Orgové
        </a>
        <a className="px-4" href="/#galerie">
          Galerie
        </a>
      </div>
      <div className="hidden md:block">
        <a href="/SignUp/">
          <buthrefn className="font-medium btn btn-primary">Přihláška</buthrefn>
        </a>
      </div>
    </div>
  </header>
)

export default Header
import React from 'react';
import axios from 'axios';

export class LoginFormMathrace extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.render = this.render.bind(this);
    }
    
    handleChange(event) {
        let newState = {}
        newState[event.target.name] = event.target.value
        this.setState(newState);
    }

    handleSubmit(event) {
        axios.defaults.headers.common['X-CSRFToken'] = CSRF_TOKEN;

        this.setState({status: 'waiting', message: ''})

        let data = new FormData();
        data.append("username", this.state.username);
        data.append("password", this.state.password);

        axios.post('/login/?text_only', data)
            .then((response => {
                window.location.replace(this.props.redirect_url || '');
                this.setState({
                    status: 'info',
                    message: response.data
                });
            }).bind(this))
            .catch((error => {
                    this.setState({
                        status: 'error',
                        message: error.response.data
                    });
                }).bind(this)
            )

        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <h3 className="dark-text">Přihlášení</h3>
                <div className="alert alert-info my-2 p-3 text-sm block text-justify">Můžete se přihlásit účtem od BRKOSu, příp. účtem z minulého roku.</div>
                <div className="form-control">
                    <label className="label">
                        <span className="label-text">Uživatelské jméno</span>
                    </label> 
                    <input
                        id="username-field"
                        type="text"
                        name="username"
                        placeholder="Jméno"
                        className={`input input-bordered ${this.state.username=="" ? "input-info" : ""}`}
                        onChange={this.handleChange}
                    ></input>

                    <label className="label">
                        <span className="label-text">Heslo</span>
                    </label> 
                    <input
                        id="password-field"
                        type="password"
                        name="password"
                        placeholder="Heslo"
                        className={`input input-bordered ${this.state.password=="" ? "input-info" : ""}`}
                        onChange={this.handleChange}
                    ></input>
                </div>
                <p>
                    <a className="text-sm px-1" href="/mathrace/reset_password/">Obnova hesla</a>
                </p>
                <p>
                    <button id="login-button" className="btn btn-primary w-full">Přihlaš se!</button>
                </p>
                <p>
                    { this.state.status && this.state.status != "waiting" ? <div className={`alert alert-${this.state.status}`}>{this.state.message}</div> : "" }
                    { this.state.status == "waiting" ? <div className="btn btn-circle loading"></div> : "" }
                </p>
            </form>
        )
    }
}


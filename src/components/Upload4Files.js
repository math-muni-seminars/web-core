import React from 'react';
import axios from 'axios';


export class Upload4Files extends React.Component {
    // Props: url
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.render = this.render.bind(this);
        this.ref = React.createRef();
        this.handleClick = this.handleClick.bind(this);
        this.state = {type: props.types[0]};
    }

    handleClick() {
        //this.setState({type: props.types[typei]});
        this.ref.current.click();
    }

    // on click callback
    handleChange() {
        const file = this.ref.current.files[0];
        var fd = new FormData();
        fd.append('file', file);
        fd.append('type',this.state.type);

        $.ajax({
            url: this.props.url,
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            headers: { "X-CSRFToken": CSRF_TOKEN },
            success: function(response){
                if(!alert(response)) location.reload();
            },
            error: function (request, status, error) {
                alert(request.responseText);
            }
        });
    }

    render() {
        return (
            <>
                <td><a className="button" onclick={this.handleClick}>
                    <i className="fas fa-file-upload"></i>
                </a></td>
                <td><a className="button" onclick={() => this.handleClick(1)}>
                    <i className="fas fa-file-upload"></i>
                </a></td>
                <td><a className="button" onclick={() => this.handleClick(2)}>
                    <i className="fas fa-file-upload"></i>
                </a></td>
                <td><a className="button" onclick={() => this.handleClick(3)}>
                    <i className="fas fa-file-upload"></i>
                </a></td>
                <input type='file' id='file' ref={this.ref} onChange={this.handleChange} style={{display: 'none'}}/>
            </>
        );
    }
}
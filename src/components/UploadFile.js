import React from 'react';
import axios from 'axios';


export class UploadFile extends React.Component {
    // Props: url
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.render = this.render.bind(this);
        this.ref = React.createRef();
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.ref.current.click();
    }

    // on click callback
    handleChange() {
        const file = this.ref.current.files[0];
        var fd = new FormData();
        fd.append('file', file);

        $.ajax({
            url: this.props.url,
            type: 'post',
            data: fd,
            contentType: false,
            processData: false,
            headers: { "X-CSRFToken": CSRF_TOKEN },
            success: function(response){
                if(!alert(response)) location.reload();
            },
            error: function (request, status, error) {
                alert(request.responseText);
            }
        });
    }

    render() {
        return (
            <span onClick={this.handleClick}>
            {this.props.children}
            <input type='file' id='file' ref={this.ref} onChange={this.handleChange} style={{display: 'none'}}/>
            </span>
        );
    }
}
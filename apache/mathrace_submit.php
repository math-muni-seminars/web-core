<?php

try
{
    $now = $_SERVER['REQUEST_TIME'];

    $conn = new mysqli();

    $result = $conn->query("SELECT start FROM mathrace_contest ORDER BY order DESC");

    if($result->num_rows == 0) throw new Exception("no_contest");

    $row = $result->fetch_assoc();
    $start = strtotime($row['start']);

    $time = $now - $start;

    if(!(isset($_POST["team"]) && isset($_POST["token"]) && isset($_POST["answer"]) && isset($_POST["task"]))) throw new Exception("bad_request");

    $team = $_POST["team"];
    $token = $_POST["token"];
    $task = $_POST["task"];
    $answer = $_POST["answer"];
    $result = $conn->query("SELECT * FROM mathrace_team WHERE id=$team");

    if($result->num_rows == 0) throw new Exception("no_team");

    $row = $result->fetch_assoc();
    if($row['token']!=$token) throw new Exception("invalid_token");
    if($row['last_wrong_attempt'] != null and $time - $row['last_wrong_attempt'] < 40) throw new Exception("interval_viol");
    $solved_count = $row['solved_count'];
    $decisive_time = $row['decisive_time'];

    $result = $conn->query("SELECT solution_value FROM mathrace_task WHERE id=$task");

    if($result->num_rows == 0) throw new Exception("no_task");

    $correct = false;
    $row = $result->fetch_assoc();
    $solutions = explode("|",$row['solution_value']);
    foreach ($solutions as $solution)
    {
        if($solution==$answer) $correct = true;
    }

    $result = $conn->query("INSERT INTO mathrace_attempt (team_id, task_id, answer, time) VALUES ($team, $task, $answer, $time)");
    if(!$result) throw new Exception("db_failed");

    $result = $conn->query("SELECT id FROM mathrace_conteststatus WHERE task_id=$task AND team_id=$team");

    if($result->num_rows == 0)
    {
        $result = $conn->query("INSERT INTO mathrace_conteststatus (team_id, task_id) VALUES ($team, $task)");
        if(!$result) throw new Exception("db_failed");
    }

    $result = $conn->query("SELECT wrong_attempts FROM mathrace_conteststatus WHERE task_id=$task AND team_id=$team");
    if($result->num_rows == 0) throw new Exception("db_failed");
    $row = $result->fetch_assoc();
    $wrong = $row["wrong_attempts"];

    if($correct)
    {
        $result = $conn->query("UPDATE mathrace_conteststatus SET solved=1, time=$time WHERE team_id=$team AND task_id=$task");
        if(!$result) throw new Exception("db_failed");
        $solved_count += 1;
        $decisive_time += $time + 1200*$wrong;
        $result = $conn->query("UPDATE mathrace_team SET solved_count=$solved_count, decisive_time=$decisive_time WHERE team_id=$team");
        if(!$result) throw new Exception("db_failed");
        echo "right";
    }
    else
    {
        $wrong += 1;
        $result = $conn->query("UPDATE mathrace_conteststatus SET wrong_attempts=$wrong WHERE team_id=$team AND task_id=$task");
        if(!$result) throw new Exception("db_failed");
        $result = $conn->query("UPDATE mathrace_team SET last_wrong_attempt=$time WHERE team_id=$team");
        if(!$result) throw new Exception("db_failed");
        echo "wrong";
    }
}
catch (Exception $E)
{
    echo $E->getMessage();
}

?>
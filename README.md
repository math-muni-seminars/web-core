# BRKOS web

### Initialize environment
Make sure to have `python3`, `npm`, and `pandoc` installed.
`npm` is required for development only.

#### Install python requirements
In order to run the server, you need `python3`. The server is implemented
based on `django` framework. Before running the server, you need to install all the necessary
python packages. You probably want to have all python packages separated in a virtual environment:
```bash
$ virtualenv -p python3 venv
$ source venv/bin/activate
$ pip3 install -r requirements.txt
```
Virtual environment is a folder containing a copy of the Python interpreter,
and you can install packages into this folder without affecting the global Python installation
or other virtual environments.

To activate the virtual env next time, use
```bash
$ source venv/bin/activate
```

#### Intall NPM requirements
Install `npm` packages by
```bash
$ npm install
```

We use `tailwind`(https://tailwindcss.com/docs) to compile styles. It is a CSS framework that provides a lot of useful low-level classes.
There is also a framework built on the top of `tailwind` called `daisyui` which defines plenty of high-level components.  
Source codes for styles are located in folder `src`.

After built, file `main.css` is created in folder `django-web/core/static/build`.

To compile css use
```bash
$ npm run build
```

To watch css files and rebuild css on every change run
```bash
$ npm run watch-css
```

#### Create db structure

```bash
$ cd django-web
$ ./manage.py makemigrations camp contest core forum mathrace
$ ./manage.py migrate
$ ./manage.py createsuperuser
```

Command `makemigrations` creates migration files that describe changes in the database structure.
So you need to run it every time you change the database structure.
Initially, you need to run it for all apps so that all relevant tables are described.

Command `migrate` applies all migrations to the database. In particular,
it creates all tables and columns that are described in the migration files
if they do not exist yet.

Command `createsuperuser` creates a user with superuser privileges.

### Run development server
```bash
Check core/settings.py
$ cd django-web
$ ./manage.py runserver
```


### Deploy to production
It is necessary to collect static files into single static folder which is opened to `apache`.
```bash
Update the core/settings.py (do not save to git)
$ cd django-web
$ ./manage.py collectstatic
```
